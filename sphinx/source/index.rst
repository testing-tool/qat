
.. toctree::
   :maxdepth: 3
   :hidden:

   Home <self>
   doc/tutorials
   Python API reference <doc/Python API reference.md>
   doc/Contributing_index
   Authors <doc/AUTHORS.md>
   Change log <CHANGELOG.md>

.. include:: ../../README.md
   :parser: myst_parser.sphinx_

