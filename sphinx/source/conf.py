# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os

project = 'Qat'
copyright = '2024, Qat authors'
author = 'Quentin Derouault'
release = os.environ['QAT_VERSION'] if 'QAT_VERSION' in os.environ else "latest"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser"
]

templates_path = ['_templates']

exclude_patterns = []

# -- Myst-Parser configuration -----------------------------------------------
# Auto-generate header anchors of h2 level heading
myst_heading_anchors = 2

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
html_logo = 'doc/images/qat_icon.svg'
html_sidebars = {'**': ['globaltoc.html', 'searchbox.html']}
html_theme_options = {
    'fixed_sidebar': True
}
