# Create symbolic links
DOC_LINK=./sphinx/source/doc
if test -e "$DOC_LINK"; then
   unlink "$DOC_LINK"
fi
ln -s ../../doc "$DOC_LINK"

mkdir -p ./sphinx/source/client/qat/gui/
IMAGE_LINK=./sphinx/source/client/qat/gui/images
if test -e "$IMAGE_LINK"; then
   unlink "$IMAGE_LINK"
fi
ln -s ../../../../../client/qat/gui/images "$IMAGE_LINK"

CHANGELOG_LINK=./sphinx/source/CHANGELOG.md
if test -e "$CHANGELOG_LINK"; then
   unlink "$CHANGELOG_LINK"
fi
ln -s ../../CHANGELOG.md "$CHANGELOG_LINK"