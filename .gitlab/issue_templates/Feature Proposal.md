<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by "type::feature" label:

- https://gitlab.com/testing-tool/qat/issues?label_name%5B%5D=type::feature

and verify the issue you're about to submit isn't a duplicate.

Write a descriptive proposal title above.
--->

### Feature suggestion

<!-- Summarize briefly the requested feature. -->

### Intended use

<!-- Brief description of why this feature would improve Qat. Who will use this feature? (Example : developer, user, etc.)
In which context the feature can be used ? Describe how your proposal will work, with code/pseudo-code, mockup or diagram. Please use code blocks (```) code as it's tough to read otherwise. -->

/label ~"type::feature"
