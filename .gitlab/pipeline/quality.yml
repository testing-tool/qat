pylint:
  stage: code-quality
  needs: []
  interruptible: true
  image: python:3.9-slim
  before_script:
    - mkdir -p public/badges public/$CI_JOB_NAME
    - echo undefined > public/badges/$CI_JOB_NAME.score
    - python3 -m pip install -r config/requirements.txt
  script:
    - pylint --rcfile=config/.pylintrc --exit-zero --output-format=text client | tee /tmp/pylint.txt
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/badges/$CI_JOB_NAME.score
    - pylint --rcfile=config/.pylintrc --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter client > codeclimate.json
    - pylint --rcfile=config/.pylintrc --fail-under 9.9 --output-format=pylint_gitlab.GitlabPagesHtmlReporter client > public/$CI_JOB_NAME/index.html
  after_script:
    - anybadge --overwrite --label $CI_JOB_NAME --value=$(cat public/badges/$CI_JOB_NAME.score) --file=public/badges/$CI_JOB_NAME.svg 4=red 6=orange 8=yellow 10=green
    - |
      echo "Your score is: $(cat public/badges/$CI_JOB_NAME.score)"
  artifacts:
    paths:
      - public
    reports:
      codequality: codeclimate.json
    when: always


clang-tidy:
  stage: code-quality
  needs: []
  interruptible: true
  image:
    name: "carlonluca/qt-dev:6.8.0"
    entrypoint: [""]
  before_script:
    - apt update && apt -y install clang-tidy
  script:
    # Generate compile_commands.json
    - mkdir -p build
    - cmake -G "Unix Makefiles" -S . -B build  -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release -DSKIP_RUST=ON
    # Analyze server files
    - find ./server/src/ -iname '*.cpp' | xargs clang-tidy  -p=build -header-filter=server/
    # Analyze plugin files - note: native plugins for other platforms cannot be analyzed on linux
    - find ./server/plugins/Qml/ -iname '*.cpp' | xargs clang-tidy  -p=build -header-filter=server/*
    - find ./server/plugins/QWidget/ -iname '*.cpp' | xargs clang-tidy  -p=build -header-filter=server/*


doc:
  stage: code-quality
  needs: []
  interruptible: true
  image: alpine:latest
  before_script:
    - apk add --update doxygen graphviz font-bitstream-type1 ghostscript-fonts ttf-cantarell
    - apk add --no-cache python3 py3-pip
    - !reference [.python_virtual_env_setup, script]
  script:
    - if [ "${CI_COMMIT_TAG}" != "" ]; then export QAT_VERSION="${CI_COMMIT_TAG}"; else export QAT_VERSION="0.0.0-dev"; fi
    # Doxygen
    - ( cat config/Doxyfile ; echo "OUTPUT_DIRECTORY=public" ) | doxygen -
    # Sphinx
    - python -m pip install -r ./sphinx/requirements.txt
    - . sphinx/prebuild.sh
    - sphinx-build -M html sphinx/source/ public/sphinx/
  artifacts:
    paths:
      - public
    when: always

spell-checker:
  stage: code-quality
  needs: []
  interruptible: true
  image: alpine:latest
  before_script:
    - apk add --no-cache python3 py3-pip
    - !reference [.python_virtual_env_setup, script]
  script:
    - python -m pip install codespell
    - codespell . --config ./config/.codespellrc -L thirdparty,globalY,localY,crate


# Generate code coverage
coverage:
  stage: analysis
  interruptible: true
  image: alpine:latest
  before_script:
    - apk add --no-cache python3 py3-pip
    - !reference [.python_virtual_env_setup, script]
    - python3 --version
    - python3 -m pip install coverage
  script:
    - coverage combine build/coverage/
    - coverage report --rcfile=config/.coveragerc
    - coverage xml -o coverage.xml --rcfile=config/.coveragerc
    - coverage html -d public/pycov --rcfile=config/.coveragerc
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    paths:
      - public
      - coverage.xml
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
