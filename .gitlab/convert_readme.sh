#!/bin/bash

# First argument should be the current Qat version
version=$1
if [ -z "${version}" ]; then
   echo "No version given, aborting"
   exit 2
fi

# Second argument should be the destination folder
destination=$2

readme_org="./README.md"
readme_conv="${destination}/README.md"

# Original links
doc_path="\(.\/doc"
img_path="${doc_path}\/images\/"
doc_link="${doc_path}\/(.*).md"

# Target links to readthedocs
rtd_path="\(https:\/\/qat\.readthedocs\.io\/en\/${version}"
rtd_img_path="${rtd_path}\/_images\/"
rtd_link_path="${rtd_path}\/doc\/\1.html"

# Generate converted file
sed -E -e "s/${img_path}/${rtd_img_path}/g" -e "s/${doc_link}/${rtd_link_path}/g" ${readme_org} > ${readme_conv}
