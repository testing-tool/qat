#!/bin/bash

# Argument should be the current Qat version
version=$1
if [ -z "${version}" ]; then
   version="0.0-dev.1"
   echo "No version given. Defaulting to ${version}"
fi

# Configure destinations for binaries
build_dir=build
package_dir=${build_dir}/package
bin_dir=$package_dir/bin
plugin_dir=$bin_dir/plugins
tcp_server_dir=$bin_dir/tcp_server
template_dir=$package_dir/templates

# Convert README file to standalone format
source $(dirname $0)/convert_readme.sh ${version} ${build_dir}

# Configure package version
echo VERSION=\'${version}\' > build/package/__about__.py

# Copy binaries
mkdir -p $plugin_dir/
mkdir -p $tcp_server_dir/
mkdir -p $template_dir/
cp build/libinjector.so $bin_dir
cp build/libinjector.dylib $bin_dir
cp build/injector.dll $bin_dir
cp build/injector.exe $bin_dir
cp build/libQatServer*.so $bin_dir
cp build/libQatServer*.dylib $bin_dir
cp build/QatServer*.dll $bin_dir
cp build/tcp_server/*.dll $tcp_server_dir
cp build/tcp_server/*.so $tcp_server_dir
cp build/tcp_server/*.dylib $tcp_server_dir
cp -R build/plugins/* $plugin_dir
cp -R templates/* $template_dir

# Generate package (wheel and sdist)
rm -rf ./pypackage
python3 -m build . -o pypackage
