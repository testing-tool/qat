// (c) Copyright 2024, Qat’s Authors

#include <mach-o/dyld.h>
#include <chrono>
#include <cstdlib>
#include <dlfcn.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

namespace
{
/// Thread calling the Start() function of the server
std::thread* startThread{nullptr};

/// Thread used for delayed injection (when we need to wait for Qt libraries to be loaded)
std::thread* injectionThread{nullptr};

/// Handle to the injected library
void* handle{nullptr};

/// Class implementing RAII for loading/unloading the server in the injected library
struct Loader
{
   /// Constructor - loads and starts the server
   Loader();

   /// Destructor - stops the server
   ~Loader();
};

/// Global instance to automatically start the server when this library is loaded.
static Loader gLoaderInstance;
} // namespace

/// Find the absolute root path of Qt Core library.
std::string FindQtCorePath()
{
   const auto nbLibs = _dyld_image_count();
   for (auto i = 0; i < nbLibs; ++i)
   {
      const auto* imageName = _dyld_get_image_name(i);
      if (imageName)
      {
         const std::filesystem::path path{imageName};
         const auto libName = path.filename().string();
         if (libName.find("QtCore") != std::string::npos)
         {
            std::cout << "QtCore library found at " << path << std::endl;
            return path;
         }
      }
   }
   return {};
}

/// Wait for all dependent libraries to be loaded into the current application.
bool WaitForLibrariesToLoad()
{
   uint32_t prevNbLibs = 0;
   int stableSteps = 0;
   // Allow 10 seconds for libs to be loaded
   for (int i = 0; i < 200; ++i)
   {
      const auto nbLibs = _dyld_image_count();
      if (nbLibs != prevNbLibs)
      {
         stableSteps = 0;
         prevNbLibs = nbLibs;
      }
      else
      {
         // Consider loading done when no new library is loaded for 500 ms
         if (++stableSteps >= 10)
         {
            std::cout << "Libraries loaded in " << (50 * i) << " ms" << std::endl;
            return true;
         }
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
   }
   return false;
}

/// Pre-load Qt libraries required by Qat server.
bool LoadQtLibraries()
{
   const auto qtCorePathString = FindQtCorePath();
   if (qtCorePathString.empty())
   {
      std::cerr << "Qt libraries not found" << std::endl;
      return false;
   }
   const std::filesystem::path qtCorePath{qtCorePathString};
   const auto qtLibPath = qtCorePath.parent_path();
   const auto qtCoreName = qtCorePath.filename().string();
   auto handle = dlopen(qtCorePathString.c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtCore library" << std::endl;
      return false;
   }

   const auto networkLibName = std::regex_replace(
      qtCoreName, std::regex("Core."), "Network.");
   handle = dlopen(
      (qtLibPath / networkLibName).string().c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtNetwork library" << std::endl;
      return false;
   }

   const auto guiLibName = std::regex_replace(qtCoreName, std::regex("Core."), "Gui.");
   handle = dlopen((qtLibPath / guiLibName).string().c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtGui library" << std::endl;
      return false;
   }

   return true;
}

bool Inject()
{
   std::cout << "Loading injector" << std::endl;

   // Get a handle to the current executable
   handle = dlopen(nullptr, RTLD_NOLOAD | RTLD_LAZY);
   // Assuming the current executable is linked to Qt, find the handle to the qVersion() function
   const char* (*qtVersionFunc)(void);
   *(void**) (&qtVersionFunc) = dlsym(handle, "qVersion");
   if (!qtVersionFunc)
   {
      const auto qtCoreLibPath = FindQtCorePath();
      if (!qtCoreLibPath.empty())
      {
         std::cout << "QtCore lib path is " << qtCoreLibPath << std::endl;
         const auto coreLibHandle = dlopen(qtCoreLibPath.c_str(), RTLD_LAZY);
         if (!coreLibHandle)
         {
            std::cerr << "Could not load QtCore library" << std::endl;
            return false;
         }
         *(void**) (&qtVersionFunc) = dlsym(coreLibHandle, "qVersion");
         if (!qtVersionFunc)
         {
            std::cerr << "Could not determine Qt version" << std::endl;
            return false;
         }
      }
      else
      {
         std::cerr << "Could not find QtCore library" << std::endl;
         return false;
      }
   }

   // Get current Qt version
   const std::string qtVersion = qtVersionFunc();
   std::cout << "Detected Qt version " << qtVersion << std::endl;
   dlclose(handle);

   if (!LoadQtLibraries())
   {
      std::cerr << "Could not load Qt libraries" << std::endl;
      return false;
   }

   // Get server library name
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return false;
   }
   const auto libraryName = "libQatServer." + elements[0] + "." + elements[1] + ".dylib";

   Dl_info libInfo;
   if (!dladdr(&gLoaderInstance, &libInfo))
   {
      std::cerr << "Could not retrieve library path (dladdr failed)" << std::endl;
      return false;
   }

   std::string libPathData = libInfo.dli_fname;
   if (libPathData.empty())
   {
      std::cerr << "Could not retrieve library path" << std::endl;
      return false;
   }

   std::filesystem::path libPath = libPathData;
   libPath = libPath.parent_path() / libraryName;
   std::cout << "Loading Qat server library: " << libPath << std::endl;

   // Load server library
   handle = dlopen(libPath.string().c_str(), RTLD_LAZY);
   if (!handle)
   {
      std::cerr << "Failed to load Qat server: " << libPath.string() << std::endl;
      std::cerr << dlerror() << std::endl;
      return false;
   }
   std::cout << "Successfully loaded Qat server" << std::endl;

   void (*func_start)(void);
   *(void**) (&func_start) = dlsym(handle, "Start");
   if (!func_start)
   {
      std::cerr << "Could not find Start function" << std::endl;
      return false;
   }
   startThread = new std::thread(
      [func_start]()
      {
         func_start();
      });
   return true;
}

void DelayedInject()
{
   std::cout << "Waiting for Qt libraries to be loaded before injecting server"
             << std::endl;
   if (!WaitForLibrariesToLoad())
   {
      std::cerr << "Timeout waiting for libraries to load" << std::endl;
      return;
   }
   Inject();
}

void OnUnload()
{
   std::cout << "OnUnload" << std::endl;
   if (startThread)
   {
      // Stop or abort the injected server
      void (*func_stop)(void);
      *(void**) (&func_stop) = dlsym(handle, "Stop");
      if (!func_stop)
      {
         std::cerr << "Could not find Stop function" << std::endl;
         return;
      }
      func_stop();

      startThread->join();
      std::cout << "Deleting startup thread" << std::endl;
      delete (startThread);
      startThread = nullptr;
   }
}

void OnLoad()
{
   if (!Inject())
   {
      injectionThread = new std::thread(DelayedInject);
   }
}

Loader::Loader()
{
   OnLoad();
}

Loader::~Loader()
{
   OnUnload();
}