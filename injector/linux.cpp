// (c) Copyright 2023, Qat’s Authors

#include <chrono>
#include <cstdlib>
#include <dlfcn.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <link.h>
#include <regex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

namespace
{
/// Thread calling the Start() function of the server
std::thread* startThread{nullptr};

/// Thread used for delayed injection (when we need to wait for Qt libraries to be loaded)
std::thread* injectionThread{nullptr};

/// Handle to the injected library
void* handle{nullptr};

/// Class implementing RAII for loading/unloading the server in the injected library
struct Loader
{
   /// Constructor - loads and starts the server
   Loader();

   /// Destructor - stops the server
   ~Loader();
};

/// Global instance to automatically start the server when this library is loaded.
Loader gLoaderInstance;
} // namespace

/// Callback used by dl_iterate_phdr() to find the absolute path of the injector library.
static int LibIterator(struct dl_phdr_info* info, size_t size, void* data)
{
   std::string name;

   /* Empty name refers to the binary itself. */
   if (!info->dlpi_name || !info->dlpi_name[0])
   {
      return 0;
   }

   name = info->dlpi_name;

   if (name.find("libinjector.so") == std::string::npos)
   {
      return 0;
   }

   auto* path = (std::string*) data;
   *path = std::filesystem::path(name).parent_path().string();

   return 1;
}

/// Callback used by dl_iterate_phdr() to count the number of dynamic libraries that are currently loaded.
static int LibCounter(struct dl_phdr_info* info, size_t size, void* data)
{
   int* count = reinterpret_cast<int*>(data);

   // Increment count if there is a non-empty library name
   if (info->dlpi_name && info->dlpi_name[0] != '\0')
   {
      (*count)++;
   }

   return 0;
}

/// Find the absolute root path of Qt Core library.
std::string FindQtCorePath()
{
   auto* handle = dlopen(nullptr, RTLD_NOW);
   struct link_map* map;
   if (dlinfo(handle, RTLD_DI_LINKMAP, &map))
   {
      std::cerr << dlerror() << std::endl;
      return {};
   }

   while (map)
   {
      std::string path{map->l_name};
      if (
         path.find("libQt6Core.so") != std::string::npos ||
         path.find("libQt5Core.so") != std::string::npos)
      {
         std::cout << "QtCore library found at " << path << std::endl;
         return path;
      }
      map = map->l_next;
   }
   return {};
}

/// Wait for all dependent libraries to be loaded into the current application.
bool WaitForLibrariesToLoad()
{
   int prevNbLibs = 0;
   int stableSteps = 0;
   // Allow 10 seconds for libs to be loaded
   constexpr int loopPeriod = 50; // ms
   constexpr int maxSteps = 200;
   constexpr int minStableSteps = 10;
   for (int i = 0; i < maxSteps; ++i)
   {
      int nbLibs = 0;
      dl_iterate_phdr(LibCounter, &nbLibs);
      if (nbLibs != prevNbLibs)
      {
         stableSteps = 0;
         prevNbLibs = nbLibs;
      }
      else
      {
         // Consider loading done when no new library is loaded for 500 ms
         if (++stableSteps >= minStableSteps)
         {
            std::cout << "Libraries loaded in " << (loopPeriod * i) << " ms" << std::endl;
            return true;
         }
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(loopPeriod));
   }
   std::cerr << "Timeout waiting for libraries to be loaded" << std::endl;
   return false;
}

/// Pre-load Qt libraries required by Qat server.
bool LoadQtLibraries()
{
   const auto qtCorePathString = FindQtCorePath();
   if (qtCorePathString.empty())
   {
      std::cerr << "Qt libraries not found" << std::endl;
      return false;
   }
   const std::filesystem::path qtCorePath{qtCorePathString};
   const auto qtLibPath = qtCorePath.parent_path();
   const auto qtCoreName = qtCorePath.filename().string();
   auto* handle = dlopen(qtCorePathString.c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtCore library" << std::endl;
      return false;
   }

   const auto networkLibName = std::regex_replace(
      qtCoreName, std::regex("Core."), "Network.");
   handle = dlopen(
      (qtLibPath / networkLibName).string().c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtNetwork library" << std::endl;
      return false;
   }

   const auto guiLibName = std::regex_replace(qtCoreName, std::regex("Core."), "Gui.");
   handle = dlopen((qtLibPath / guiLibName).string().c_str(), RTLD_LAZY | RTLD_GLOBAL);
   if (!handle)
   {
      std::cerr << "Could not load QtGui library" << std::endl;
      return false;
   }

   return true;
}

bool Inject()
{
   std::cout << "Loading injector" << std::endl;

   // Assuming the current executable is linked to Qt, find the handle to the qVersion() function
   const char* (*qtVersionFunc)(void);
   *(void**) (&qtVersionFunc) = dlsym(nullptr, "qVersion");
   if (!qtVersionFunc)
   {
      const auto qtCoreLibPath = FindQtCorePath();
      if (!qtCoreLibPath.empty())
      {
         std::cout << "QtCore lib path is " << qtCoreLibPath << std::endl;
         auto* const coreLibHandle = dlopen(qtCoreLibPath.c_str(), RTLD_LAZY);
         if (!coreLibHandle)
         {
            std::cerr << "Could not load QtCore library" << std::endl;
            return false;
         }
         *(void**) (&qtVersionFunc) = dlsym(coreLibHandle, "qVersion");
         if (!qtVersionFunc)
         {
            std::cerr << "Could not determine Qt version" << std::endl;
            return false;
         }
      }
      else
      {
         std::cerr << "Could not find QtCore library" << std::endl;
         return false;
      }
   }

   // Get current Qt version
   const std::string qtVersion = qtVersionFunc();
   std::cout << "Detected Qt version " << qtVersion << std::endl;

   if (!LoadQtLibraries())
   {
      std::cerr << "Could not load Qt libraries" << std::endl;
      return false;
   }

   // Get server library name
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return false;
   }
   const auto libraryName = "libQatServer." + elements[0] + "." + elements[1] + ".so";

   std::string libPathData;
   dl_iterate_phdr(LibIterator, &libPathData);
   if (libPathData.empty())
   {
      std::cerr << "Could not retrieve library path" << std::endl;
      return false;
   }

   std::filesystem::path libPath = libPathData;
   libPath = libPath / libraryName;

   // Load server library
   std::cout << "Loaded Qat server from: " << libPath << std::endl;
   handle = dlopen(libPath.string().c_str(), RTLD_LAZY);
   if (!handle)
   {
      std::cerr << "Failed to load Qat server: " << libPath.string() << std::endl;
      std::cerr << dlerror() << std::endl;
      return false;
   }
   std::cout << "Successfully loaded Qat server" << std::endl;

   void (*startFunction)(void);
   *(void**) (&startFunction) = dlsym(handle, "Start");
   if (!startFunction)
   {
      std::cerr << "Could not find Start function" << std::endl;
      return false;
   }
   startThread = new std::thread(startFunction);
   return true;
}

void DelayedInject()
{
   std::cout << "Waiting for Qt libraries to be loaded before injecting server"
             << std::endl;
   if (!WaitForLibrariesToLoad())
   {
      std::cerr << "Timeout waiting for libraries to load" << std::endl;
      return;
   }
   Inject();
}

void OnLoad()
{
   // If loading application includes Qt path in its RPATH or RUNPATH
   // (typically when running on the same machine the app was built)
   // it is possible to load Qat's libraries immediately
   if (!Inject())
   {
      // Otherwise we need to wait for the application to load Qt dependencies
      // itself, then use them to load Qat's libraries.
      injectionThread = new std::thread(DelayedInject);
   }
}

void OnUnload()
{
   std::cout << "OnUnload" << std::endl;
   if (startThread)
   {
      // Stop or abort the injected server
      void (*stopFunction)(void);
      *(void**) (&stopFunction) = dlsym(handle, "Stop");
      if (!stopFunction)
      {
         std::cerr << "Could not find Stop function" << std::endl;
         return;
      }
      stopFunction();

      startThread->join();
      std::cout << "Deleting startup thread" << std::endl;
      delete (startThread);
      startThread = nullptr;
   }
}

Loader::Loader()
{
   OnLoad();
}

Loader::~Loader()
{
   OnUnload();
}