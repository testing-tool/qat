// (c) Copyright 2023, Qat’s Authors

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #include <windows.cpp> // NOLINT(bugprone-suspicious-include)
#elif __linux__
   #include <linux.cpp> // NOLINT(bugprone-suspicious-include)
#elif __APPLE__
   #include <macos.cpp> // NOLINT(bugprone-suspicious-include)
#else
   #error "Unsupported platform"
#endif