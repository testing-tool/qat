mkdir build
cmake -G "Visual Studio 17 2022" -T v142 -DQAT_VERSION=0.0-dev.1 -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release -S . -B build

copy README.md build\README.md
mkdir build\package
mkdir build\package\bin
mkdir build\package\templates

python -m pip install qat -e .

@echo off

set PYTHONPATH=client
set QSG_RHI_BACKEND=opengl
set QT3D_RENDERER=opengl

set /p answer=Do you want to open VSCode? (y/N) 
if /I "%answer%" == "y" start code .

exit
