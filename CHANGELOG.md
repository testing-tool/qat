# Change Log

## 1.4.0

### General

#### *Added:*
- Qat now supports PySide and PyQt applications for all compatible Qt versions.

#### *Removed:*
- Qat does not support Qt 6.1 anymore.

#### *Changed:*
- Pip package generation now uses the [hatch](https://pypi.org/project/hatch/) build backend.

### GUI

#### *Added:*
- Ability to attach the Spy Window to an already running application.

### Python API

#### *Fixed:*
- Improved error messages when _qat.attach_to_application()_ function fails. (see [issue #24](https://gitlab.com/testing-tool/qat/-/issues/24))
- _qat.attach_to_application()_ can no use the application name instead of the process name.
- Qat applications can now be manually started without setting the 'TEMP' environment variable. (see [issue #25](https://gitlab.com/testing-tool/qat/-/issues/25))

## 1.3.0

### General

#### *Added:*
- Compatibility with Qt 6.8.

### Python API

#### *Added:*
- _qat.type_in()_ and _qat.shortcut()_ functions can now generate native events by using the _qat.Platform_ constant as the object definition. See API documentation for details.
- _qat.QApp_ constant can be used to interact with the _QGuiApplication_ instance and to take screenshots of the entire screen / desktop.

### Note

- __Support for Qt 6.1 will be removed in the next version (1.4.0).__

## 1.2.0

### General

#### *Added:*
- Support for Mac OS (both ARM and Intel architectures).

### Python API

#### *Fixed:*
- *qat.connect()* now supports signals with arguments (see [issue #26](https://gitlab.com/testing-tool/qat/-/issues/26)). 

## 1.1.2

### Python API

#### *Fixed:*
- Focus issue when using *qat.type_in()* function (see [issue #22](https://gitlab.com/testing-tool/qat/-/issues/22)).

## 1.1.1

### GUI

#### *Fixed:*
- User folder for Qat preferences is created if it does not exist.

## 1.1.0

### GUI
- GUI was completely rewritten and is now based on [CustomTkinter](https://customtkinter.tomschimansky.com/):
    - Themes are supported (system / light / dark),
    - More intuitive flow with Save/Cancel buttons,
    - Available methods are displayed in the property table ([issue #13](https://gitlab.com/testing-tool/qat/-/issues/13)),
    - Faster navigation in the Spy object tree and property table.

### Python API

#### *Changed:*
- All functions using an object _definition_ now accept a simple string that will be interpreted as an _objectName_.

#### *Added:*
- New method _list_methods()_ has been added to the QtObject class. Similar to the _list_properties()_ method, it returns a list of existing methods for an object.
- New classes to represent common Qt types, allowing to create instances of QPoint, QFont, QColor, QRect, QSize, etc. These instances can be used as properties and method arguments.
  ```python
  from qat.qt_types import *
  color = QColor("yellow", alpha=160)
  font = QFont("Times", pixelSize=16, bold=True, italic=False)
  rectangle = QRect(QPoint(10, 10), QSize(150, 50))
  ```
- New API function _qat.detach()_ allowing an application to keep running after a script terminates (see [issue #20](https://gitlab.com/testing-tool/qat/-/issues/20))

#### *Fixed:*
- Scripts hang if the application is not closed ([issue #20](https://gitlab.com/testing-tool/qat/-/issues/20)). Started applications are now automatically closed when a script terminates.