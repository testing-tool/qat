# (c) Copyright 2024, Qat’s Authors

"""
Define version for packaging system (Hatch)
"""

VERSION='@QAT_VERSION@'
