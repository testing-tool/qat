# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to object picker (Spy)
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_picker():
    """
    Verify that we can pick a widget
    """
    button_def = {
        'objectName': 'counterButton'
    }
    main_window = {
        'objectName': 'mainWindow'
    }
    picker_def = {
        'parent': main_window,
        'objectName': 'QatObjectPicker'
    }

    qat.activate_picker()

    button = qat.wait_for_object(button_def)
    qat.mouse_click(button)

    # Click should have been ignored by the button
    assert button.text == "0"

    # Picked object should be the button
    picker = qat.wait_for_object_exists(picker_def)
    picked_object = picker.pickedObject
    assert picked_object is not None
    assert not picked_object.is_null()

    # Picking can be temporarily disabled by holding the CTRL key
    qat.press_key(main_window, "<Control>")
    qat.mouse_click(button_def, modifier=qat.Modifier.CTL)
    qat.release_key(main_window, "<Control>")
    assert button.text == "1"

    qat.mouse_click(button)
    assert button.text == "1"

    # Verify that objects cannot be picked when Picker is de-activated
    qat.deactivate_picker()
    qat.mouse_click(button)
    picked_object = picker.pickedObject
    assert picked_object.is_null()

    # The click should be handled by the button
    assert button.text == "2"
