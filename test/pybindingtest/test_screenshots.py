# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to screenshots
"""

from pathlib import Path
import os
import shutil
import time

import pytest
from helpers import round_float_to_int
import qat
import qat.qt_types as Qt


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_screenshot(app_name):
    """
    Verify that we can take screenshot of the whole application
    """
    object_def = {}
    object_def['objectName'] = 'counterButton'

    screenshot_dir = os.getcwd() + f"/screenshots/{app_name}/test_screenshot"
    if os.path.exists(screenshot_dir):
        shutil.rmtree(screenshot_dir)

    # Make sure the main window is fully initialized and displayed
    time.sleep(2)

    qat.take_screenshot(screenshot_dir)

    assert os.path.exists(screenshot_dir)
    files = os.listdir(screenshot_dir)
    assert len(files) == 1


def test_grab_object(app_name):
    """
    Verify that we can take screenshot of a single widget
    """
    color_button_def = {
        'objectName': 'colorButton',
        'type': 'ColorButton'
    }

    screenshot_dir = os.getcwd() + f"/screenshots/{app_name}/test_grab_object/"
    if os.path.exists(screenshot_dir):
        shutil.rmtree(screenshot_dir)

    os.makedirs(screenshot_dir)

    file_path = Path(screenshot_dir) / 'test_grab.png'
    if os.path.exists(file_path):
        os.remove(file_path)

    button = qat.wait_for_object(color_button_def)
    button.color = Qt.QColor(red=80, green=149, blue=241)

    image = qat.grab_screenshot(button, delay=100)
    image.save(str(file_path))

    # Screenshot should have the same size as the widget
    assert image.width == round_float_to_int(button.width * button.pixelRatio)
    assert image.height == round_float_to_int(button.height * button.pixelRatio)

    pixel = image.getPixelRGBA(10, 10)
    assert pixel.red == 80
    assert pixel.green == 149
    assert pixel.blue == 241
    assert pixel.alpha == 255

    assert pixel[0] == 80
    assert pixel[1] == 149
    assert pixel[2] == 241
    assert pixel[3] == 255

    pixel = image.getPixel(10, 10)
    assert pixel == 0xFF5095F1

    # Close the app and re-open it
    qat.close_application(qat.current_application_context())
    qat.start_application(app_name, "")
    # Button should have its initial color
    assert button.color.name == '#ff000000'

    # But image should still be available (auto-reload)
    pixel = image.getPixel(10, 10)
    assert pixel == 0xFF5095F1

    # Save screenshot to file
    file_path = Path(screenshot_dir) / 'test_image.png'
    if os.path.exists(file_path):
        os.remove(file_path)

    image.save(str(file_path))
    assert os.path.exists(file_path)
