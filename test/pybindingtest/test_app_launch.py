# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to script application launch
"""
import pytest

import qat

# pylint: disable=protected-access

@pytest.fixture(autouse=True)
def teardown():
    """
    Clean up on exit
    """
    yield

    if qat.current_application_context():
        qat.close_application()


def test_script_launch(app_name, qt_version):
    """
    Verify that a PySide application can be launched
    """
    # Start an existing application
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0
    assert context._internal.process is not None
    assert context.is_running()

    assert qat.current_application_context() == context
    assert len(qat.get_context_list()) == 1
    assert qat.get_context_list()[0] == context
    assert context.qt_version == qt_version

    assert qat.close_application(context) != 0
    assert not context.is_running()
    assert len(qat.get_context_list()) == 0


def test_attach_to_app(app_name):
    """
    Verify that Qat can attach to a running application
    """
    button_def = {
        'objectName': 'counterButton',
        'type': 'QPushButton'
    }

    # Start the application to retrieve its path
    context1 = qat.start_application(app_name, "")
    assert context1 is not None
    assert context1.pid != 0
    assert context1._internal.process is not None
    pid = context1.pid
    button1 = qat.wait_for_object(button_def)
    qat.mouse_click(button1)

    # Attach by process ID
    context2 = qat.attach_to_application(pid)
    assert context2 is not None
    assert context2.pid == pid
    assert context2._internal.process is None  # process object is not available when attaching

    # Verify we can interact with the same application
    button2 = qat.wait_for_object(button_def)
    assert button2.text == '1'

    qat.mouse_click(button2)
    # Button should be updated in both contexts since they are attached to the same app
    assert button2.text == '2'
    assert button1.text == '2'


@pytest.mark.serial
def test_attach_by_name():
    """
    Verify that Qat can attach to an app by name
    """

    # Decrease timeout to make this test faster. Test applications are small and start quickly.
    qat.Settings.wait_for_app_start_timeout = 3000

    context1 = qat.start_application('PySideApp')
    context2 = qat.attach_to_application('PySideApp')
    assert context1.pid == context2.pid


def test_app_args(app_name):
    """
    Verify that arguments can be passed to the application
    """
    # Too many args
    with pytest.raises(Exception):
        qat.start_application(app_name, "-h --too-many")
    context = qat.start_application(app_name, '-h --too-many', detached=True)
    assert context.get_exit_code() == 2
    assert context.is_finished()
    assert context.pid > 0

    # Invalid arg due to quotes
    context = qat.start_application(app_name, '"-h --too-many"', detached=True)
    assert context.get_exit_code() == 1
    assert context.is_finished()
    assert context.pid > 0

    # Valid arg
    with pytest.raises(Exception):
        qat.start_application(app_name, "-h")
    context = qat.start_application(app_name, "-h", detached=True)
    assert context.get_exit_code() == 0
    assert context.is_finished()
    assert context.pid > 0
