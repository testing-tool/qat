# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to mouse
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_mouse_click():
    """
    Verify that we can click on widgets
    """
    # Simple button
    object_def = {}
    object_def['objectName'] = 'counterButton'
    counter_button = qat.wait_for_object(object_def)
    qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    # Right and middle buttons should have no effect
    qat.mouse_click(counter_button, button=qat.Button.RIGHT)
    assert counter_button.text == '1'
    qat.mouse_click(counter_button, button=qat.Button.MIDDLE)
    assert counter_button.text == '1'

    # Disabled button should not accept clicks
    counter_button.enabled = False
    counter_button = qat.wait_for_object_exists(object_def)
    with pytest.raises(Exception):
        qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    # Checkable button
    button_def = {
        'objectName': 'checkableButton'
    }
    button = qat.wait_for_object_exists(button_def)
    assert button.checkable
    assert not button.checked
    qat.mouse_click(button)
    assert button.checked
    qat.mouse_click(button)
    assert not button.checked

    # Button in popup
    open_popup_button = {
        'objectName': 'openButton'
    }
    popup_def = {
        'objectName': 'messageDialog'
    }
    close_button = {
        'container': popup_def,
        'type': 'QPushButton'
    }
    qat.mouse_click(open_popup_button)
    assert qat.wait_for_object(popup_def) is not None
    qat.wait_for_property_value(popup_def, 'visible', True, check=True)
    qat.mouse_click(close_button)
    qat.wait_for_property_value(popup_def, 'visible', False, check=True)

    # Color button
    object_def['objectName'] = 'colorButton'
    color_button = qat.wait_for_object(object_def)
    assert color_button.color.name == '#ff000000'  # black

    # Default (=Left) click
    qat.mouse_click(color_button)
    assert color_button.color.name == '#ff008000'  # green

    # Right click
    qat.mouse_click(color_button, button=qat.Button.RIGHT)
    assert color_button.color.name == '#ffff0000'  # red

    # Middle click
    qat.mouse_click(color_button, button=qat.Button.MIDDLE)
    assert color_button.color.name == '#ff0000ff'  # blue

    # Left click
    qat.mouse_click(color_button, button=qat.Button.LEFT)
    assert color_button.color.name == '#ff008000'  # green

    # Invalid click
    with pytest.raises(Exception):
        qat.mouse_click(color_button, button=qat.Button.NONE)
    assert color_button.color.name == '#ff008000'  # green


def test_mouse_modifiers():
    """
    Verify that we can use keyboard modifiers when clicking
    """
    button_def = {
        'objectName': 'modifierButton'
    }
    button = qat.wait_for_object(button_def)
    assert button.text == ''

    qat.mouse_click(button, modifier=qat.Modifier.ALT)
    assert button.text.strip() == 'ALT'

    qat.mouse_click(button, modifier=qat.Modifier.CTL)
    assert button.text.strip() == 'CTRL'

    qat.mouse_click(button, modifier=qat.Modifier.SHIFT)
    assert button.text.strip() == 'SHIFT'

    qat.mouse_click(button, modifier=[qat.Modifier.CTL, qat.Modifier.ALT])
    assert button.text.strip() == 'CTRL ALT'

    qat.mouse_click(button, modifier=[qat.Modifier.ALT, qat.Modifier.SHIFT])
    assert button.text.strip() == 'SHIFT ALT'


def test_mouse_click_position():
    """
    Verify that we can click at a specific position
    """
    # Simple button
    counter_button = qat.wait_for_object('counterButton')
    qat.mouse_click(counter_button)
    assert counter_button.text == '1'

    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=1000, y=1000)
    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=-10, y=-65)
    assert counter_button.text == '1'

    # Test bounds
    with pytest.raises(Exception):
        qat.mouse_click(counter_button, x=counter_button.width + 2, y=counter_button.height + 2)
    
    # Test specific position
    qat.mouse_click(counter_button, x=15, y=15)
    assert counter_button.text == '2'


def test_mouse_press_release():
    """
    Verify that we can press and release mouse buttons
    """
    # Simple button
    object_def = 'counterButton'
    counter_button = qat.wait_for_object(object_def)
    assert counter_button.text == '0'
    qat.mouse_press(counter_button)
    assert counter_button.text == '0'
    qat.mouse_release(counter_button)
    assert counter_button.text == '1'
