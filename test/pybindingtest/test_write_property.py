# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to property writing
"""

import pytest
import qat
import qat.qt_types as Qt

@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_set_object_property():
    """
    Verify that we can write remote properties
    """

    # Boolean
    button = qat.wait_for_object_exists('counterButton')
    assert button.enabled

    button.enabled = False
    assert not button.enabled

    # Int
    button.autoRepeatDelay = 10
    assert button.autoRepeatDelay == 10

    # Write invalid property
    with pytest.raises(Exception):
        button.not_a_property = False

    # Set invalid value
    with pytest.raises(Exception):
        button.autoRepeatDelay = "avoid conversion"
    assert button.autoRepeatDelay == 10


def test_set_color():
    """
    Verify that we can access properties defined in Python
    """
    button_def = {
        'objectName' : 'colorButton',
        'type' : 'ColorButton'
    }
    button = qat.wait_for_object(button_def)

    # Set by object definition with name
    button.color = Qt.QColor('#FF5095F1')
    assert str(button.color.name).upper() == '#FF5095F1'

    # Set by object definition with channels
    button.color = Qt.QColor(red=10, green=20, blue=50)
    assert button.color.red == 10
    assert button.color.green == 20
    assert button.color.blue == 50

    # Set by color name
    button.color = "white"
    assert str(button.color.name).upper() == "#FFFFFFFF"
    assert button.color.red == 255
    assert button.color.green == 255
    assert button.color.blue == 255

