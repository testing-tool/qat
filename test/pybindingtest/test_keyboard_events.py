# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to keyboard
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_simple_text():
    """
    Verify that we can type text in fields
    """
    text_field_def = {
        'objectName': 'edtableLine',
        'type': 'QLineEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    text_field_def = {
        'objectName': 'readOnlyLine',
        'type': 'QLineEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    with pytest.raises(RuntimeWarning):
        qat.type_in(text_field, "Bye")
    assert text_field.text == ""

    open_window_button = qat.wait_for_object('counterButton')
    with pytest.raises(RuntimeWarning):
        qat.type_in(open_window_button, "test")


def test_special_keys():
    """
    Verify that we can use special key such as Del, Backspace, ...
    """
    text_field_def = {
        'objectName': 'edtableLine',
        'type': 'QLineEdit'
    }
    readonly_text_field_def = {
        'objectName': 'readOnlyLine',
        'type': 'QLineEdit'
    }
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    # Backspace
    qat.type_in(text_field, "<Backspace><Backspace><Backspace>")
    assert text_field.text == "He"
    qat.type_in(text_field, "<Backspace><Backspace><Backspace>")
    assert text_field.text == ""

    # Return/Enter
    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"
    qat.type_in(text_field, "<Return>")
    read_only_field = qat.wait_for_object(readonly_text_field_def)
    assert read_only_field.text == "Hello"

    qat.type_in(text_field, "World")
    assert text_field.text == "HelloWorld"
    qat.type_in(text_field, "<Enter>")
    read_only_field = qat.wait_for_object(readonly_text_field_def)
    assert read_only_field.text == "HelloWorld"


@pytest.mark.serial
def test_shortcuts():
    """
    Verify that shortcuts can be activated
    """
    text_field_def = 'edtableLine'
    text_field = qat.wait_for_object(text_field_def)
    assert text_field.text == ""

    qat.type_in(text_field, "Hello")
    assert text_field.text == "Hello"

    # Standard shortcuts
    qat.shortcut(text_field, 'Ctrl+Z')
    assert text_field.text == ""

    qat.shortcut(text_field, 'Ctrl+Shift+Z')
    assert text_field.text == "Hello"

    text_field.selectAll()
    qat.shortcut(text_field, 'Ctrl+C')
    qat.shortcut(text_field, 'Ctrl+Z')
    assert text_field.text == ""
    qat.shortcut(text_field, 'Ctrl+V')
    assert text_field.text == "Hello"

    # Custom shortcuts
    button_def = {
        'objectName': 'openButton'
    }

    popup_def = {
        'objectName': 'messageDialog'
    }

    qat.shortcut(button_def, 'Ctrl+O')

    popup = qat.wait_for_object(popup_def)
    assert popup.visible
