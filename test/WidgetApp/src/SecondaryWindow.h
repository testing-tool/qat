// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <CustomWidgets.h>

#include <QMainWindow>

class QAction;
class QContextMenuEvent;
class QMenu;

class SecondaryWindow : public QMainWindow
{
public:
   /// Default constructor
   SecondaryWindow();

   /// Default destructor
   ~SecondaryWindow() override = default;

   /// Handle context menu events
   /// \param[in] event A QContextMenuEvent
   void contextMenuEvent(QContextMenuEvent* event) override;

   /// Actions of the context menu
   /// @{
   QAction* mEnableAction;
   QAction* mVisibleAction;
   /// @}
private:
   /// Context menu to be displayed
   QMenu* mContextMenu{nullptr};
};
