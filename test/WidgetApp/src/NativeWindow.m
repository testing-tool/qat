// (c) Copyright 2024, Qat’s Authors

#import <Cocoa/Cocoa.h>

@interface CounterButton : NSButton
{
    @public NSInteger counter;
}
@end

@implementation CounterButton

- (void)buttonClicked:(id)sender
{
    NSEvent* event = [NSApp currentEvent];
    switch (event.clickCount)
    {
        case 1:
            ++counter;
            break;
        case 2:
            counter = 0;
            break;
        default:
            break;
    }
    [self setTitle:[NSString stringWithFormat: @"%ld", counter]];
}

@end

void populateCocoaWindow(unsigned long handle)
{
    NSView* window = (__bridge NSView*)handle;
    if (!window)
    {
        NSLog(@"Could not create Cocoa window");
        return;
    }

    NSTextField* textField = [[NSTextField alloc] initWithFrame:NSMakeRect(10, 10, 200, 50)];
    [window addSubview:textField];

    CounterButton* button = [[CounterButton alloc] initWithFrame:NSMakeRect(10, 100, 200, 50)];
    [button setTitle:[NSString stringWithFormat: @"%ld", button->counter]];
    [button setTarget:button];
    [button setAction:@selector(buttonClicked:)];
    [window addSubview:button];
}
