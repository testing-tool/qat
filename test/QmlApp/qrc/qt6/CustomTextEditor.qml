// (c) Copyright 2024, Qat’s Authors

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item 
{
   id: root
   property alias text: label.text

   Label 
   {
      id: label
      anchors.fill: parent

      MouseArea
      {
         anchors.fill: parent
         onClicked: function(mouse) 
         {
            textField.visible = true
            textField.text = label.text
            label.visible = false
            textField.focus = true
         }
      }
   }

   TextField 
   {
      id: textField
      anchors.fill: parent
      visible: false

      onAccepted: function()
      {
         label.text = textField.text
         textField.visible = false
         label.visible = true
      }

      onEditingFinished: function()
      {
         label.text = textField.text
         textField.visible = false
         label.visible = true
      }

      Keys.onEscapePressed: {
         textField.text = label.text
         textField.focus = false
         label.visible = true
    }
   }
}