// (c) Copyright 2023, Qat’s Authors

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle
{
   id: root
   color: "white"
   Item
   {
      // Item is added to verify nested clickable elements
      anchors.fill: parent
      MouseArea
      {
         anchors.fill: parent
         acceptedButtons: Qt.AllButtons
         objectName: root.objectName + "Area"

         onClicked: function(mouse)
         {
            if (mouse.button == Qt.LeftButton)
               root.color = "green"
            else if (mouse.button == Qt.RightButton)
               root.color = "red"
            else if (mouse.button == Qt.MiddleButton)
               root.color = "black"
         }

         onDoubleClicked: function(mouse)
         {
            if (mouse.button == Qt.LeftButton)
               root.color = "blue"
            else if (mouse.button == Qt.RightButton)
               root.color = "yellow"
            else if (mouse.button == Qt.MiddleButton)
               root.color = "grey"
         }
      }
   }
}