﻿// (c) Copyright 2023, Qat’s Authors

import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Popup
{
   id: pop
   modal: true

   closePolicy: Popup.CloseOnEscape

   width: 700
   height: 400

   x: (parent.width - width) / 2
   y: (parent.height - height) / 2
   padding: 3

   // The message to display
   property alias message: messageText.text
   property alias title: title.text

   // Content alias
   default property alias iconItem: placeholder.data

    // Private constants that are only used in Popup.
   QtObject
   {
      id: internal
      readonly property int popUpHorizontalOffset: 8
      readonly property int popUpVerticalOffset: 8
      readonly property int popUpRadius: 12
      readonly property int popUpSamples: 17
      readonly property int popUpSpread: 0
      readonly property real popUpOpacity: 0.5
      readonly property int popUpBorderSize: 0
      readonly property int popUpIconWidth: 200
      readonly property real popUpIconHeight: 200
   }


   // Customize the background
   background: Rectangle
   {
      anchors.fill: parent
      radius: 3
      border.width: internal.popUpBorderSize
      color: "white"
      border.color: "white"
   }

   ColumnLayout
   {
      anchors.fill: parent
      Label
      {
         id: title
         objectName: "title"
         text: "Message"
         color: "#2b4c70"
         Layout.alignment: Qt.AlignTop
         Layout.leftMargin: 16
      }
      Rectangle
      {
         id: mainRect
         color: "#2b4c70"
         Layout.fillWidth: true
         Layout.fillHeight: true
         ColumnLayout
         {
            anchors.fill : parent
            Layout.alignment: Qt.AlignCenter
            RowLayout
            {
               Layout.margins: 16
               spacing: 32
               Item
               {  
                  id : placeholder 
                  Layout.preferredWidth: internal.popUpIconWidth
                  Layout.preferredHeight: internal.popUpIconHeight
               }
               Text
               {
                  id: messageText
                  objectName: "message"
                  Layout.fillWidth: true
                  color: "white"
                  wrapMode: Text.Wrap
               }
            }
         }
      }
      ListView
      {
         id: listView
         objectName: "popupListView"
         Layout.fillWidth: true
         boundsBehavior: Flickable.StopAtBounds
         height: 50
         model: 50
         delegate: Rectangle
         {
            width: element.width
            height: element.height
            Text
            {
               id: element
               text: "item " + modelData
               color: listView.currentIndex == index ? "red" : "black"
            }
            MouseArea 
            {
               anchors.fill: parent
               onClicked: listView.currentIndex = index
               preventStealing: true
            }
         }

         ScrollBar.vertical: ScrollBar
         {
            objectName: "popupListScrollBar"
         }
      }
      Button
      {
         objectName: pop.objectName + "CloseButton"
         text: "Close"
         onClicked: pop.close()
      }
   }
}