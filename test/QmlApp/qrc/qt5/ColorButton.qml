// (c) Copyright 2023, Qat’s Authors

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle
{
   id: root
   color: "white"
   Item
   {
      // Item is added to verify nested clickable elements
      anchors.fill: parent
      MouseArea
      {
         anchors.fill: parent
         acceptedButtons: Qt.AllButtons
         objectName: root.objectName + "Area"

         onClicked: function(mouse)
         {
            if (mouse.button == Qt.LeftButton)
               root.color = "green"
            else if (mouse.button == Qt.RightButton)
               root.color = "red"
            else if (mouse.button == Qt.MidButton)
               root.color = "black"
         }

         onDoubleClicked: function(mouse)
         {
            if (mouse.button == Qt.LeftButton)
               root.color = "blue"
            else if (mouse.button == Qt.RightButton)
               root.color = "yellow"
            else if (mouse.button == Qt.MidButton)
               root.color = "grey"
         }
      }
   }
}