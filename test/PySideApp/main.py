# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Simple PySide application for testing purposes
"""

import sys

from PySide6.QtCore import Qt, QSize, QTimer, Property
from PySide6.QtGui import QColor
from PySide6.QtWidgets import (
    QApplication,
    QMessageBox,
    QMainWindow,
    QLineEdit,
    QPushButton,
    QWidget,
    QHBoxLayout,
    QVBoxLayout
)


class ColorButton(QPushButton):
    """
    Square button changing color when clicked
    """
    def __init__(self):
        super().__init__()
        self.setFixedSize(QSize(50, 50))
        self._color = QColor('black')
        self.setStyleSheet('background-color: black; border:none')

    @Property(QColor)
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = QColor(value)
        self.setStyleSheet(f'background-color: {self._color.name()}; border:none')

    def mouseReleaseEvent(self, e):
        if e.button() == Qt.MouseButton.LeftButton:
            self.color = 'green'

        elif e.button() == Qt.MouseButton.MiddleButton:
            self.color = 'blue'

        elif e.button() == Qt.MouseButton.RightButton:
            self.color = 'red'


class ModifierButton(QPushButton):
    """
    Button displaying modifiers when clicked
    """
    def mousePressEvent(self, e):
        text = ''
        if e.modifiers() & Qt.ControlModifier:
            text += 'CTRL '
        if e.modifiers() & Qt.ShiftModifier:
            text += 'SHIFT '
        if e.modifiers() & Qt.AltModifier:
            text += 'ALT '

        self.setText(text)


class MainWindow(QMainWindow):
    """
    Main Window
    """
    def __init__(self):
        super().__init__()

        self.setWindowTitle("PySide App")
        self.setObjectName('mainWindow')

        main_layout = QHBoxLayout()
        left_column = QVBoxLayout()
        right_column = QVBoxLayout()

        # Left column

        # Checkable button
        check_button = QPushButton("unchecked")
        check_button.setFixedWidth(250)
        check_button.setCheckable(True)
        check_button.setObjectName("checkableButton")
        check_button.clicked.connect(lambda checked: check_button.setText('checked' if checked else 'unchecked'))
        left_column.addWidget(check_button)

        # Counter button
        counter = 0
        counter_button = QPushButton("0")
        counter_button.setFixedWidth(250)
        counter_button.setObjectName("counterButton")
        def increment():
            nonlocal counter
            counter += 1
            counter_button.setText(f'{counter}')
        counter_button.clicked.connect(increment)
        left_column.addWidget(counter_button)

        # Modifier button
        modifier_button = ModifierButton()
        modifier_button.setFixedWidth(250)
        modifier_button.setObjectName("modifierButton")
        left_column.addWidget(modifier_button)

        # Popup button
        popup_button = QPushButton("Open popup")
        popup_button.setFixedWidth(250)
        popup_button.setObjectName("openButton")
        popup_button.setShortcut("Ctrl+O")
        left_column.addWidget(popup_button)

        dlg = QMessageBox(self)
        dlg.setObjectName('messageDialog')
        dlg.setWindowTitle('Popup')
        dlg.setText('Hello')

        def open_dlg():
            dlg.exec()
        popup_button.clicked.connect(lambda: QTimer.singleShot(0, open_dlg))

        # Color button
        color_button = ColorButton()
        color_button.setObjectName("colorButton")
        left_column.addWidget(color_button)

        # Right column

        # Editable line
        edit_line = QLineEdit()
        edit_line.setObjectName('edtableLine')
        edit_line.setFixedWidth(300)
        right_column.addWidget(edit_line)

        # Read-only line
        ro_line = QLineEdit()
        ro_line.setObjectName('readOnlyLine')
        ro_line.setFixedWidth(300)
        ro_line.setReadOnly(True)
        right_column.addWidget(ro_line)

        edit_line.returnPressed.connect(lambda: ro_line.setText(edit_line.text()))

        # Configure window layouts
        self.setFixedSize(QSize(700, 600))
        main_widget = QWidget()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)
        main_layout.addLayout(left_column)
        main_layout.addLayout(right_column)
        left_column.addStretch()
        right_column.addStretch()


if len(sys.argv) > 2:
    sys.exit(2)
if len(sys.argv) == 2:
    if sys.argv[1] != '-h':
        print('Invalid argument: ' + sys.argv[1])
        sys.exit(1)
    print('Usage: python main.py [-h]')
    sys.exit(0)

app = QApplication(sys.argv)

window = MainWindow()
window.show()

sys.exit(app.exec())
