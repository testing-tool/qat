Feature: Second example feature

   Just a second example

   Background: 
      Given current test name is Simple example 2

   @fixture.app.autoclose
   Scenario: Auto quit scenario
      Given the QmlApp application is running
         When Clicking on the Counter button
            Then the text of the Counter button is 1

   @fixture.app.autoclose
   Scenario: Colored buttons left only
      Given the QmlApp application is running
         When Clicking on each Colored button with the left button
            Then each Colored button is colored in green
