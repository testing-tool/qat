# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Given steps implementation
"""

# pylint: disable = no-name-in-module
# pylint: disable = missing-function-docstring
# pylint: disable = function-redefined

from behave import Given
import qat

@Given("current test name is {name}")
def step(_, name):
    qat.get_state().current_report.log(f"Test name is '{name}'")


@Given("the {name} application is running")
def step(context, name):
    context.userData['appCtxt'] = qat.start_application(name)
