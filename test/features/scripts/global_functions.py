# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Global functions to be shared between step implementations
"""

def strip_object_name(name: str) -> str:
    """
    Remove 'the' from the given name
    """
    if name.lower().startswith('the '):
        name = name[4:]

    return name
