# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to the wait_for function
"""

import pytest

import qat
from qat.test_settings import Settings


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_find_object_by_name():
    """
    Verify that we can use the wait_for function to identify objects
    """
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    reached = qat.wait_for(
        lambda: qat.wait_for_object_exists(object_def).text == "0",
        timeout=Settings.wait_for_object_timeout
    )
    assert reached

    qat.mouse_click(object_def)

    reached = qat.wait_for(
        lambda: qat.wait_for_object_exists(object_def).text == "1"
    )
    assert reached

    # Negative test
    reached = qat.wait_for(
        lambda: qat.wait_for_object_exists(object_def).text == "2"
    )
    assert not reached
