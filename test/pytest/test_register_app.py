# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to application registering
"""

import os
import pytest
import qat

@pytest.fixture(autouse=True)
def teardown(app_name, mocker):
    """
    Run tests in another directory to manage configurations
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='This test does not depend on application type and should only run once')

    mocker.patch(
        'os.getcwd',
        return_value = './build/temp')
    yield

    for app in qat.list_applications():
        qat.unregister_application(app)


def test_local_config():
    """
    Verify that an application can be (un)registered in the local configuration
    """
    # No configuration file should exist
    local_file = qat.get_config_file(shared=False)
    assert not os.path.exists(local_file)
    # Default file should be the local one
    assert qat.get_config_file() == local_file

    qat.register_application('test_app', '../build/app', '--arg 1')
    apps = qat.list_applications()
    assert 'test_app' in apps
    assert apps['test_app']['path'] == '../build/app'
    assert apps['test_app']['args'] == '--arg 1'

    # Config file should have been created
    assert qat.get_config_file() == local_file
    assert os.path.exists(local_file)

    qat.unregister_application('test_app')
    apps = qat.list_applications()
    assert 'test_app' not in apps

    # Config file should have been deleted
    assert qat.get_config_file() == local_file
    assert not os.path.exists(local_file)


def test_global_config():
    """
    Verify that an application can be (un)registered in the global configuration
    """
    # Shared file should not exist
    shared_file = qat.get_config_file(shared=True)
    assert not os.path.exists(shared_file)

    qat.register_application('test_app', '../build/app', '--arg 1', True)
    apps = qat.list_applications()
    assert 'test_app' in apps
    assert apps['test_app']['path'] == '../build/app'
    assert apps['test_app']['args'] == '--arg 1'

    # Config file should have been created
    assert qat.get_config_file() == shared_file
    assert os.path.exists(shared_file)

    qat.unregister_application('test_app', True)
    apps = qat.list_applications()
    assert 'test_app' not in apps

    # Config file should have been deleted
    assert qat.get_config_file() != shared_file
    assert not os.path.exists(shared_file)


def test_config_merge():
    """
    Verify that local and global configurations are properly handled
    """
    # No configuration file should exist
    local_file = qat.get_config_file(shared=False)
    assert not os.path.exists(local_file)
    shared_file = qat.get_config_file(shared=True)
    assert not os.path.exists(shared_file)
    # Default file should be the local one
    assert qat.get_config_file() == local_file

    # Register the same app in both configurations
    qat.register_application('test_app', '../build/global', shared = True)
    qat.register_application('test_app', '../build/local', '--arg 1',shared = False)

    # Both files should have been created
    assert os.path.exists(local_file)
    assert os.path.exists(shared_file)
    # Default file should be the local one
    assert qat.get_config_file() == local_file

    # Local configuration takes precedence
    apps = qat.list_applications()
    assert 'test_app' in apps
    assert apps['test_app']['path'] == '../build/local'
    assert apps['test_app']['args'] == '--arg 1'
    assert qat.get_application_path('test_app') == '../build/local'

    # unregister local app
    qat.unregister_application('test_app', shared=False)
    apps = qat.list_applications()
    assert 'test_app' in apps
    assert apps['test_app']['path'] == '../build/global'
    assert apps['test_app']['args'] == ''
    assert qat.get_application_path('test_app') == '../build/global'

    # Local file should have been deleted
    assert not os.path.exists(local_file)
    assert os.path.exists(shared_file)
    # Default file should be the shared one
    assert qat.get_config_file() == shared_file

    # Register another app
    qat.register_application('test_app2', '../build/local2',shared = False)
    apps = qat.list_applications()
    assert 'test_app' in apps
    assert 'test_app2' in apps

    # unregister all apps
    qat.unregister_application('test_app', shared=True)
    qat.unregister_application('test_app2', shared=False)
    apps = qat.list_applications()
    assert 'test_app' not in apps
    assert 'test_app2' not in apps

    # Both files should have been deleted
    assert not os.path.exists(local_file)
    assert not os.path.exists(shared_file)
    # Default file should be the local one
    assert qat.get_config_file() == local_file
    