# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Helper functions for tests
"""

from decimal import Decimal
import decimal


def round_float_to_int(value: float) -> int:
    """
    Return the given value rounded to the nearest integer (using ROUND_HALF_UP strategy)
    """
    decimal.getcontext().rounding = decimal.ROUND_HALF_UP
    return int(round(Decimal(str(value)), 0))
