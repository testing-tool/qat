# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to property reading
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_get_object_property(app_name):
    """
    Verify that we can read remote properties
    """

    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'

    button = qat.wait_for_object(object_def)
    assert button.text == "0"
    assert button.enabled
    assert button.property('text') == "0"
    assert button.property('enabled')

    with pytest.raises(Exception):
        assert button.not_a_property is None

    # ID is not a real property and is QML-specific
    if app_name == "QmlApp":
        object_def = {}
        object_def['objectName'] = 'rightColumn'
        object_def['type'] = 'ColumnLayout'

        column = qat.wait_for_object(object_def)
        assert column.id == 'spacer'


def test_get_children():
    """
    Verify that we can access the 'children' virtual property
    """
    row_def = {
        'objectName': "mainRowLayout"
    }
    parent = qat.wait_for_object_exists(row_def)

    children = parent.children
    assert children is not None
    assert 0 != len(children)

    for child in children:
        assert child is not None
        assert isinstance(child['children'], bool)
        qat.wait_for_object_exists(child)


def test_list_properties():
    """
    Verify that we can list all the properties of an object
    """
    object_def = {}
    object_def['objectName'] = 'counterButton'
    object_def['type'] = 'Button'

    expected_properties = [
        "id",
        "enabled",
        "text",
        "objectName",
        "visible",
        "width"
    ]
    button = qat.wait_for_object(object_def)
    all_properties = dict(button.list_properties())
    for prop in expected_properties:
        assert prop in all_properties


def test_bounds(app_name):
    """
    Verify that the globalBounds property uses global coordinates
    """
    main_window_def = {
        'objectName': 'mainWindow',
        'type': 'ApplicationWindow'
    }

    main_window = qat.wait_for_object(main_window_def)
    button = qat.wait_for_object({'objectName': 'counterButton'})
    x_offset = 0

    # Weird behavior of Qt < 6.3 where the x property does not match the global position
    if app_name == 'WidgetApp' and qat.app_launcher.is_windows():
        qt_version = str(qat.current_application_context().qt_version)
        version_numbers = [int(n) for n in str(qt_version).split('.')]
        if version_numbers[0] < 6 or (version_numbers[0] == 6 and version_numbers[1] < 3):
            x_offset = 1
    assert main_window.x == main_window.globalBounds.x - x_offset
    # y property refers to the window frame, including the title bar for QWidgets
    assert main_window.y <= main_window.globalBounds.y

    assert main_window.globalBounds.width == main_window.width
    assert main_window.globalBounds.height == main_window.height
    assert button.globalBounds.width == button.width
    assert button.globalBounds.height == button.height

    # Make sure global bounds are computed correctly
    local_x = button.x
    local_y = button.y
    if app_name == 'QmlApp':
        # Button is in a parent layout
        local_x += button.parent.x
        local_y += button.parent.y
    assert button.globalBounds.x == main_window.globalBounds.x + local_x
    assert button.globalBounds.y == main_window.globalBounds.y + local_y

    # pos and size properties exist in QWidgets only
    if app_name == 'WidgetApp':
        # Make sure all position-related properties are consistent
        assert main_window.x == main_window.pos.x
        assert main_window.y == main_window.pos.y
        assert button.x == button.pos.x
        assert button.y == button.pos.y

        # Make sure all size-related properties are consistent
        assert main_window.globalBounds.width == main_window.size.width
        assert main_window.globalBounds.height == main_window.size.height

        assert button.globalBounds.width == button.size.width
        assert button.globalBounds.height == button.size.height
