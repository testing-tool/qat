# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to Qt object wrappers
"""

from copy import deepcopy
import pytest
import qat

app = None

@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    global app
    app = app_name

    yield

    try:
        qat.close_application()
    except ProcessLookupError:
        pass


def test_object():
    """
    Test the QtObject class
    """
    qat.close_application()
    # Empty object without any application running
    no_ctxt_object = qat.QtObject(None, None)
    assert no_ctxt_object._get_app_context() is None
    assert no_ctxt_object.get_definition() is None
    assert no_ctxt_object.is_null()

    # Current context should be automatically given to the object
    qat.start_application(app)
    assert no_ctxt_object._get_app_context() is not None
    assert no_ctxt_object.get_definition() is None
    assert no_ctxt_object.is_null()

    with pytest.raises(AttributeError):
        no_ctxt_object.list_properties()

    # Serialization of unnamed objects
    unnamed_object_def = {
        'type': 'Text',
        'id': 'textWithNoName'
    }
    unnamed_object = qat.QtObject(None, unnamed_object_def)
    assert str(unnamed_object) == 'textWithNoName'

    unnamed_object_def = {
        'type': 'Text',
        'text': 'no_name_or_id'
    }
    unnamed_object = qat.QtObject(None, unnamed_object_def)
    assert str(unnamed_object) == '<unnamed>'

    # Verify cache access
    assert unnamed_object['type'] == 'Text'


def test_custom_object():
    """
    Test the QtCustomObject class
    """

    public_attributes = {
        'x': 0,
        'y': 1,
        'z': 2
    }

    # QVariantType[Name] are used internally
    internal_attributes = deepcopy(public_attributes)
    internal_attributes.update({'QVariantType': 5000})
    internal_attributes.update({'QVariantTypeName': 'Point'})

    custom_object = qat.QtCustomObject(internal_attributes)
    same_object = qat.QtCustomObject(internal_attributes)
    copy_object = deepcopy(custom_object)

    # Comparisons
    assert custom_object is not None
    assert custom_object == public_attributes
    assert str(custom_object) == str(public_attributes)
    assert custom_object == same_object
    assert custom_object == copy_object

    # Custom object behaves like a container
    assert len(custom_object) == len(public_attributes)
    for key in public_attributes:
        assert key in custom_object
    for item in custom_object:
        assert item in public_attributes

    assert custom_object['x'] == 0
    assert custom_object['y'] == 1
    assert custom_object['z'] == 2

    assert custom_object[0] == 0
    assert custom_object[1] == 1
    assert custom_object[2] == 2

    # Verifying attributes
    assert custom_object.get_attribute_dict() == internal_attributes
    assert custom_object.typeId == 5000
    assert custom_object.type == 'Point'
    with pytest.raises(AttributeError):
        assert custom_object.invalid_attribute == 0

    with pytest.raises(AttributeError):
        custom_object.invalid_attribute = 0

    custom_object.x = 5
    assert custom_object[0] == 5

    with pytest.raises(AttributeError):
        custom_object[1] = 8
    