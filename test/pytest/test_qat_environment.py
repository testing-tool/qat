
# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to qat environment
"""

import os
import random
from pathlib import Path

import pytest

from qat.internal import qat_environment


def test_get_temp_folder(mocker):
    """
    Verify that the directory used for temporary file is returned
    """

    # Verifies the temporary directory from the environment variable is returned
    mocker.patch.dict('os.environ', {'TEMP': "temp_folder_from_env"})
    assert qat_environment.get_temp_folder() == Path("temp_folder_from_env")

    # Verifies the temporary directory is determined and returned when it is not in the
    # environment variable
    mocker.patch.dict('os.environ', {}, clear=True)

    temp_folder_result = qat_environment.get_temp_folder()

    assert os.path.isdir(temp_folder_result), f"{temp_folder_result} is not a directory"
    assert os.access(temp_folder_result, os.W_OK), f"{temp_folder_result} is not writable"
    assert Path(os.environ['TEMP']) == temp_folder_result


def test_create_qat_config_file_path():
    """
    Verify that the path to qat configuration file is properly created
    """

    with pytest.raises(NameError):
        qat_environment.create_qat_config_file_path(-1)

    random_pid = random.randint(1, 100)
    expected_qat_config_path = qat_environment.get_temp_folder() / f"qat-{random_pid}.txt"

    assert qat_environment.create_qat_config_file_path(random_pid) == expected_qat_config_path
