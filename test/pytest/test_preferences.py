# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to qat preferences
"""

import json
import uuid
import pytest

from qat.internal import preferences


def test_qat_preferences(tmp_path, mocker):
    """
    Verify that the preferences can be loaded/saved/updated/removed when not read only
    """
    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    preferences_json = {
        "GUI": {
            "picker_color": "yellow",
            "theme": "dark"
        },
        "setting": "string_value",
        "list_setting": [1, 2, 3]
    }

    # Create the json file
    with open(preferences_path, "w",  encoding='utf-8') as file:
        json.dump(preferences_json, file, indent = 3)

    assert preferences_path.is_file()

    json_dump_spy = mocker.spy(json, "dump")

    # Load and save the preferences at the end of the statement (RAII)
    with preferences.Preferences(preferences_path, is_read_only=False) as loaded_preferences:
        # Ensure the value are properly loaded and are accessible using dot notation
        assert loaded_preferences.GUI.picker_color == "yellow"
        assert loaded_preferences.GUI.theme == "dark"
        assert loaded_preferences.setting == "string_value"
        assert loaded_preferences.list_setting == [1, 2, 3]

        # Update the values of preferences
        loaded_preferences.GUI.picker_color = "red"
        loaded_preferences.GUI.theme = "light"
        loaded_preferences.setting = "string_updated_value"
        loaded_preferences.list_setting = [4, 5, 6]

        assert loaded_preferences.GUI.picker_color == "red"
        assert loaded_preferences.GUI.theme == "light"
        assert loaded_preferences.setting == "string_updated_value"
        assert loaded_preferences.list_setting == [4, 5, 6]

        # Add new preference
        loaded_preferences.new_setting = "New_Setting"
        assert loaded_preferences.new_setting == "New_Setting"

    json_dump_spy.assert_called_once()

    # Reload and check the values were saved
    with preferences.Preferences(preferences_path, is_read_only=False) as loaded_preferences:
        assert loaded_preferences.GUI.picker_color == "red"
        assert loaded_preferences.GUI.theme == "light"
        assert loaded_preferences.setting == "string_updated_value"
        assert loaded_preferences.list_setting == [4, 5, 6]

        # Removing some preferences
        del loaded_preferences.GUI
        del loaded_preferences.setting
        del loaded_preferences.list_setting

        assert len(loaded_preferences) == 1

    # Reload and check the preferences were properly deleted
    with preferences.Preferences(preferences_path, is_read_only=False) as loaded_preferences:
        assert len(loaded_preferences) == 1

        # Remove all preferences
        del loaded_preferences.new_setting
        assert len(loaded_preferences) == 0

    # The preferences file is removed when there are no preferences
    assert not preferences_path.is_file()


def test_qat_settings_readonly(tmp_path, mocker):
    """
    Verify that the preferences can only be loaded when read only
    """
    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    preferences_json = {
        "GUI": {
            "picker_color": "yellow",
            "theme": "dark"
        },
        "setting": "string_value",
        "list_setting": [1, 2, 3]
    }

    # Create the json file
    with open(preferences_path, "w",  encoding='utf-8') as file:
        json.dump(preferences_json, file, indent = 3)

    assert preferences_path.is_file()

    json_dump_spy = mocker.spy(json, "dump")

    # Load the settings
    with preferences.Preferences(preferences_path, is_read_only=True) as loaded_preferences:
        # Ensure the value are properly loaded
        assert loaded_preferences.GUI.picker_color == "yellow"
        assert loaded_preferences.GUI.theme == "dark"
        assert loaded_preferences.setting == "string_value"
        assert loaded_preferences.list_setting == [1, 2, 3]

        # Trying to remove preferences
        with pytest.raises(AttributeError):
            del loaded_preferences.GUI.picker_color

        with pytest.raises(AttributeError):
            del loaded_preferences.GUI.list_setting

        with pytest.raises(AttributeError):
            del loaded_preferences.setting

        # Trying to update preferences
        with pytest.raises(AttributeError):
            loaded_preferences.GUI.picker_color = "red"

        with pytest.raises(AttributeError):
            loaded_preferences.GUI.list_setting = [4, 5, 6]

        with pytest.raises(AttributeError):
            loaded_preferences.setting = "Updated_Is_Not_Allowed"

        # Trying to add preferences
        with pytest.raises(AttributeError):
            loaded_preferences.GUI.new_dict_entry = "red"

        with pytest.raises(AttributeError):
            loaded_preferences.GUI.new_list_setting = [4, 5, 6]

        with pytest.raises(AttributeError):
            loaded_preferences.add_new_setting = "Adding_Is_Not_Allowed"

    # Verify the preferences were not saved
    json_dump_spy.assert_not_called()

    # Verify an exception is raised when the attribute found in preferences is None
    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    preferences_json = {
        "GUI": {
            "picker_color": "yellow"
        }
    }

    with open(preferences_path, "w",  encoding='utf-8') as file:
        json.dump(preferences_json, file, indent = 3)

    with preferences.Preferences(preferences_path, is_read_only=True) as loaded_preferences:
        with pytest.raises(AttributeError):
            ignored_value = loaded_preferences.GUI.theme


def test_qat_empty_preferences(tmp_path):
    """
    Verify that the preferences are empty when the preferences file does not exist
    """
    non_existing_preferences_path = tmp_path / "non_existing_preferences_file.json"

    assert not non_existing_preferences_path.is_file()

    # Creating preferences from an empty file in read only
    with preferences.Preferences(non_existing_preferences_path) as empty_preferences:
        assert len(empty_preferences) == 0

        # Trying to add preferences
        with pytest.raises(AttributeError):
            empty_preferences.new_dict_entry = "new_entry"

    # Ensure the file is not written when the preferences are empty
    with preferences.Preferences(non_existing_preferences_path, is_read_only=False) as empty_preferences:
        assert len(empty_preferences) == 0

    assert not non_existing_preferences_path.is_file()

    # Creating writable preferences from an empty file
    with preferences.Preferences(non_existing_preferences_path, is_read_only=False) as empty_preferences:
        assert len(empty_preferences) == 0
        empty_preferences.setting = "string_value"
        assert empty_preferences.setting == "string_value"

    # Reloading and verifying the preferences were saved
    with preferences.Preferences(non_existing_preferences_path, is_read_only=False) as reloaded_preferences:
        assert len(reloaded_preferences) == 1
        assert reloaded_preferences.setting == "string_value"


def test_qat_dynamic_variables(tmp_path):
    """
    Verify that the preferences dynamic variables can be accessed, updated properly
    and created dynamically
    """
    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        # Automatically create the GUI key
        assert dict_preferences.GUI == {}

        dict_preferences.GUI.picker_color = "yellow"
        assert dict_preferences.GUI.picker_color == "yellow"

        dict_preferences.GUI.picker_color = "red"
        assert dict_preferences.GUI.picker_color == "red"

        dict_preferences.GUI.list_number = [1, 2, 3]
        assert dict_preferences.GUI.list_number == [1, 2, 3]

        assert dict_preferences.GUI == { "picker_color": "red", "list_number": [1, 2, 3] }

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        assert dict_preferences.GUI == { "picker_color": "red", "list_number": [1, 2, 3] }

    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        dict_preferences.list_number = [1, [2, 4], 3]
        assert dict_preferences.list_number == [1, [2, 4], 3]

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        assert dict_preferences.list_number == [1, [2, 4], 3]


def test_preferences_cleanup(tmp_path):
    """
    Verify that the preferences are cleaned up of empty dictionaries when serializing to JSON
    """

    preferences_path = tmp_path / f"preferences_test_{uuid.uuid4().hex}.json"

    # Clean up of empty values when serializing to JSON
    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        dict_preferences.GUI.empty_list = []
        dict_preferences.GUI.empty_string = ""
        dict_preferences.GUI.empty_dict = {}
        dict_preferences.GUI.none_value = None

        assert dict_preferences.GUI.empty_list == []
        assert dict_preferences.GUI.empty_string == ""
        assert dict_preferences.GUI.empty_dict == {}
        assert dict_preferences.GUI.none_value == {}

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        assert dict_preferences == {'GUI': {'empty_list': [], 'empty_string': ''}}

    # Ensure the non-empty values are kept when serialiwing to JSON
    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        dict_preferences.GUI.picker_color = "red"
        assert dict_preferences.GUI.picker_color == "red"

        dict_preferences.GUI.list_number = [1, 2, 3]
        assert dict_preferences.GUI.list_number == [1, 2, 3]

        dict_preferences.GUI.empty_list = []
        dict_preferences.GUI.empty_string = ""
        dict_preferences.GUI.empty_dict = {}
        dict_preferences.GUI.none_value = None

        assert dict_preferences.GUI.empty_list == []
        assert dict_preferences.GUI.empty_string == ""
        assert dict_preferences.GUI.empty_dict == {}
        assert dict_preferences.GUI.none_value == {}

        assert dict_preferences == {
            'GUI': {
                'picker_color': 'red',
                'list_number': [1, 2, 3],
                'empty_list': [],
                'empty_string': '',
                'empty_dict': {},
                'none_value': {}
                }
            }

    with preferences.Preferences(preferences_path, is_read_only=False) as dict_preferences:
        assert dict_preferences == {
            'GUI': {
                "picker_color": "red",
                "list_number": [1, 2, 3],
                'empty_list': [],
                'empty_string': ''
                }
            }
