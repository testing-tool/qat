# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to native events
"""

from pathlib import Path
import time
import os
import pytest
import qat

test_file = Path.home() / '.AAB' / 'test.txt'

@pytest.fixture(autouse=True)
def teardown():
    """
    Clean up on exit
    """
    os.makedirs(test_file.parent, exist_ok=True)
    with open(test_file, 'w', encoding='utf-8') as file:
        file.write('test')

    context = qat.start_application('WidgetApp', "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()
    if os.path.exists(test_file):
        os.remove(test_file)
        os.rmdir(test_file.parent)
    test_folder = Path.home() / 'TestFolder'
    if os.path.exists(test_folder):
        os.rmdir(test_folder)
    test_folder = Path.home() / 'New Folder'
    if os.path.exists(test_folder):
        os.rmdir(test_folder)


@pytest.mark.serial
def test_native_keyboard():
    """
    Verify that native events such as typing and shortcuts can be generated
    """
    if qat.app_launcher.is_windows():
        windows_native_keyboard()
    elif qat.app_launcher.is_macos():
        macos_native_keyboard()
    else:
        pytest.skip(reason='Native events are supported on Windows and MacOS only')


def windows_native_keyboard():
    """
    Test implementation for MacOS
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'Dialog',
        'text': 'Open'
    }
    new_folder_path = Path.home() / 'TestFolder'
    assert not os.path.exists(new_folder_path)

    # Open the native File dialog
    qat.mouse_click(open_file_btn)
    assert qat.wait_for_property_value(open_file_dlg, 'visible', True)

    # Create a new folder
    qat.shortcut(qat.Platform, 'Ctrl+Shift+N')
    # Unfortunately, the folder object is not accessible to Qat.
    # There is no other choice than waiting a bit and hoping it has been created.
    time.sleep(0.5)
    qat.type_in(qat.Platform, 'TestFolder<Enter>')
    time.sleep(0.5)
    # Close dialog
    qat.type_in(qat.Platform, "<Escape>")

    # Dialog should be closed
    qat.wait_for_object_missing(open_file_dlg, timeout=500)

    # Verify that folder has been created
    assert os.path.exists(new_folder_path)


def macos_native_keyboard():
    """
    Test implementation for MacOS
    """
    open_file_btn = {'objectName': 'openFile'}
    open_file_dlg = {
        'type': 'NSOpenPanel',
        'text': 'Open'
    }

    # Open the native File dialog
    qat.mouse_click(open_file_btn)
    dialog = qat.wait_for_object(open_file_dlg)
    assert not dialog.is_null()

    # Use shortcut to open the path editor
    qat.shortcut(qat.Platform, 'Ctrl+Shift+g')
    # Unfortunately, the path editor is not a Cocoa widget and is not accessible by Qat.
    # There is no other choice than waiting a bit and hoping it has been opened.
    time.sleep(0.5)
    qat.type_in(qat.Platform, str(test_file))
    qat.type_in(qat.Platform, "<Enter>")
    # Close dialog
    time.sleep(0.5)
    qat.type_in(qat.Platform, "<Enter>")

    # Selected file should be displayed on the button
    qat.wait_for_property_value(
        open_file_btn,
        'text',
        test_file)

    # Dialog should be closed
    qat.wait_for_object_missing(open_file_dlg, timeout=500)
