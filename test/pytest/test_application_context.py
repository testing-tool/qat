# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to application launcher
"""

from pathlib import Path
import subprocess
import sys
import threading
import psutil
import pytest

from qat.internal import application_context, qat_environment
import qat


def test_config_file():
    """
    Verify that the qat config file getter/setter get and set the values correctly
    """
    ctxt = application_context.ApplicationContext('QmlApp', "")

    # Raises an exception when the pid is not set
    assert ctxt.pid == -1
    with pytest.raises(NameError):
        str(ctxt.config_file)

    # Creates the qat configuration file path when the pid is set and the configuration file path
    # was not set
    ctxt.pid = 1
    assert ctxt.config_file == qat_environment.create_qat_config_file_path(ctxt.pid)

    # Ensure the setter set the config_file parameter
    qat_config_file_path = Path("qat_config_file_path")
    ctxt.config_file = qat_config_file_path
    assert ctxt.config_file == qat_config_file_path


def test_auto_close():
    """
    Verify that applications are automatically closed when a script terminates.
    """
    # Start application from another process
    script = "import qat;qat.start_application('WidgetApp3');qat.start_application('WidgetApp4')"
    command = [sys.executable, '-c', script]
    process = subprocess.Popen(command)
    result = (True, "")
    try:
        rc = process.wait(timeout=10.0)
        assert rc == 0
    except subprocess.TimeoutExpired as error:
        print(error)
        process.kill()
    finally:
        # Make sure applications are killed in case of failure
        for proc in psutil.process_iter(['name']):
            if proc.name() in ['WidgetApp3', 'WidgetApp4']:
                result = (False, f"'{proc.name()}' was still running")
                proc.kill()

    if not result[0]:
        assert False, result[1]

    # Attached applications should not be closed
    qat.start_application('WidgetApp3')
    script = "import qat;qat.attach_to_application('WidgetApp3')"
    command = [sys.executable, '-c', script]
    process = subprocess.Popen(command)
    result = (True, "")
    try:
        rc = process.wait(10.0)
        assert rc == 0
    except subprocess.TimeoutExpired as error:
        print(error)
        process.kill()
        result = (False, "Script did not terminate (timeout)")

    context = qat.current_application_context()
    assert context is not None
    assert context.is_running()
    qat.close_application()

    if not result[0]:
        assert False, result[1]


def test_detach():
    """
    Verify that applications can be detached and not be closed upon script termination.
    """
    context1 = qat.start_application('WidgetApp5')
    context2 = qat.start_application('WidgetApp6')
    assert context1.is_running()
    assert context2.is_running()

    # Detach then re-attach a single application
    qat.detach(context2)
    qat.set_current_application_context(context2)
    with pytest.raises(ConnectionAbortedError):
        qat.list_top_windows()
    context2 = qat.attach_to_application(context2.pid)
    assert len(qat.list_top_windows()) > 0

    # Detach then re-attach multiple applications
    qat.detach([context1, context2])
    qat.set_current_application_context(context1)
    with pytest.raises(ConnectionAbortedError):
        qat.list_top_windows()
    qat.set_current_application_context(context2)
    with pytest.raises(ConnectionAbortedError):
        qat.list_top_windows()
    context1 = qat.attach_to_application(context1.pid)
    assert len(qat.list_top_windows()) > 0
    context2 = qat.attach_to_application(context2.pid)
    assert len(qat.list_top_windows()) > 0

    qat.close_application(context1)
    qat.close_application(context2)

    # Detached application should not be closed
    script = "import qat;qat.start_application('WidgetApp7');qat.start_application('WidgetApp8');qat.detach()"
    command = [sys.executable, '-c', script]
    process = subprocess.Popen(command)
    result = (True, "")
    try:
        rc = process.wait(timeout=10.0)
        assert rc == 0
    except subprocess.TimeoutExpired as error:
        print(error)
        process.kill()
        result = (False, "Script did not terminate (timeout)")

    assert qat.attach_to_application('WidgetApp7').is_running()
    qat.close_application()
    assert qat.attach_to_application('WidgetApp8').is_running()
    qat.close_application()

    if not result[0]:
        assert False, result[1]


def test_close_callback():
    """
    Verify that a function can be called when the application closes.
    """
    context = qat.start_application('WidgetApp')

    sync = threading.Condition()
    called = False

    def on_closed():
        nonlocal sync
        nonlocal called
        called = True
        with sync:
            sync.notify_all()

    context.register_close_callback(on_closed)

    qat.mouse_click('quitButton')

    with sync:
        sync.wait(3.0)
    assert called
