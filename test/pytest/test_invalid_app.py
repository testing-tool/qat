# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to application error management
"""

import os
from pathlib import Path
import pytest
import qat

@pytest.fixture(autouse=True)
def teardown(app_name, mocker):
    """
    Run tests in another directory to manage configurations
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='This test does not depend on application type and should only run once')

    mocker.patch(
        'os.getcwd',
        return_value = './build/invalid_app')
    yield

    for app in qat.list_applications():
        qat.unregister_application(app)


def test_invalid_app():
    """
    Verify that invalid paths are detected
    """
    qat.register_application('test', path='')

    with pytest.raises(ValueError):
        qat.start_application('undefined')

    with pytest.raises(FileNotFoundError):
        qat.start_application('test')

    qat.register_application('test', path='./build/not-a-path')
    with pytest.raises(FileNotFoundError):
        qat.start_application('test')

    current_path = Path(os.path.dirname(__file__)).resolve().absolute()
    qat.register_application('test', path=current_path)
    with pytest.raises(FileNotFoundError):
        qat.start_application('test')
