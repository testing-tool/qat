# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to object identification
"""

import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0
    yield
    qat.close_application()


def test_list_top_windows(app_name):
    """
    Verify that all top windows are accessible
    """
    expected_nb_windows = 2 if app_name == 'QmlApp' else 3
    assert len(qat.list_top_windows()) == expected_nb_windows


def test_find_all_objects():
    """
    Verify that multiple objects matching a definition can be found
    """
    # Single object
    object_def = {
        'objectName': 'counterButton',
        'type': 'Button'
    }

    objects = qat.find_all_objects(object_def)
    assert len(objects) == 1
    assert qat.wait_for_object_exists(objects[0]) is not None

    # Invalid object
    object_def = {
        'objectName': 'none',
        'type': 'Button'
    }

    objects = qat.find_all_objects(object_def)
    assert len(objects) == 0

    # Multiple objects
    object_def = {
        'container': {'objectName': 'buttonGrid'},
        'type': 'ColorButton'
    }

    objects = qat.find_all_objects(object_def)
    assert len(objects) == 25
    for obj in objects:
        assert qat.wait_for_object_exists(obj) is not None
