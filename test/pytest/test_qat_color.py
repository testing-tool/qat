# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to object picker color (Spy)
"""

import pytest
from qat.internal import app_launcher, qat_color


def test_is_valid_color():
    """
    Verify that the color selection works with valid hex color and with Qt general colors
    """

    if app_launcher.is_windows():
        pytest.skip(reason='This test does not depend on the system and should only run once')

    # Verify HexRGB colors are valid Yellow (#ffff00)
    assert qat_color.is_valid_color("#ffff00")

    # Verify HexARGB colors are valid when using alpha channel Yellow (#80ffff00)
    assert qat_color.is_valid_color("#80ffff00")
    
    # Verify the Qt predefined colors are valid
    assert qat_color.is_valid_color("yellow")
    
    # Verify invalid hex color
    assert not qat_color.is_valid_color("#ZZZZZZZZ")
    
    # Verify invalid hex format (too few/many and uneven values)
    assert not qat_color.is_valid_color("#80ffff00ff")
    assert not qat_color.is_valid_color("#80ff")
    assert not qat_color.is_valid_color("#80ffff0")

    # Missing # character
    assert not qat_color.is_valid_color("ffff00")

    # Verify invalid color
    assert not qat_color.is_valid_color("invalid_color")
