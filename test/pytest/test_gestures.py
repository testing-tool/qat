# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to gestures
"""


import threading
import math
import pytest
import qat


# Relative tolerance for float comparison
FLOAT_TOLERANCE = 1e-05


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_flick(app_name):
    """
    Verify that the flick function works with Flickable widgets
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='Flick gestures are only supported with QML/QtQuick')

    list_view_def = {
        'objectName': 'listView'
    }

    mvt_started = False
    mvt_ended = False

    sync = threading.Condition()

    def start():
        nonlocal mvt_started
        mvt_started = True

    def end():
        nonlocal mvt_ended
        mvt_ended = True
        nonlocal sync
        with sync:
            sync.notify_all()

    with sync:
        list_view = qat.wait_for_object(list_view_def)
        qat.connect(list_view, "movementStarted", start)
        qat.connect(list_view, "movementEnded", end)

        qat.flick(list_view_def, 0, 50)
        assert list_view.contentX == 0
        assert list_view.contentY == 50
        sync.wait(3.0)
        assert mvt_started
        assert mvt_ended


@pytest.mark.serial
def test_pinch(app_name):
    """
    Verify that pinch events can be simulated 
    """
    if app_name == 'WidgetApp' and qat.app_launcher.is_macos():
        pytest.skip(reason='QGestures not supported on MacOS')

    pinch_area_def = {
        'objectName': 'pinchArea'
    }
    pinch_item_def = {
        'objectName': 'gestureItem'
    }

    pinch_item = qat.wait_for_object(pinch_item_def)
    initial_width = pinch_item.width
    initial_height = pinch_item.height

    def compare_angles(value, reference):
        return abs(value - reference) < 1
    def compare_scales(value, reference):
        return abs(value - reference) < 0.01
    def compare_distances(value, reference):
        return abs(value - reference) < 1

    # Pure translation
    expected_x = 50
    expected_y = 60
    qat.pinch(pinch_area_def, translation = [expected_x, expected_y])
    assert qat.wait_for_property_value(pinch_item, "x", expected_x, comparator=compare_distances)
    assert qat.wait_for_property_value(pinch_item, "y", expected_y, comparator=compare_distances)
    # Item should not have changed its size or rotation
    assert initial_width == pinch_item.width
    assert initial_height == pinch_item.height
    assert pinch_item.rotation == 0
    assert math.isclose(pinch_item.scale, 1, rel_tol=FLOAT_TOLERANCE)

    # Pure rotation
    expected_angle = 90
    qat.pinch(pinch_area_def, rotation = expected_angle)

    assert qat.wait_for_property_value(pinch_item, "rotation", expected_angle, comparator=compare_angles)
    # Item should not have moved or changed its size
    assert initial_width == pinch_item.width
    assert initial_height == pinch_item.height
    assert expected_x == pinch_item.x
    assert expected_y == pinch_item.y

    # Pure zoom (= scaling)
    expected_scale = 1.5
    qat.pinch(pinch_area_def, scale = expected_scale)
    assert qat.wait_for_property_value(pinch_item, "scale", expected_scale, comparator=compare_scales)

    # Item should not have moved or changed its size or rotation
    assert initial_width == pinch_item.width
    assert initial_height == pinch_item.height
    assert pinch_item.rotation == expected_angle
    assert expected_x == pinch_item.x
    assert expected_y == pinch_item.y

    # Combined event
    qat.pinch(pinch_area_def, rotation = -45, scale = 0.5, translation = [50, -20])
    assert qat.wait_for_property_value(pinch_item, "rotation", expected_angle - 45, comparator=compare_angles)
    assert qat.wait_for_property_value(pinch_item, "scale", expected_scale / 2, comparator=compare_scales)
    assert qat.wait_for_property_value(pinch_item, "x", expected_x + 50, comparator=compare_distances)
    assert qat.wait_for_property_value(pinch_item, "y", expected_y - 20, comparator=compare_distances)


def test_native_pinch(app_name):
    """
    Verify that native pinch events can be simulated 
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='Native gestures are only supported with QML/QtQuick')

    pinch_area_def = {
        'objectName': 'pinchArea'
    }
    pinch_item_def = {
        'objectName': 'gestureItem'
    }
    pinch_item = qat.wait_for_object(pinch_item_def)
    initial_width = pinch_item.width
    initial_height = pinch_item.height
    initial_x = pinch_item.x
    initial_y = pinch_item.y

    # Pure rotation
    expected_angle = 40
    qat.native_pinch(pinch_area_def, angle = expected_angle)

    assert pinch_item.rotation == expected_angle
    # Item should not have moved or changed its size
    assert initial_width == pinch_item.width
    assert initial_height == pinch_item.height
    assert initial_x == pinch_item.x
    assert initial_y == pinch_item.y

    # Pure zoom (= scaling)
    expected_scale = 1.5
    qat.native_pinch(pinch_area_def, scale = expected_scale - 1)
    assert math.isclose(pinch_item.scale, expected_scale, rel_tol=FLOAT_TOLERANCE)

    # Item should not have moved or changed its size or rotation
    assert initial_width == pinch_item.width
    assert initial_height == pinch_item.height
    assert pinch_item.rotation == expected_angle
    assert initial_x == pinch_item.x
    assert initial_y == pinch_item.y

    # Combined event
    qat.native_pinch(pinch_area_def, angle = -60, scale = -0.5)
    assert pinch_item.rotation == expected_angle - 60
    assert math.isclose(pinch_item.scale, expected_scale / 2, rel_tol=FLOAT_TOLERANCE)

    # Invalid event: button does not handle gestures
    button_def = {
        'objectName': 'counterButton'
    }
    button = qat.wait_for_object(button_def)
    with pytest.raises(RuntimeWarning):
        qat.native_pinch(button, angle = expected_angle, check=True)
