# -*- coding: utf-8 -*-
# (c) Copyright 2023, Qat’s Authors

"""
Tests related to Qt's Model-View framework
"""

from helpers import round_float_to_int
import pytest
import qat


@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    if app_name != 'WidgetApp':
        pytest.skip(reason='Model-View framework not yet supported for QML applications')
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_listview_model():
    """
    Verify that we can access the underlying model of a ListView widget
    """
    list_view_def = {
        'objectName': 'listView',
        'type': 'ListView'
    }
    list_view = qat.wait_for_object(list_view_def)

    list_model = list_view.model
    assert list_model is not None
    # This ListView uses a proxy with a source model
    list_model = list_model.sourceModel

    index5 = list_model.index(5, 0)

    # Default role is Qt::DisplayRole
    assert list_model.data(index5) == "item 5"

    with pytest.raises(RuntimeError):
        # List only has 1 column
        list_model.data(list_model.index(5, 2))

    with pytest.raises(RuntimeError):
        # Invalid row index
        list_model.data(list_model.index(-1, 0))

    with pytest.raises(RuntimeError):
        # Out-of-bounds row index
        list_model.data(list_model.index(10000, 0))


def test_treeview_model():
    """
    Verify that we can access the underlying model of a TreeView widget
    """
    tree_def = {
        'objectName': 'treeView',
        'type': 'TreeView'
    }
    tree = qat.wait_for_object(tree_def)
    tree_model = tree.model

    root_index_3 = tree_model.index(3, 0)
    # Default role is Qt::DisplayRole
    assert tree_model.data(root_index_3) == "Root item 3"

    # Child item
    index = tree_model.index(2, 0, root_index_3)
    assert tree_model.data(index) == "Child item 14"

    # Other columns
    index = tree_model.index(2, 1, root_index_3)
    assert tree_model.data(index) == "Name 14"
    index = tree_model.index(2, 2, root_index_3)
    assert tree_model.data(index) == "Value 14"

    # Invalid column
    index = tree_model.index(2, 100, root_index_3)
    with pytest.raises(RuntimeError):
        tree_model.data(index)

    # Root items are also checkable.
    # todo Make Qt enums available to Python API
    check_state_role = 10
    assert tree_model.data(root_index_3, check_state_role) == 0
    assert tree_model.setData(root_index_3, 2, check_state_role)
    assert tree_model.data(root_index_3, check_state_role) == 2


def test_find_item_in_tree():
    """
    Verify that we can identify items in a TreeView
    """
    tree_def = {
        'objectName': 'treeView',
        'type': 'TreeView'
    }

    assert qat.wait_for_object(tree_def) is not None

    item_def = {
        'container': tree_def,
        'row': 1,
        'column': 0,
        'type': 'QModelIndex'
    }
    root_item = qat.wait_for_object(item_def)
    assert root_item.text == "Root item 1"

    # Find by row and column
    sub_item_def = {
        'container': item_def,
        'row': 2,
        'column': 2
    }
    sub_item = qat.wait_for_object(sub_item_def)
    assert sub_item.text == "Value 6"

    invalid_item_def = {
        'container': tree_def,
        'row': 10,
        'column': 2,
        'type': 'QModelIndex'
    }
    with pytest.raises(LookupError):
        qat.wait_for_object_exists(invalid_item_def)

    # Find by text
    sub_item_def = {
        'container': item_def,
        'text': 'Name 7'
    }
    sub_item = qat.wait_for_object(sub_item_def)
    assert sub_item.text == "Name 7"
    assert sub_item.row == 3
    assert sub_item.column == 1

    invalid_item_def = {
        'container': tree_def,
        'text': 'invalid'
    }
    with pytest.raises(LookupError):
        qat.wait_for_object_exists(invalid_item_def)

    # Find by pattern
    sub_item_def = {
        'container': item_def,
        'text': 'Name *'
    }
    matches = qat.find_all_objects(sub_item_def)
    assert len(matches) == 4
    sub_item_def = {
        'container': tree_def,
        'text': 'Value *'
    }
    matches = qat.find_all_objects(sub_item_def)
    assert len(matches) == 20


def test_edit_item_in_tree():
    """
    Verify that we can read and write properties of items in a TreeView
    """
    tree_def = {
        'objectName': 'treeView',
        'type': 'TreeView'
    }

    root_item_def = {
        'container': tree_def,
        'row': 3
    }
    root_item = qat.wait_for_object(root_item_def)
    assert root_item.text == "Root item 3"
    root_item.text = "new text"
    assert root_item.text == "new text"

    assert root_item.data() == "new text"
    assert root_item.setData("another text")
    assert root_item.data() == "another text"

    # todo Make Qt enums available to Python API
    # Change check state
    check_state_role = 10
    assert root_item.data(check_state_role) == 0
    assert root_item.setData(2, check_state_role)
    assert root_item.data(check_state_role) == 2

    foreground_role = 9
    foreground = root_item.data(foreground_role)
    assert foreground is not None
    assert foreground.color.name == "#ff00008b"

    # Change foreground brush
    foreground.color.red = 255
    foreground.color.green = 0
    foreground.color.blue = 0
    foreground.color.name = "#ffff0000"
    root_item.setData(foreground, foreground_role)
    foreground = root_item.data(foreground_role)
    assert foreground is not None
    assert foreground.color.name == "#ffff0000"

    qt_version = str(qat.current_application_context().qt_version)
    major_version = qt_version.split('.', maxsplit=1)[0]
    # Implicit conversion is not supported in Qt5
    if major_version != '5':
        # Foreground can also use a color
        new_color = foreground.color
        new_color.red = 0
        new_color.green = 255
        new_color.blue = 0
        new_color.name = "#ff00ff00"
        root_item.setData(new_color, foreground_role)
        foreground = root_item.data(foreground_role)
        assert foreground is not None
        assert foreground.name == "#ff00ff00"


def test_selection_model():
    """
    Verify that we can access the underlying selection model of a Tree View
    """
    tree_def = {
        'objectName': 'treeView',
        'type': 'TreeView'
    }
    tree_view = qat.wait_for_object(tree_def)

    tree_model = tree_view.model
    assert tree_model is not None

    index3 = tree_model.index(3, 0)

    model = tree_view.selectionModel
    assert model is not None

    assert not model.isSelected(index3)

    # Selecting a root item
    model.select(index3, 3)
    assert model.isSelected(index3)

    index3_2_0 = tree_model.index(2, 0, index3)
    index3_2_2 = tree_model.index(2, 2, index3)
    # Selecting first column of a child item
    model.select(index3_2_0, 3)
    assert not model.isSelected(index3)
    assert model.isSelected(index3_2_0)
    assert not model.isSelected(index3_2_2)
    # Selecting third column of a child item
    model.select(index3_2_2, 3)
    assert not model.isSelected(index3)
    assert not model.isSelected(index3_2_0)
    assert model.isSelected(index3_2_2)


@pytest.mark.serial
def test_listview_items():
    """
    Verify that we can access the items of a ListView widget
    """
    list_view_def = {
        'objectName': 'listView',
        'type': 'ListView'
    }

    item_def = {
        'container': list_view_def,
        'row': 2
    }
    # Clicking on an item changes its color
    element = qat.wait_for_object(item_def)
    assert element.color.name == '#ff000000'  # black

    qat.mouse_click(item_def)
    assert element.color.name == '#ffff0000'  # red

    # Cannot edit an item without selecting it
    original_text = element.text
    new_text = 'some text'
    qat.type_in(item_def, new_text)
    assert element.text == original_text

    # Edit item by double-clicking on it
    qat.double_click(item_def)
    qat.type_in(item_def, new_text)
    # Cancel changes
    qat.type_in(item_def, '<Escape>')
    assert element.text == original_text

    # Edit item by double-clicking on it
    qat.double_click(item_def)
    new_text = 'other text'
    qat.type_in(item_def, new_text)
    # Commit changes
    qat.type_in(item_def, '<Enter>')
    qat.wait_for_property_change(item_def, 'text', original_text, check=True)
    assert element.text == new_text

    # Cannot click on invisible items
    item_def['row'] = 15
    with pytest.raises(Exception):
        qat.mouse_click(item_def)

    # Automatically scroll to the item
    element = qat.wait_for_object(item_def)
    element.ScrollTo()
    qat.mouse_click(item_def, check=True)

    screenshot = qat.grab_screenshot(item_def)
    bounds = element.globalBounds
    assert screenshot.width == round_float_to_int(bounds['width'] * element.pixelRatio)
    assert screenshot.height == round_float_to_int(bounds['height'] * element.pixelRatio)


@pytest.mark.serial
def test_treeview_items():
    """
    Verify that we can access the items of a TreeView widget
    """
    tree_def = {
        'objectName': 'treeView',
        'type': 'TreeView'
    }
    root_item_def = {
        'container': tree_def,
        'row': 3
    }
    item_def = {
        'container': root_item_def,
        'row': 1,
        'column': 2
    }

    tree = qat.wait_for_object(tree_def)
    # Click on a root item
    root_item = qat.wait_for_object(root_item_def)
    qat.mouse_click(root_item, check=True)

    index = tree.model.index(3, 0)
    assert tree.selectionModel.isSelected(index)

    # Click on its checkbox
    check_state_role = 10
    assert root_item.data(check_state_role) == 0
    height = root_item.globalBounds['height']
    qat.mouse_click(root_item, x=height / 2, y=height / 2)
    assert root_item.data(check_state_role) == 2

    # Cannot edit an item without selecting it
    item = qat.wait_for_object(item_def)
    original_text = item.text
    new_text = 'some text'
    qat.type_in(item_def, new_text)
    qat.wait_for_property_value(item_def, 'text', original_text, check=True)

    # Edit item by double-clicking on it
    qat.double_click(item_def)
    qat.type_in(item_def, new_text)
    # Cancel changes
    qat.type_in(item_def, '<Escape>')
    qat.wait_for_property_value(item_def, 'text', original_text, check=True)

    # Edit item by double-clicking on it
    qat.double_click(item_def)
    new_text = 'other text'
    qat.type_in(item_def, new_text)
    # Commit changes
    qat.type_in(item_def, '<Enter>')
    qat.wait_for_property_value(item_def, 'text', new_text, check=True)

    # Cannot click on hidden item
    tree.hideColumn(item_def['column'])
    with pytest.raises(Exception):
        qat.mouse_click(item_def)
