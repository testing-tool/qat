# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors
"""
Tests related to object picker overlay (Spy)
"""

import pytest
import qat

@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    qat.close_application()


def test_picker_overlay(app_name):
    """
    Verify that the default color of the picker is yellow and the picker overlay
    size corresponds to the hovered object.
    """

    button_def = {
        'objectName': 'colorButton2'
    }

    main_window = {
        'objectName': 'mainWindow'
    }

    picker_overlay_def = {
        'container': main_window,
        'objectName': 'QatObjectPickerOverlay'
    }

    qat.activate_picker()

    button = qat.wait_for_object_exists(button_def)

    # Hovering the mouse over the button text (at the center of the button) to create a picker overlay.
    qat.mouse_move(button, x = 5, y = 5, button=qat.Button.NONE)
    qat.mouse_move(button, button=qat.Button.NONE)

    # Getting the picker overlay
    picker_overlay = qat.wait_for_object_exists(picker_overlay_def)

    if app_name == 'WidgetApp':
        stylesheet = picker_overlay.styleSheet
        # Verifies the picker overlay is yellow
        assert stylesheet == "background-color: rgba(255, 255, 0, 128)"

        # Verifying the picker overlay is the same size as the hovered object
        assert picker_overlay.width == button.width
        assert picker_overlay.height == button.height
    elif app_name == 'QmlApp':
        # Verifies the picker overlay is yellow
        picker_color = picker_overlay.color
        assert picker_color.red == 255
        assert picker_color.green == 255
        assert picker_color.blue == 0
        assert picker_color.alpha == 255
        assert picker_color.name == "#ffffff00"

        # Verifying the picker overlay is the same size as the hovered object
        # This ensures the hovered object is not a Qt internal object
        assert picker_overlay.width == button.width
        assert picker_overlay.height == button.height
    else:
        pytest.fail(f"Unsupported application name {app_name}")
