# -*- coding: utf-8 -*-
# (c) Copyright 2024, Qat’s Authors

"""
Tests related to Qt object types and serialization
"""

import math
import pytest

# pylint: disable = wildcard-import
# pylint: disable = unused-wildcard-import
from qat.qt_types import *
import qat

@pytest.fixture(autouse=True)
def teardown(app_name):
    """
    Clean up on exit
    """
    if app_name != 'QmlApp':
        pytest.skip(reason='This test does not depend on application type and should only run once')

    context = qat.start_application(app_name, "")
    assert context is not None
    assert context.pid != 0

    yield

    try:
        qat.close_application()
    except ProcessLookupError:
        pass


def test_qt_types():
    """
    Test the QtObject class
    """
    main_window = qat.wait_for_object('mainWindow')
    container = main_window.mainProxy.qtTypes
    # QColor
    color = QColor("red")
    container.color = color
    changed_color = container.color
    assert changed_color.red == 255
    assert changed_color.green == 0
    assert changed_color.blue == 0
    assert changed_color.alpha == 255
    assert changed_color.name == "#ffff0000"

    color = QColor(red=12, green=65, blue=200)
    container.color = color
    changed_color = container.color
    assert changed_color.red == 12
    assert changed_color.green == 65
    assert changed_color.blue == 200
    assert changed_color.alpha == 255

    color = QColor(red=12, green=65, blue=200, alpha=127)
    container.color = color
    changed_color = container.color
    assert changed_color.alpha == 127

    color = QColor(name='blue', red=12, green=65, blue=200, alpha=127)
    container.color = color
    changed_color = container.color
    # 'name' overrides other values
    assert changed_color.red == 0
    assert changed_color.green == 0
    assert changed_color.blue == 255
    assert changed_color.alpha == 255

    # QBrush
    brush = QBrush()
    container.brush = brush
    changed_brush = container.brush
    assert changed_brush.style == QBrush.BrushStyle.SolidPattern
    assert changed_brush.color.name == "#ff000000" # Black

    brush = QBrush(style=QBrush.BrushStyle.HorPattern, color=QColor("red"))
    container.brush = brush
    changed_brush = container.brush
    assert changed_brush.style == QBrush.BrushStyle.HorPattern
    assert changed_brush.color.name == "#ffff0000" # Red

    # QFont
    font = QFont(bold=False,
                 italic=False,
                 strikeOut=False,
                 underline=False,
                 fixedPitch=False,
                 pixelSize=10,
                 weight=QFont.Weight.Thin)
    container.font = font
    changed_font = container.font
    assert len(str(changed_font.family)) > 0 # Default family should be used
    assert not changed_font.bold
    assert not changed_font.italic
    assert not changed_font.strikeOut
    assert not changed_font.underline
    assert not changed_font.fixedPitch
    assert changed_font.pixelSize == 10
    assert changed_font.pointSize == -1 # Because pixelSize was used
    assert changed_font.weight == QFont.Weight.Thin

    font = QFont(bold=True,
                 italic=True,
                 strikeOut=True,
                 underline=True,
                 fixedPitch=True,
                 pointSize=10)
    container.font = font
    changed_font = container.font
    assert len(str(changed_font.family)) > 0 # Default family should be used
    assert changed_font.bold
    assert changed_font.italic
    assert changed_font.strikeOut
    assert changed_font.underline
    assert changed_font.fixedPitch
    assert changed_font.pixelSize == -1 # Because pointSize was used
    assert changed_font.pointSize == 10
    assert changed_font.weight == QFont.Weight.Bold

    # QPoint
    point = QPoint(20, -89)
    container.point = point
    changed_point = container.point
    assert changed_point == point
    assert changed_point.x == 20
    assert changed_point.y == -89
    container.point = QPoint()
    changed_point = container.point
    assert changed_point == QPoint()
    assert changed_point.x == 0
    assert changed_point.y == 0

    # QPointF
    point = QPointF(23.23, -0.14)
    container.pointF = point
    changed_point = container.pointF
    assert math.isclose(changed_point.x, 23.23, abs_tol=1e-5)
    assert math.isclose(changed_point.y, -0.14, abs_tol=1e-5)
    container.pointF = QPointF()
    changed_point = container.pointF
    assert math.isclose(changed_point.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_point.y, 0.0, abs_tol=1e-5)

    # QLine
    point1 = QPoint(10, 10)
    point2 = QPoint(10, 90)
    line = QLine(point1, point2)
    container.line = line
    changed_line = container.line
    assert changed_line == line
    assert changed_line.p1 == point1
    assert changed_line.p2 == point2
    assert changed_line.center == QPoint(10, 50)
    container.line = QLine()
    changed_line = container.line
    assert changed_line == QLine()
    assert changed_line.p1 == QPoint(0, 0)
    assert changed_line.p2 == QPoint(0, 0)
    assert changed_line.center == QPoint(0, 0)

    # QLineF
    point1 = QPointF(23.6, 2.5)
    point2 = QPointF(23.6, 17.5)
    line = QLineF(point1, point2)
    container.lineF = line
    changed_line = container.lineF
    assert math.isclose(changed_line.p1.x, 23.6, abs_tol=1e-5)
    assert math.isclose(changed_line.p1.y, 2.5, abs_tol=1e-5)
    assert math.isclose(changed_line.p2.x, 23.6, abs_tol=1e-5)
    assert math.isclose(changed_line.p2.y, 17.5, abs_tol=1e-5)
    assert math.isclose(changed_line.center.x, 23.6, abs_tol=1e-5)
    assert math.isclose(changed_line.center.y, 10.0, abs_tol=1e-5)
    container.lineF = QLineF()
    changed_line = container.lineF
    assert math.isclose(changed_line.p1.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_line.p1.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_line.p2.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_line.p2.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_line.center.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_line.center.y, 0.0, abs_tol=1e-5)

    # QSize
    size = QSize(150, 50)
    container.size = size
    changed_size = container.size
    assert changed_size.width == 150
    assert changed_size.height == 50
    container.size = QSize()
    changed_size = container.size
    assert changed_size.width == 0
    assert changed_size.height == 0

    # QSizeF
    size = QSizeF(12.5, 36.98)
    container.sizeF = size
    changed_size = container.sizeF
    assert math.isclose(changed_size.width, 12.5, abs_tol=1e-5)
    assert math.isclose(changed_size.height, 36.98, abs_tol=1e-5)
    container.sizeF = QSizeF()
    changed_size = container.sizeF
    assert math.isclose(changed_size.width, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_size.height, 0.0, abs_tol=1e-5)

    # QRect
    rect = QRect(QPoint(10, 20), QSize(50, 60))
    container.rect = rect
    changed_rect = container.rect
    assert changed_rect.x == 10
    assert changed_rect.y == 20
    assert changed_rect.width == 50
    assert changed_rect.height == 60
    container.rect = QRect()
    changed_rect = container.rect
    assert changed_rect.x == 0
    assert changed_rect.y == 0
    assert changed_rect.width == 0
    assert changed_rect.height == 0

    # QRectF
    rect = QRectF(QPointF(12.3, 63.1), QSizeF(75.4, 87.9))
    container.rectF = rect
    changed_rect = container.rectF
    assert math.isclose(changed_rect.x, 12.3, abs_tol=1e-5)
    assert math.isclose(changed_rect.y, 63.1, abs_tol=1e-5)
    assert math.isclose(changed_rect.width, 75.4, abs_tol=1e-5)
    assert math.isclose(changed_rect.height, 87.9, abs_tol=1e-5)
    container.rectF = QRectF()
    changed_rect = container.rectF
    assert math.isclose(changed_rect.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_rect.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_rect.width, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_rect.height, 0.0, abs_tol=1e-5)

    # QVector2D
    vector = QVector2D(-63.04, 53.67)
    container.vector2d = vector
    changed_vector = container.vector2d
    assert math.isclose(changed_vector.x, -63.04, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 53.67, abs_tol=1e-5)
    container.vector2d = QVector2D()
    changed_vector = container.vector2d
    assert math.isclose(changed_vector.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 0.0, abs_tol=1e-5)

    # QVector3D
    vector = QVector3D(12.3, 4.56, 78.9)
    container.vector3d = vector
    changed_vector = container.vector3d
    assert math.isclose(changed_vector.x, 12.3, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 4.56, abs_tol=1e-5)
    assert math.isclose(changed_vector.z, 78.9, abs_tol=1e-5)
    container.vector3d = QVector3D()
    changed_vector = container.vector3d
    assert math.isclose(changed_vector.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.z, 0.0, abs_tol=1e-5)

    # QVector4D
    vector = QVector4D(0.1, 2.3, 4.5, 6.7)
    container.vector4d = vector
    changed_vector = container.vector4d
    assert math.isclose(changed_vector.x, 0.1, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 2.3, abs_tol=1e-5)
    assert math.isclose(changed_vector.z, 4.5, abs_tol=1e-5)
    assert math.isclose(changed_vector.w, 6.7, abs_tol=1e-5)
    container.vector4d = QVector4D()
    changed_vector = container.vector4d
    assert math.isclose(changed_vector.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.z, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_vector.w, 0.0, abs_tol=1e-5)

    # QQuaternion
    quaternion = QQuaternion(QVector3D(1.2, 3.4, 5.6), scalar=0.15)
    container.quaternion = quaternion
    changed_quaternion = container.quaternion
    assert math.isclose(changed_quaternion.x, 1.2, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.y, 3.4, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.z, 5.6, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.scalar, 0.15, abs_tol=1e-5)
    container.quaternion = QQuaternion()
    changed_quaternion = container.quaternion
    assert math.isclose(changed_quaternion.x, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.y, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.z, 0.0, abs_tol=1e-5)
    assert math.isclose(changed_quaternion.scalar, 1.0, abs_tol=1e-5)


    # QByteArray
    values = [5, 8, 65, 1, 117, 0, 58]
    byte_array = QByteArray(values)
    container.byteArray = byte_array
    changed_array = container.byteArray
    assert changed_array == byte_array
    assert len(changed_array.bytes) == len(values)
    for i, value in enumerate(values):
        assert value == changed_array.bytes[i]
    