
============
Contributing
============

.. include:: Contributing.md
   :parser: myst_parser.sphinx_
   
.. toctree::
   :maxdepth: 0
   :hidden:

   Building Qat <Build instructions.md>
   Building Qt <qt/Build instructions.md>
   Issues workflow <contributing/Issues_Workflow.md>
   Architecture <contributing/Architecture.md>
