# How to contribute to Qat

First of all, thank you for contributing to Qat!

## Getting source code
You can either clone the Qat project or fork it to your own project.
Qat uses a development process similar to the Git flow: the main development branch is called ___develop___ and contributions must be done in either a ___feature___ or a ___bugfix___ branch.

Simply create a branch named _feature/\<name\>_ or _bugfix/\<name\>_.

## Building
The building process is described in the [Build instructions](./Build%20instructions.md). Feel free to improve these instructions or submit an issue if you encounter any problem while building or using Qat.

## Publishing your change
Commit and push your changes to Gitlab then [open](https://gitlab.com/testing-tool/qat/-/merge_requests) a Merge Request.
Once the pipeline runs successfully and the Merge Request is approved, your changes will be merged into the ___develop___ branch and your branch will be deleted from the Qat repository.

In order for the pipeline to succeed:
- The Doxygen generation must not report any issue.
- The coding standard must be followed.
- The code must compile and link successfully for all supported Qt versions and platforms.
- The tests must pass for all supported Qt versions and platforms.

> You are encouraged to make Merge Requests as small as possible. This helps reviewers focusing on your changes and find potential mistakes or flaws.

## Coding style
TODO C++

Python code follows the [PEP 8](https://peps.python.org/pep-0008/) Coding Style.

## Documentation
General architecture is described [here](./contributing/Architecture.md).

C++ code is documented with [Doxygen](https://www.doxygen.nl/manual/docblocks.html#cppblock).

Python code is documented with [Docstring Conventions](https://peps.python.org/pep-0257/)

