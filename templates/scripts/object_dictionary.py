# -*- coding: utf-8 -*-

"""
Definitions of all objects used by the tests
"""

# Define each object in a property dictionary
main_window = {
    'type': 'ApplicationWindow'
}

# Map a unique name to each object definition.
# These names will be used in feature files.
bdd_mapping = {
    'Main Window': main_window
}
