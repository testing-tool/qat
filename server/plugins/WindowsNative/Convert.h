// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QKeyEvent>

#include <Windows.h>

#include <optional>

/// Convert the given Qt key to a Windows Virtual key
/// \param key A Qt key
/// \return The corresponding Virtual Key or nullopt is the key is not supported
inline std::optional<int> ConvertKeyToVKey(int key)
{
   switch (key)
   {
      case 0:
         return 0;
      case Qt::Key::Key_Escape:
         return VK_ESCAPE;
      case Qt::Key::Key_Return:
         return VK_RETURN;
      case Qt::Key::Key_Backspace:
         return VK_BACK;
      case Qt::Key::Key_Delete:
         return VK_DELETE;
      case Qt::Key::Key_Tab:
         return VK_TAB;
      case Qt::Key::Key_Control:
         return VK_CONTROL;
      case Qt::Key::Key_Shift:
         return VK_SHIFT;
      case Qt::Key::Key_Alt:
         return VK_MENU;
      case Qt::Key::Key_F1:
         return VK_F1;
      case Qt::Key::Key_F2:
         return VK_F2;
      case Qt::Key::Key_F3:
         return VK_F3;
      case Qt::Key::Key_F4:
         return VK_F4;
      case Qt::Key::Key_F5:
         return VK_F5;
      case Qt::Key::Key_F6:
         return VK_F6;
      case Qt::Key::Key_F7:
         return VK_F7;
      case Qt::Key::Key_F8:
         return VK_F8;
      case Qt::Key::Key_F9:
         return VK_F9;
      case Qt::Key::Key_F10:
         return VK_F10;
      case Qt::Key::Key_F11:
         return VK_F11;
      case Qt::Key::Key_F12:
         return VK_F12;
      default:
         return std::nullopt;
   }
}
