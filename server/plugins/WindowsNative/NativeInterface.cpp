// (c) Copyright 2024, Qat’s Authors
#include <Convert.h>
#include <NativeInterface.h>

#include <qat-server/events/NativeEventsFilter.h>

#include <QKeyEvent>
#include <QKeySequence>
#include <QMouseEvent>

#include <Windows.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <sstream>

namespace Qat
{

NativeInterface::NativeInterface()
    : INativeInterface()
{
}

bool NativeInterface::event(QEvent* event)
{
   switch (event->type())
   {
      case QEvent::MouseButtonPress:
      case QEvent::MouseButtonRelease:
      case QEvent::MouseButtonDblClick:
      {
         const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
         return HandleMouseEvent(mouseEvent);
      }
      case QEvent::KeyPress:
      case QEvent::KeyRelease:
      {
         const auto* keyEvent = static_cast<const QKeyEvent*>(event);
         return HandleKeyboardEvent(keyEvent);
      }
      default:
         return QObject::event(event);
   }
}

bool NativeInterface::HandleMouseEvent([[maybe_unused]] const QMouseEvent* mouseEvent)
{
   return false;
}

bool NativeInterface::HandleKeyboardEvent(const QKeyEvent* keyboardEvent)
{
   DWORD flags = 0;
   switch (keyboardEvent->type())
   {
      case QEvent::KeyPress:
         break;
      case QEvent::KeyRelease:
         flags = KEYEVENTF_KEYUP;
         break;
      default:
         std::cerr << "Cannot send keyboard event: action not supported" << std::endl;
         return false;
   }

   auto text = keyboardEvent->text();
   if (keyboardEvent->key() != 0)
   {
      QKeySequence key(keyboardEvent->key());
      text = key.toString();
   }
   const auto modifiers = keyboardEvent->modifiers();
   if (modifiers == Qt::NoModifier && text.isEmpty())
   {
      std::cerr << "Cannot send empty keyboard event" << std::endl;
      return false;
   }

   std::vector<INPUT> inputList;
   if (modifiers.testFlag(Qt::AltModifier))
   {
      INPUT input = {0};
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = flags;
      input.ki.wVk = VK_MENU;
      inputList.push_back(input);
   }
   if (modifiers.testFlag(Qt::ControlModifier))
   {
      INPUT input = {0};
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = flags;
      input.ki.wVk = VK_CONTROL;
      inputList.push_back(input);
   }
   if (modifiers.testFlag(Qt::MetaModifier))
   {
      INPUT input = {0};
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = flags;
      input.ki.wVk = VK_LWIN;
      inputList.push_back(input);
   }
   if (modifiers.testFlag(Qt::ShiftModifier))
   {
      INPUT input = {0};
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = flags;
      input.ki.wVk = VK_SHIFT;
      inputList.push_back(input);
   }

   if (!text.isEmpty())
   {
      INPUT input = {0};
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = flags;

      if (text.length() > 1)
      {
         const auto vkey = ConvertKeyToVKey(keyboardEvent->key());
         if (!vkey.has_value() || *vkey == 0)
         {
            std::cerr << "Keyboard event not supported: <" << text.toStdString() << ">"
                      << std::endl;
            return false;
         }
         input.ki.wVk = *vkey;
      }
      else
      {
         auto c = text.front();
         // SendInput supports upper chars only
         if (c.isLower())
         {
            c = c.toUpper();
         }
         else if (c.isUpper())
         {
            // Add shift key to modifiers
            if (!modifiers.testFlag(Qt::ShiftModifier))
            {
               INPUT shiftInput = {0};
               shiftInput.type = INPUT_KEYBOARD;
               shiftInput.ki.dwFlags = flags;
               shiftInput.ki.wVk = VK_SHIFT;
               inputList.push_back(shiftInput);
            }
         }
         input.ki.wVk = c.toLatin1();
      }
      inputList.push_back(input);
   }

   if (flags == KEYEVENTF_KEYUP)
   {
      std::reverse(inputList.begin(), inputList.end());
   }

   if (mFilter)
   {
      mFilter->ExpectKeyEvents(
         static_cast<int>(inputList.size()), std::chrono::seconds(1));
   }
   UINT result = SendInput(
      static_cast<UINT>(inputList.size()), inputList.data(), sizeof(INPUT));
   if (result != static_cast<UINT>(inputList.size()))
   {
      std::cerr << "Failed to send keyboard event: " << HRESULT_FROM_WIN32(GetLastError())
                << std::endl;
      if (mFilter)
      {
         mFilter->ExpectKeyEvents(
            -static_cast<int>(inputList.size()), std::chrono::seconds(0));
      }
      return false;
   }

   return true;
}

} // namespace Qat