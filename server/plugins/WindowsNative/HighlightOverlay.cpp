// (c) Copyright 2024, Qat’s Authors

#include <HighlightOverlay.h>

#include <QPainter>

namespace
{
constexpr double NORMAL_OPACITY = 0.5;
constexpr double TRANSPARENT_OPACITY = 0.01;
} // namespace

namespace Qat::WindowsNativePlugin
{

HighlightOverlay::HighlightOverlay()
    : QRasterWindow(nullptr)
{
   // Configure flags to display an overlay window
   // without frame nor shadow and not appearing in the taskbar
   setFlag(Qt::FramelessWindowHint);
   setFlag(Qt::WindowStaysOnTopHint);
   setFlag(Qt::Tool);
   setFlag(Qt::NoDropShadowWindowHint);

   setModality(Qt::ApplicationModal);

   setOpacity(NORMAL_OPACITY);
}

void HighlightOverlay::SetHighlightArea(const QRect& area)
{
   mHighlightArea = area;
}

void HighlightOverlay::paintEvent([[maybe_unused]] QPaintEvent* paintEvent)
{
   QPainter painter(this);
   QRect allArea{0, 0, width(), height()};
   painter.eraseRect(allArea);

   if (!mHighlightArea.isValid() || mHighlightArea.isNull())
   {
      setOpacity(TRANSPARENT_OPACITY);
      return;
   }
   setOpacity(NORMAL_OPACITY);
   painter.fillRect(mHighlightArea, Qt::yellow);
}

} // namespace Qat::WindowsNativePlugin