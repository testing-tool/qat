// (c) Copyright 2024, Qat’s Authors

#include <NativeInterfaceWidget.h>

#include <qat-server/Constants.h>

#include <iostream>

namespace Qat
{

NativeInterfaceWidget::NativeInterfaceWidget(NativeInterface* interface)
    : mInterface{interface}
{
}

QObject* NativeInterfaceWidget::GetQtObject() const
{
   return mInterface;
}

std::string NativeInterfaceWidget::GetId() const
{
   return Constants::NATIVE_IFACE_ID;
}

QObject* NativeInterfaceWidget::GetParent() const
{
   return nullptr;
}

std::vector<QObject*> NativeInterfaceWidget::GetChildWidgets() const
{
   return {};
}

QAbstractItemModel* NativeInterfaceWidget::GetModel() const
{
   return nullptr;
}

QItemSelectionModel* NativeInterfaceWidget::GetSelectionModel() const
{
   return nullptr;
}

QWindow* NativeInterfaceWidget::GetWindow() const
{
   return nullptr;
}

QPointF NativeInterfaceWidget::MapToGlobal(const QPointF& point) const
{
   return point;
}

QPointF NativeInterfaceWidget::MapFromGlobal(const QPointF& point) const
{
   return point;
}

QPointF NativeInterfaceWidget::MapToScene(const QPointF& point) const
{
   return point;
}

QPointF
NativeInterfaceWidget::MapToWidget(const IWidget* widget, const QPointF& point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   return widget->MapFromGlobal(point.toPoint());
}

bool NativeInterfaceWidget::Contains(const QPointF& point) const
{
   return GetBounds().contains(point.toPoint());
}

QSizeF NativeInterfaceWidget::GetSize() const
{
   return GetBounds().size();
}

qreal NativeInterfaceWidget::GetWidth() const
{
   return GetSize().width();
}

qreal NativeInterfaceWidget::GetHeight() const
{
   return GetSize().height();
}

QRect NativeInterfaceWidget::GetBounds() const
{
   return {-1, -1, -1, -1};
}

float NativeInterfaceWidget::GetPixelRatio() const
{
   return 1.0F;
}

qreal NativeInterfaceWidget::GetZ() const
{
   return 0;
}

bool NativeInterfaceWidget::IsVisible() const
{
   return true;
}

void NativeInterfaceWidget::ForceActiveFocus(
   [[maybe_unused]] Qt::FocusReason reason) const
{
}

void NativeInterfaceWidget::ReleaseActiveFocus() const {}

void NativeInterfaceWidget::GrabImage(
   [[maybe_unused]] std::function<void(const QImage&)> callback) const
{
}
} // namespace Qat