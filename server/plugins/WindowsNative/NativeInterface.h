// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <qat-server/plugins/INativeInterface.h>

#include <QObject>
#include <QPointer>


// Forward declarations
class QEvent;
class QKeyEvent;
class QMouseEvent;

namespace Qat
{

/// \brief Handle native events.
class NativeInterface : public INativeInterface
{
   Q_OBJECT

public:
   /// Constructor
   NativeInterface();

   /// Default destructor
   ~NativeInterface() = default;

   /// \copydoc QObject::event()
   bool event(QEvent* event) override;

   /// \copydoc INativeInterface::SetEventFilter
   void SetEventFilter(NativeEventsFilter* filter) override
   {
      mFilter = filter;
   }

private:
   /// Handle the given mouse event
   /// \param mouseEvent A mouse event
   /// \return True if the event was properly handled, False otherwise
   bool HandleMouseEvent(const QMouseEvent* mouseEvent);

   /// Handle the given keyboard event
   /// \param keyboardEvent A keyboard event
   /// \return True if the event was properly handled, False otherwise
   bool HandleKeyboardEvent(const QKeyEvent* keyboardEvent);

   /// Optional event filter
   NativeEventsFilter* mFilter{nullptr};
};

} // namespace Qat