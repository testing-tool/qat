// (c) Copyright 2024, Qat’s Authors
#include <CocoaWidgetWrapper.h>

#include <CocoaInterface.h>
#include <CocoaInterfaceConstants.h>
#include <Convert.h>

#include <QMouseEvent>

#include <Carbon/Carbon.h> // For keyboard constants kVK_*

#include <iostream>

namespace Qat
{

CocoaWidgetWrapper::CocoaWidgetWrapper(
   CocoaWidgetWrapper* parent, WIDGET_HANDLE handle, WINDOW_HANDLE windowHandle)
    : INativeWidget(parent)
    , mHandle(handle)
    , mWindowHandle(windowHandle)
    , mParent(parent)
{
}

WIDGET_HANDLE CocoaWidgetWrapper::GetHandle() const
{
   return mHandle;
}

WINDOW_HANDLE CocoaWidgetWrapper::GetWindowHandle() const
{
   return mWindowHandle;
}

bool CocoaWidgetWrapper::Exists() const
{
   if (!mParent || !mParent->Exists())
   {
      return false;
   }
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return mParent->GetChildren().contains(const_cast<CocoaWidgetWrapper*>(this));
#else
   return mParent->GetChildren().contains(this);
#endif
}

QObjectList CocoaWidgetWrapper::GetChildren()
{
   if (!Exists())
   {
      return {};
   }
   // Find current children
   std::vector<WIDGET_HANDLE> currentChildHandles;
   int nbChildren = 0;
   CocoaInterface::GetChildren(mHandle, nullptr, &nbChildren);
   currentChildHandles.resize(nbChildren);
   CocoaInterface::GetChildren(mHandle, currentChildHandles.data(), &nbChildren);

   for (auto h : currentChildHandles)
   {
      const auto it = mChildren.find(h);
      if (it == mChildren.cend())
      {
         mChildren[h] = new CocoaWidgetWrapper(this, h, mWindowHandle);
      }
   }

   // Delete obsolete wrappers
   std::vector<WIDGET_HANDLE> obsoleteChildHandles;
   for (const auto& child : mChildren)
   {
      const auto it = std::find(
         currentChildHandles.cbegin(), currentChildHandles.cend(), child.first);
      if (it == currentChildHandles.cend())
      {
         obsoleteChildHandles.push_back(child.first);
         child.second->deleteLater();
      }
   }
   for (const auto& handle : obsoleteChildHandles)
   {
      mChildren.erase(handle);
   }

   // Build result
   QObjectList children;
   children.reserve(static_cast<int>(mChildren.size()));
   for (const auto& child : mChildren)
   {
      children.push_back(child.second);
   }
   return children;
}

CocoaWidgetWrapper* CocoaWidgetWrapper::GetChildAt(QPoint position)
{
   if (!Contains(position))
   {
      return nullptr;
   }
   long globalX = position.x();
   long globalY = position.y();
   CocoaInterface::MapToGlobal(
      GetHandle(), position.x(), position.y(), &globalX, &globalY);
   QPointF globalPosition(globalX, globalY);
   const auto childWidgets = GetChildren();
   for (auto child : childWidgets)
   {
      auto* widget = qobject_cast<CocoaWidgetWrapper*>(child);
      if (!widget)
         continue;
      long localX = 0;
      long localY = 0;
      CocoaInterface::MapFromGlobal(
         widget->GetHandle(), globalPosition.x(), globalPosition.y(), &localX, &localY);
      auto* result = widget->GetChildAt(QPoint(localX, localY));
      if (result)
         return result;
   }
   return this;
}

bool CocoaWidgetWrapper::IsVisible() const
{
   return Exists();
}

bool CocoaWidgetWrapper::IsEnabled() const
{
   if (!Exists())
   {
      return false;
   }
   return CocoaInterface::IsEnabled(mHandle);
}

QString CocoaWidgetWrapper::GetType() const
{
   if (!Exists())
   {
      return "";
   }
   char buffer[MAX_CLASS_NAME_LENGTH] = {0};
   CocoaInterface::GetClassName(mHandle, buffer);
   return QString(buffer);
}

QString CocoaWidgetWrapper::GetText() const
{
   if (!Exists())
   {
      return "";
   }
   char buffer[MAX_TEXT_LENGTH] = {0};
   CocoaInterface::GetText(mHandle, buffer);
   return QString(buffer);
}

QString CocoaWidgetWrapper::GetHandleHex() const
{
   if (!Exists())
   {
      return "";
   }
   return QString("0x") +
          QString("%1")
             .arg(static_cast<quintptr>(mHandle), QT_POINTER_SIZE, 16, QChar('0'))
             .toUpper();
}

QRect CocoaWidgetWrapper::GetBounds() const
{
   if (!Exists())
   {
      return {};
   }
   int x;
   int y;
   int width;
   int height;
   CocoaInterface::GetBounds(GetHandle(), &x, &y, &width, &height);
   return {x, y, width, height};
}

int CocoaWidgetWrapper::GetWidth() const
{
   return GetBounds().width();
}

int CocoaWidgetWrapper::GetHeight() const
{
   return GetBounds().height();
}

QRectF CocoaWidgetWrapper::GetScaledBounds() const
{
   const auto pixelRatio = GetPixelRatio();
   const auto bounds = GetBounds();
   return {bounds.topLeft() / pixelRatio, bounds.size() / pixelRatio};
}

float CocoaWidgetWrapper::GetPixelRatio() const
{
   return 1.0F;
}

bool CocoaWidgetWrapper::Contains(const QPointF& point) const
{
   const auto bounds = GetBounds();
   return point.x() >= 0 && point.x() < bounds.width() && point.y() >= 0 &&
          point.y() < bounds.height();
}

bool CocoaWidgetWrapper::event(QEvent* event)
{
   if (!Exists())
   {
      return false;
   }
   switch (event->type())
   {
      case QEvent::MouseButtonPress:
      case QEvent::MouseButtonRelease:
      case QEvent::MouseButtonDblClick:
      {
         const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
         return HandleMouseEvent(mouseEvent);
      }
      case QEvent::KeyPress:
      case QEvent::KeyRelease:
      {
         const auto* keyEvent = static_cast<const QKeyEvent*>(event);
         return HandleKeyboardEvent(keyEvent);
      }
      default:
         return QObject::event(event);
   }
}

bool CocoaWidgetWrapper::HandleMouseEvent(const QMouseEvent* mouseEvent)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   const auto position = mouseEvent->globalPos();
#else
   const auto position = mouseEvent->globalPosition().toPoint();
#endif
   MouseButton button = MouseButton::ButtonLeft;
   switch (mouseEvent->button())
   {
      case Qt::MouseButton::LeftButton:
         button = MouseButton::ButtonLeft;
         break;
      case Qt::MouseButton::RightButton:
         button = MouseButton::ButtonRight;
         break;
      default:
         std::cerr << "Cannot send mouse event: button not supported" << std::endl;
         return false;
   }
   MouseEventType type = MouseEventType::MouseDown;
   switch (mouseEvent->type())
   {
      case QEvent::MouseButtonPress:
         type = MouseEventType::MouseDown;
         break;
      case QEvent::MouseButtonRelease:
         type = MouseEventType::MouseUp;
         break;
      case QEvent::MouseButtonDblClick:
         type = MouseEventType::MouseDblClick;
         break;
      default:
         std::cerr << "Cannot send mouse event: action not supported" << std::endl;
         return false;
   }
   return CocoaInterface::SendMouseEvent(
      mWindowHandle, button, type, position.x(), position.y());
}

bool CocoaWidgetWrapper::HandleKeyboardEvent(const QKeyEvent* keyboardEvent)
{
   auto eventType = KeyEventType::KeyDown;
   switch (keyboardEvent->type())
   {
      case QEvent::KeyPress:
         eventType = KeyEventType::KeyDown;
         break;
      case QEvent::KeyRelease:
         eventType = KeyEventType::KeyUp;
         break;
      default:
         std::cerr << "Cannot send keyboard event: action not supported" << std::endl;
         return false;
   }
   const auto text = keyboardEvent->text();
   const auto vkey = ConvertKeyToVKey(keyboardEvent->key());

   return CocoaInterface::SendKeyboardEvent(
      mWindowHandle, eventType, text[0].toLatin1(), vkey ? *vkey : 0);
}

} // namespace Qat