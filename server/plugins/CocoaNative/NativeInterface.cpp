// (c) Copyright 2024, Qat’s Authors
#include <CocoaInterface.h>
#include <CocoaInterfaceConstants.h>
#include <Convert.h>
#include <NativeInterface.h>

#include <QKeyEvent>
#include <QMouseEvent>

#include <iostream>
#include <sstream>

namespace Qat
{

NativeInterface::NativeInterface()
    : INativeInterface()
{
}

bool NativeInterface::event(QEvent* event)
{
   switch (event->type())
   {
      case QEvent::MouseButtonPress:
      case QEvent::MouseButtonRelease:
      case QEvent::MouseButtonDblClick:
      {
         const auto* mouseEvent = static_cast<const QMouseEvent*>(event);
         return HandleMouseEvent(mouseEvent);
      }
      case QEvent::KeyPress:
      case QEvent::KeyRelease:
      {
         const auto* keyEvent = static_cast<const QKeyEvent*>(event);
         return HandleKeyboardEvent(keyEvent);
      }
      default:
         return QObject::event(event);
   }
}

bool NativeInterface::HandleMouseEvent(const QMouseEvent* mouseEvent)
{
   return false;
}

bool NativeInterface::HandleKeyboardEvent(const QKeyEvent* keyboardEvent)
{
   switch (keyboardEvent->type())
   {
      case QEvent::KeyPress:
         // Keystroke will occur on KeyRelease event only
         return true;
      case QEvent::KeyRelease:
         break;
      default:
         std::cerr << "Cannot send keyboard event: action not supported" << std::endl;
         return false;
   }

   const auto modifiers = keyboardEvent->modifiers();
   std::vector<std::string> modifierList;
   if (modifiers.testFlag(Qt::AltModifier))
   {
      modifierList.push_back("alt");
   }
   if (modifiers.testFlag(Qt::ControlModifier))
   {
      modifierList.push_back("command");
   }
   if (modifiers.testFlag(Qt::MetaModifier))
   {
      modifierList.push_back("control");
   }
   if (modifiers.testFlag(Qt::ShiftModifier))
   {
      modifierList.push_back("shift");
   }

   const auto vkey = ConvertKeyToVKey(keyboardEvent->key());
   const auto text = keyboardEvent->text().toStdString();
   if (text.empty() && (!vkey.has_value() || *vkey == 0))
   {
      std::cerr << "Key not supported" << std::endl;
      return false;
   }

   std::stringstream command;
   command << "tell application \"System Events\"" << std::endl;
   command << "    ";
   if (vkey.has_value() && *vkey != 0)
   {
      command << "key code " << *vkey;
   }
   else
   {
      command << "keystroke \"" << text << "\"";
   }

   if (modifierList.size() > 0)
   {
      command << " using ";
   }
   if (modifierList.size() > 1)
   {
      command << "{";
   }
   bool firstMod = true;
   for (const auto& mod : modifierList)
   {
      if (!firstMod)
      {
         command << ", ";
      }
      command << mod << " down";
      firstMod = false;
   }
   if (modifierList.size() > 1)
   {
      command << "}";
   }
   command << std::endl;
   command << "end tell";

   return CocoaInterface::RunAppleScript(command.str().c_str());
}

} // namespace Qat