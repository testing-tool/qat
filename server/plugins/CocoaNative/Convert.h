// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QKeyEvent>

#include <Carbon/Carbon.h> // For keyboard constants kVK_*

#include <optional>

/// Convert the given Qt key to a Cocoa Virtual key
/// \param key A Qt key
/// \return The corresponding Virtual Key or nullopt is the key is not supported
inline std::optional<unsigned short> ConvertKeyToVKey(int key)
{
   switch (key)
   {
      case 0:
         return 0;
      case Qt::Key::Key_Escape:
         return kVK_Escape;
      case Qt::Key::Key_Return:
         return kVK_Return;
      case Qt::Key::Key_Backspace:
         return kVK_Delete;
      case Qt::Key::Key_Delete:
         return kVK_ForwardDelete;
      case Qt::Key::Key_Tab:
         return kVK_Tab;
      case Qt::Key::Key_Control:
         return kVK_Command;
      case Qt::Key::Key_Meta:
         return kVK_Control;
      case Qt::Key::Key_Shift:
         return kVK_Shift;
      case Qt::Key::Key_F1:
         return kVK_F1;
      case Qt::Key::Key_F2:
         return kVK_F2;
      case Qt::Key::Key_F3:
         return kVK_F3;
      case Qt::Key::Key_F4:
         return kVK_F4;
      case Qt::Key::Key_F5:
         return kVK_F5;
      case Qt::Key::Key_F6:
         return kVK_F6;
      case Qt::Key::Key_F7:
         return kVK_F7;
      case Qt::Key::Key_F8:
         return kVK_F8;
      case Qt::Key::Key_F9:
         return kVK_F9;
      case Qt::Key::Key_F10:
         return kVK_F10;
      case Qt::Key::Key_F11:
         return kVK_F11;
      case Qt::Key::Key_F12:
         return kVK_F12;
      case Qt::Key::Key_A:
         return kVK_ANSI_A;
      case Qt::Key::Key_B:
         return kVK_ANSI_B;
      case Qt::Key::Key_C:
         return kVK_ANSI_C;
      case Qt::Key::Key_D:
         return kVK_ANSI_D;
      case Qt::Key::Key_E:
         return kVK_ANSI_E;
      case Qt::Key::Key_F:
         return kVK_ANSI_F;
      case Qt::Key::Key_G:
         return kVK_ANSI_G;
      case Qt::Key::Key_H:
         return kVK_ANSI_H;
      case Qt::Key::Key_I:
         return kVK_ANSI_I;
      case Qt::Key::Key_J:
         return kVK_ANSI_J;
      case Qt::Key::Key_K:
         return kVK_ANSI_K;
      case Qt::Key::Key_L:
         return kVK_ANSI_L;
      case Qt::Key::Key_M:
         return kVK_ANSI_M;
      case Qt::Key::Key_N:
         return kVK_ANSI_N;
      case Qt::Key::Key_O:
         return kVK_ANSI_O;
      case Qt::Key::Key_P:
         return kVK_ANSI_P;
      case Qt::Key::Key_Q:
         return kVK_ANSI_Q;
      case Qt::Key::Key_R:
         return kVK_ANSI_R;
      case Qt::Key::Key_S:
         return kVK_ANSI_S;
      case Qt::Key::Key_T:
         return kVK_ANSI_T;
      case Qt::Key::Key_U:
         return kVK_ANSI_U;
      case Qt::Key::Key_V:
         return kVK_ANSI_V;
      case Qt::Key::Key_W:
         return kVK_ANSI_W;
      case Qt::Key::Key_X:
         return kVK_ANSI_X;
      case Qt::Key::Key_Y:
         return kVK_ANSI_Y;
      case Qt::Key::Key_Z:
         return kVK_ANSI_Z;
      case Qt::Key::Key_0:
         return kVK_ANSI_O;
      case Qt::Key::Key_1:
         return kVK_ANSI_1;
      case Qt::Key::Key_2:
         return kVK_ANSI_2;
      case Qt::Key::Key_3:
         return kVK_ANSI_3;
      case Qt::Key::Key_4:
         return kVK_ANSI_4;
      case Qt::Key::Key_5:
         return kVK_ANSI_5;
      case Qt::Key::Key_6:
         return kVK_ANSI_6;
      case Qt::Key::Key_7:
         return kVK_ANSI_7;
      case Qt::Key::Key_8:
         return kVK_ANSI_8;
      case Qt::Key::Key_9:
         return kVK_ANSI_9;
      default:
         return std::nullopt;
   }
}
