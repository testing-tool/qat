// (c) Copyright 2024, Qat’s Authors

#include <CocoaInterface.h>
#include <CocoaWidget.h>
#include <CocoaWidgetWrapper.h>
#include <CocoaWindowWrapper.h>
#include <NativeInterface.h>
#include <NativeInterfaceWidget.h>
#include <ObjectPicker.h>

#include <qat-server/plugins/IObjectPicker.h>
#include <qat-server/plugins/IWidget.h>

#include <QGuiApplication>
#include <QImage>
#include <QObject>

#include <iostream>
#include <map>
#include <memory>

namespace
{
struct Loader
{
   Loader()
   {
      CocoaInterface::QatActivateCocoaMultiThreading();
   }
};
} // namespace


extern "C"
{
   Qat::IWidget* CastObject(QObject* qtobject)
   {
      if (!qtobject)
      {
         return nullptr;
      }

      auto* nativeObject = qobject_cast<Qat::CocoaWidgetWrapper*>(qtobject);
      if (nativeObject)
      {
         return new Qat::CocoaWidget(nativeObject);
      }
      auto* nativeInterface = qobject_cast<Qat::NativeInterface*>(qtobject);
      if (nativeInterface)
      {
         return new Qat::NativeInterfaceWidget(nativeInterface);
      }

      return nullptr;
   }

   bool GetTopWindows(QObject** windowsArray, unsigned int* size)
   {
      if (!size)
      {
         std::cerr << "Invalid call to GetTopWindows()" << std::endl;
         return false;
      }
      const auto numWindows = Qat::CocoaWindowWrapper::GetNumOpenedWindows();
      if (*size == 0)
      {
         *size = static_cast<unsigned int>(numWindows);
         return true;
      }
      if (!windowsArray)
      {
         std::cerr << "Invalid call to GetTopWindows()" << std::endl;
         return false;
      }
      memset(windowsArray, 0, *size * sizeof(windowsArray[0]));
      const auto list = Qat::CocoaWindowWrapper::GetOpenedWindows(numWindows);
      for (auto i = 0; i < list.size(); ++i)
      {
         if (i < (int) *size)
         {
            windowsArray[i] = list[i];
         }
      }
      return true;
   }

   QImage* GrabImage(QObject* window)
   {
      auto* windowWrapper = qobject_cast<Qat::CocoaWindowWrapper*>(window);
      if (!windowWrapper)
      {
         return nullptr;
      }
      Qat::CocoaWidget widget(windowWrapper);
      QImage* result = nullptr;
      widget.GrabImage(
         [&result](const QImage& image)
         {
            result = new QImage(image);
         });
      return result;
   }

   Qat::IObjectPicker* CreatePicker(QObject* window)
   {
      if (!window)
      {
         return nullptr;
      }

      auto* windowWrapper = qobject_cast<Qat::CocoaWindowWrapper*>(window);
      if (!windowWrapper)
      {
         return nullptr;
      }

      /// Picker does not support NSPanel.
      if (windowWrapper->IsPanel())
      {
         return nullptr;
      }

      return new Qat::CocoaNativePlugin::ObjectPicker(windowWrapper);
   }

   Qat::INativeInterface* GetNativeInterface()
   {
      static auto* gNativeInterface = new Qat::NativeInterface();
      return gNativeInterface;
   }
}