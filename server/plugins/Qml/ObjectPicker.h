// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/plugins/IObjectPicker.h>

#include <QObject>
#include <QPointer>
#include <QQuickItem>

#include <atomic>

namespace Qat::QmlPlugin
{

class ObjectPicker : public IObjectPicker
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(ObjectPicker)
   Q_PROPERTY(QObject* pickedObject MEMBER mPickedObject NOTIFY ObjectPicked)
   Q_PROPERTY(bool hasNewObject MEMBER mHasNewObject NOTIFY HasNewObjectChanged)

public:
   /// Constructor
   explicit ObjectPicker(QObject* parent);

   /// Destructor
   ~ObjectPicker() override = default;

   /// \copydoc QObject::eventFilter
   bool eventFilter(QObject* object, QEvent* event) override;

   /// \copydoc IObjectPicker::Reset
   void Reset() override;

   /// \copydoc IObjectPicker::SetActivated
   void SetActivated(bool activate) override;

   /// \copydoc IObjectPicker::Pause
   void Pause() override;

   /// \copydoc IObjectPicker::Restore
   void Restore() override;

   /// Highlight the picked object by drawing a transparent rectangle above it
   /// \param[in] object The object to be highlighted
   Q_INVOKABLE void HighLightObject(QObject* object);

   /// Remove the transparent rectangle above the current object
   Q_INVOKABLE void CancelHighLighting();

signals:
   /// Emitted when an object is picked
   /// @{
   void ObjectPicked();
   void HasNewObjectChanged();
   /// @}

private:
   /// Get the current object under the mouse cursor
   /// \param[in] object The parent object
   /// \param[in] mousePosition The position of the mouse
   /// \param[in] deep If True, return the leaf-child under the mouse. Otherwise return
   ///            any parent of the same size
   /// \return The object under the mouse cursor
   QObject* GetPickedObject(QObject* object, QPoint mousePosition, bool deep);

   /// Perform actions when mouse is released.
   /// \param object Receiver of the event.
   /// \param event Mouse event.
   /// \return True upon success, False otherwise.
   bool HandleMouseReleaseEvent(QObject* object, QEvent* event);

   /// Perform actions when mouse moves over a widget.
   /// \param object Receiver of the event.
   /// \param event Mouse event.
   /// \return True upon success, False otherwise.
   bool HandleMHoverMoveEvent(QObject* object, QEvent* event);

   /// The last object picked
   QObject* mPickedObject{nullptr};

   /// The current highlighted object picked
   QObject* mHighlightedObject{nullptr};

   /// Flag indicating whether the picked object is currently highlighted or not
   bool mIsHighLighted{false};

   /// Flag indicating whether a new picked object is available or not
   bool mHasNewObject{false};

   /// The QML overlay displayed during highlighting
   QPointer<QQuickItem> mHighlightOverlay{nullptr};

   /// The QML rectangle displayed during highlighting
   QPointer<QQuickItem> mHighlightRectangle{nullptr};

   /// Flag indicating whether this picker is active or not
   bool mIsActive{false};
};

} // namespace Qat::QmlPlugin