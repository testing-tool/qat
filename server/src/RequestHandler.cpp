// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Exception.h>
#include <qat-server/RequestExecutor.h>
#include <qat-server/RequestHandler.h>
#include <qat-server/TcpServerLib.h>

#include <QIODevice>

#include <nlohmann/json.hpp>

#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace Qat
{

RequestHandler::RequestHandler(QObject* parent, std::shared_ptr<TcpServerLib> tcpServer)
    : QObject(parent)
    , mServer(std::move(tcpServer))
{
}

void RequestHandler::ConnectToHost(
   const std::string& hostName, int port, TcpServerLib::ClientIdType clientId) const
{
   if (!mServer)
   {
      throw Exception("Cannot connect to client: TCP server is null");
   }
   const std::string url = hostName + ":" + std::to_string(port);
   mServer->InitComm(clientId, url.c_str());
}

void RequestHandler::DisconnectFromHost(TcpServerLib::ClientIdType clientId) const
{
   std::cout << "Disconnecting client" << std::endl;
   if (!mServer)
   {
      throw Exception("Cannot disconnect to client: TCP server is null");
   }
   mServer->CloseComm(clientId);
}


void RequestHandler::Execute(const QString& request, quint16 clientId) noexcept
{
   if (!mServer)
   {
      std::cerr << "Cannot handle TCP request: TCP server is null" << std::endl;
      return;
   }
   std::string result;
   try
   {
      const auto command = nlohmann::json::parse(request.toStdString());
      RequestExecutor executor(command, this, clientId);
      result = executor.Run().dump();
   }
   catch (const Exception& e)
   {
      nlohmann::json error;
      error["error"] = e.what();
      result = error.dump();
   }
   catch (const std::exception& e)
   {
      nlohmann::json error;
      error["error"] = e.what();
      result = error.dump();
   }
   catch (...)
   {
      nlohmann::json error;
      error["error"] = "Unknown error while executing request";
      result = error.dump();
   }

   mServer->SendResponse(clientId, result.c_str());
}

void RequestHandler::SendMessage(
   TcpServerLib::ClientIdType clientId, const std::string& message) const
{
   if (!mServer)
   {
      throw Exception("Cannot send message to client: TCP server is null");
   }
   mServer->SendMessage(clientId, message.c_str());
}

void RequestHandler::SendMessage(quint16 clientId, const QString& message) const
{
   try
   {
      SendMessage(clientId, message.toStdString());
   }
   catch (const std::exception& e)
   {
      std::cerr << e.what() << std::endl;
   }
}

} // namespace Qat