// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/CommCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/RequestHandler.h>
#include <qat-server/events/SignalListener.h>

#include <QMetaMethod>
#include <QMetaProperty>
#include <QObject>
#include <QPointer>
#include <QString>

#include <iostream>
#include <string>

namespace Qat
{

using namespace Constants;

std::map<std::string, QPointer<SignalListener>> CommCommandExecutor::mListenerCache;

std::unique_ptr<SignalListener> CreateListener(
   QObject* object,
   std::string propertyName,
   RequestHandler* requestHandler,
   Qat::TcpServerLib::ClientIdType clientId)
{
   // Using () at the end of the property name forces the connection to use this signal
   // even if a property with the same name exists
   bool isSignal = false;
   if (propertyName.rfind(')') != std::string::npos)
   {
      isSignal = true;
   }
   std::unique_ptr<SignalListener> listener = nullptr;
   QMetaMethod metaSignal;

   auto index = isSignal ? -1
                         : object->metaObject()->indexOfProperty(propertyName.c_str());
   if (!isSignal && index >= 0)
   {
      auto metaProp = object->metaObject()->property(index);
      if (!metaProp.hasNotifySignal())
      {
         throw Exception(
            "Cannot connect property: it has no associated notification signal");
      }
      metaSignal = metaProp.notifySignal();
      listener = std::make_unique<SignalListener>(requestHandler, clientId);
      // Send current value when binding properties
      listener->AttachTo(object, propertyName);
   }
   else
   {
      if (propertyName.rfind(')') == std::string::npos)
      {
         propertyName = propertyName + "()";
      }
      const auto signalName = QMetaObject::normalizedSignature(propertyName.c_str())
                                 .toStdString();

      index = object->metaObject()->indexOfSignal(signalName.c_str());
      if (index >= 0)
      {
         metaSignal = object->metaObject()->method(index);
         if (metaSignal.parameterCount() > 0)
         {
            listener.reset(SignalListener::Create(
               requestHandler, clientId, metaSignal.parameterCount()));
         }
         else
         {
            listener = std::make_unique<SignalListener>(requestHandler, clientId);
         }
      }
      else
      {
         throw Exception("Cannot connect property or signal: property was not found");
      }
   }

   const auto slotIndex = listener->metaObject()->indexOfMethod("Notify()");
   if (slotIndex < 0)
   {
      throw Exception("Failed to create the connection, slot not found");
   }

   const auto connection = QObject::connect(
      object, metaSignal, listener.get(), listener->metaObject()->method(slotIndex));

   if (!connection)
   {
      throw Exception("Failed to create the connection, invalid parameters");
   }
   return listener;
}

CommCommandExecutor::CommCommandExecutor(
   const nlohmann::json& request,
   RequestHandler* requestHandler,
   TcpServerLib::ClientIdType clientId)
    : BaseCommandExecutor(request)
    , mRequestHandler(requestHandler)
    , mClientId(clientId)
{
   for (const auto& field : {OBJECT_ATTRIBUTE})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json CommCommandExecutor::Run() const
{
   const auto cmdName = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   if (cmdName == Communication::INIT)
   {
      const auto args = mRequest.at(ARGUMENTS);
      if (!args.contains(HOST) || !args.contains(PORT))
      {
         throw Exception("Host and port arguments are mandatory");
      }

      const auto hostName = args.at(HOST).get<std::string>();
      const auto port = args.at(PORT).get<int>();
      mRequestHandler->ConnectToHost(hostName, port, mClientId);
   }
   else if (cmdName == Communication::CLOSE)
   {
      mRequestHandler->DisconnectFromHost(mClientId);
   }
   else if (cmdName == Communication::CONNECT)
   {
      auto* object = FindObject();
      if (!object)
      {
         throw Exception("Cannot connect property: object not found");
      }
      auto propertyName = mRequest.at(ARGUMENTS).get<std::string>();

      auto listener = CreateListener(object, propertyName, mRequestHandler, mClientId);
      if (!listener)
      {
         throw Exception("Failed to create listener");
      }
      const auto listenerId = listener->GetId();
      mListenerCache[listenerId] = listener.release();

      nlohmann::json result;
      result["found"] = true;
      result[OBJECT_ID] = listenerId;
      return result;
   }
   else if (cmdName == Communication::DISCONNECT)
   {
      bool found = false;
      const auto callbackId = mRequest.at(ARGUMENTS).get<std::string>();
      if (mListenerCache.contains(callbackId))
      {
         found = true;
         auto listener = mListenerCache[callbackId];
         mListenerCache.erase(callbackId);
         if (listener)
         {
            listener->deleteLater();
         }
      }

      nlohmann::json result;
      result["found"] = found;
      return result;
   }
   else
   {
      throw Exception("Command '" + cmdName + "' is not supported");
   }

   nlohmann::json result;
   result["found"] = true;
   return result;
}

} // namespace Qat