// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/TouchCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/commands/CommandHelper.h>
#include <qat-server/commands/Devices.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>
#include <qat-server/qt-tools/ObjectLocator.h>
#include <qat-server/qt-tools/WidgetLocator.h>

#include <QEvent>
#include <QGuiApplication>
#include <QJsonArray>
#include <QList>
#include <QObject>
#include <QWindow>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   #include <QTouchEvent>
using TouchPointType = QTouchEvent::TouchPoint;
#else
   #include <QEventPoint>
using TouchPointType = QEventPoint;
#endif

// Qt private headers are required to manage simulated touch device and events
#include <QtGui/qpa/qwindowsysteminterface.h>
#include <QtGui/qpa/qwindowsysteminterface_p.h>

#include <iostream>
#include <string>
#include <tuple>
#include <vector>

namespace
{
// Local enum for Qt5 compatibility
enum QatTouchPointState : std::uint8_t
{
   Pressed,
   Released,
   Updated,
   Stationary
};

/// \brief Generate a Touch point for current Qt version
/// \param id ID of the point
/// \param state State of the point (Pressed, Release,...)
/// \param sceneCoordinates Coordinates of the point in its parent scene
/// \param globalCoordinates Coordinates of the point in the screen
/// \return The touch point ready to be used with SendTouchEvent()
TouchPointType CreateTouchPoint(
   int id,
   QatTouchPointState state,
   [[maybe_unused]] const QPoint& sceneCoordinates,
   const QPoint& globalCoordinates)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   auto pointState = Qt::TouchPointStationary;
   switch (state)
   {
      case Pressed:
         pointState = Qt::TouchPointPressed;
         break;
      case Released:
         pointState = Qt::TouchPointReleased;
         break;
      case Updated:
         pointState = Qt::TouchPointMoved;
         break;
      default:
      case Stationary:
         pointState = Qt::TouchPointStationary;
         break;
   }
   QTouchEvent::TouchPoint point(id);
   point.setState(pointState);
   point.setScreenPos(globalCoordinates);
   return point;
#else
   auto pointState = QEventPoint::State::Stationary;
   switch (state)
   {
      case Pressed:
         pointState = QEventPoint::State::Pressed;
         break;
      case Released:
         pointState = QEventPoint::State::Released;
         break;
      case Updated:
         pointState = QEventPoint::State::Updated;
         break;
      default:
      case Stationary:
         pointState = QEventPoint::State::Stationary;
         break;
   }
   return TouchPointType{id, pointState, sceneCoordinates, globalCoordinates};
#endif
}

/// \brief Send a touch event to the plugin platform.
/// \warning This is based on Qt's private functions since there is no public API (qApp->sendEvent crashes)
/// \note This code is similar to QTest's implementation, which cannot be used since it
/// will not be available in tested application.
/// \param window The parent window to send the event to
/// \param points A list of touch points coordinates with associated properties
/// \param modifiers Keyboard modifier (none by default)
/// \return True if a widget accepted the event, False otherwise
bool SendTouchEvent(
   QWindow* window,
   const QList<TouchPointType>& points,
   Qt::KeyboardModifiers modifiers = Qt::NoModifier)
{
   if (!window)
   {
      return false;
   }
   return QWindowSystemInterface::handleTouchEvent<
      QWindowSystemInterface::SynchronousDelivery>(
      window,
      Qat::Devices::GetTouchDevice(),
      QWindowSystemInterfacePrivate::toNativeTouchPoints(points, window),
      modifiers);
}


std::vector<QPoint> GetCoordinateList(
   const nlohmann::json& args, const std::string& xArg, const std::string& yArg)
{
   std::vector<QPoint> coordinatesList;
   if (args.contains(xArg) && args.contains(yArg))
   {
      if (args.at(xArg).is_number() && args.at(yArg).is_number())
      {
         const auto x = args.at(xArg).get<int>();
         const auto y = args.at(yArg).get<int>();
         coordinatesList.emplace_back(QPoint{x, y});
      }
      else if (args.at(xArg).is_array() && args.at(yArg).is_array())
      {
         const auto xList = args.at(xArg).get<std::vector<int>>();
         const auto yList = args.at(yArg).get<std::vector<int>>();
         if (xList.size() != yList.size())
         {
            throw Qat::Exception("Cannot execute touch operation: "
                                 "x and y arguments must have the same size");
         }
         // todo Use std view/range from C++20/23
         for (auto i = 0U; i < xList.size(); ++i)
         {
            coordinatesList.emplace_back(QPoint{xList[i], yList[i]});
         }
      }
      else
      {
         throw Qat::Exception("Cannot execute touch operation: "
                              "Given coordinates are not in a valid format");
      }
   }
   return coordinatesList;
}
} // namespace

namespace Qat
{

using namespace Constants;

TouchCommandExecutor::TouchCommandExecutor(const nlohmann::json& request)
    : BaseCommandExecutor(request)
{
   for (const auto& field : {OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json TouchCommandExecutor::Run() const
{
   nlohmann::json result;
   result["status"] = true;
   auto* object = FindObject();
   const auto action = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.at(ARGUMENTS);

   auto widget = WidgetWrapper::Cast(object);
   if (!widget)
   {
      throw Exception("Cannot execute touch operation: "
                      "Object is not a supported Widget");
   }

   const auto parameters = BuildParameters(args, *widget);

   // Generate touch events
   // Note: the order is important to make drag events work
   bool actionFound = false;
   if (action == Touch::PRESS || action == Touch::TAP || action == Touch::DRAG)
   {
      SendPressEvent(parameters, *widget);
      actionFound = true;
   }

   if (action == Touch::MOVE)
   {
      SendMoveEvent(parameters, *widget);
      actionFound = true;
   }
   else if (action == Touch::DRAG)
   {
      SendDragEvent(parameters, *widget);
      actionFound = true;
   }

   // Most events need a Release event at the end
   if (action == Touch::RELEASE || action == Touch::TAP || action == Touch::DRAG)
   {
      SendReleaseEvent(parameters, *widget);
      actionFound = true;
   }

   if (!actionFound)
   {
      throw Exception(
         "Cannot send unknown touch event '" + action + "': Event type is not supported");
   }
   return result;
}

TouchCommandExecutor::TouchParameters
TouchCommandExecutor::BuildParameters(const nlohmann::json& args, IWidget& widget)
{
   TouchParameters parameters{.modifiers = CommandHelper::GetModifier(args)};

   // Optional position arguments (X, Y)
   parameters.localCoordinates = GetCoordinateList(args, Args::X, Args::Y);
   if (!parameters.localCoordinates.empty())
   {
      for (const auto& coordinates : parameters.localCoordinates)
      {
         if (!widget.Contains(coordinates))
         {
            throw Exception("Cannot execute touch operation: "
                            "Given coordinates are outside widget's boundaries");
         }
         const auto globalCoordinates = widget.MapToGlobal(coordinates).toPoint();
         parameters.globalCoordinates.push_back(globalCoordinates);
         const auto sceneCoordinates = widget.MapToScene(coordinates).toPoint();
         parameters.sceneCoordinates.push_back(sceneCoordinates);
      }
   }
   else
   {
      QPoint coordinates;
      QPoint globalCoordinates;
      std::tie(coordinates, globalCoordinates) = WidgetLocator::GetWidgetCenter(
         widget.GetQtObject());
      parameters.localCoordinates.push_back(coordinates);
      parameters.globalCoordinates.push_back(globalCoordinates);
      const auto sceneCoordinates = widget.MapToScene(coordinates).toPoint();
      parameters.sceneCoordinates.push_back(sceneCoordinates);
   }

   // Optional move arguments (DX, DY)
   parameters.moveCoordinates = GetCoordinateList(args, Args::DX, Args::DY);
   if (parameters.moveCoordinates.empty())
   {
      parameters.moveCoordinates.push_back(QPoint{0, 0});
   }

   return parameters;
}

void TouchCommandExecutor::SendPressEvent(
   const TouchParameters& parameters, IWidget& widget)
{
   QList<TouchPointType> touchPoints;
   for (auto i = 0U; i < parameters.sceneCoordinates.size(); ++i)
   {
      touchPoints << CreateTouchPoint(
         static_cast<int>(i + 1),
         QatTouchPointState::Pressed,
         parameters.sceneCoordinates[i],
         parameters.globalCoordinates[i]);
   }

   if (!SendTouchEvent(widget.GetWindow(), touchPoints, parameters.modifiers))
   {
      throw Exception("Cannot send touch press event");
   }
}

void TouchCommandExecutor::SendMoveEvent(
   const TouchParameters& parameters, IWidget& widget)
{
   QList<TouchPointType> touchPoints;
   for (auto i = 0U; i < parameters.sceneCoordinates.size(); ++i)
   {
      touchPoints << CreateTouchPoint(
         static_cast<int>(i + 1),
         QatTouchPointState::Updated,
         parameters.sceneCoordinates[i],
         parameters.globalCoordinates[i]);
   }

   if (!SendTouchEvent(widget.GetWindow(), touchPoints, parameters.modifiers))
   {
      throw Exception("Cannot send touch move event");
   }
}

void TouchCommandExecutor::SendDragEvent(
   const TouchParameters& parameters, IWidget& widget)
{
   std::vector<int> lengthList;
   for (auto i = 0U; i < parameters.localCoordinates.size(); ++i)
   {
      auto moveCoordinates = parameters.moveCoordinates.front();
      if (i < parameters.moveCoordinates.size())
      {
         moveCoordinates = parameters.moveCoordinates.at(i);
      }
      auto length = static_cast<int>(
         std::sqrt(std::pow(moveCoordinates.x(), 2) + std::pow(moveCoordinates.y(), 2)));

      lengthList.push_back(length);
   }
   const auto maxLength = std::max_element(lengthList.cbegin(), lengthList.cend());
   // Avoid generating too many events
   constexpr int maxNbEvents{20};
   const auto length = std::min(*maxLength, maxNbEvents);

   QEventLoop::ProcessEventsFlags flags;
   flags.setFlag(QEventLoop::ExcludeUserInputEvents);
   constexpr int processEventsDelay{25};
   for (auto step = 0; step <= length; ++step)
   {
      /// \todo Find another way to run async events during touch drag since this causes issues
      /// when running tests in parallel.
      qApp->processEvents(flags, processEventsDelay);

      QList<TouchPointType> touchPoints;
      const auto nbPoints = static_cast<int>(parameters.sceneCoordinates.size());
      for (auto i = 0; i < nbPoints; ++i)
      {
         auto moveCoordinates = parameters.moveCoordinates.front();
         if (i < static_cast<int>(parameters.moveCoordinates.size()))
         {
            moveCoordinates = parameters.moveCoordinates.at(i);
         }
         const auto stepCoordinates = moveCoordinates * step / length;

         touchPoints << CreateTouchPoint(
            i + 1,
            QatTouchPointState::Updated,
            parameters.sceneCoordinates[i] + stepCoordinates,
            parameters.globalCoordinates[i] + stepCoordinates);
      }

      if (!SendTouchEvent(widget.GetWindow(), touchPoints, parameters.modifiers))
      {
         touchPoints.clear();
         for (auto i = 0; i < nbPoints; ++i)
         {
            auto moveCoordinates = parameters.moveCoordinates.front();
            if (i < static_cast<int>(parameters.moveCoordinates.size()))
            {
               moveCoordinates = parameters.moveCoordinates.at(i);
            }
            const auto stepCoordinates = moveCoordinates * step / length;

            touchPoints << CreateTouchPoint(
               i + 1,
               QatTouchPointState::Released,
               parameters.sceneCoordinates[i] + stepCoordinates,
               parameters.globalCoordinates[i] + stepCoordinates);
         }
         SendTouchEvent(widget.GetWindow(), touchPoints, parameters.modifiers);
         throw Exception("Cannot send touch event");
      }
   }
}

void TouchCommandExecutor::SendReleaseEvent(
   const TouchParameters& parameters, IWidget& widget)
{
   QList<TouchPointType> touchPoints;
   for (auto i = 0U; i < parameters.sceneCoordinates.size(); ++i)
   {
      auto moveCoordinates = parameters.moveCoordinates.front();
      if (i < parameters.moveCoordinates.size())
      {
         moveCoordinates = parameters.moveCoordinates.at(i);
      }
      touchPoints << CreateTouchPoint(
         static_cast<int>(i + 1),
         QatTouchPointState::Released,
         parameters.sceneCoordinates[i] + moveCoordinates,
         parameters.globalCoordinates[i] + moveCoordinates);
   }

   if (!SendTouchEvent(widget.GetWindow(), touchPoints, parameters.modifiers))
   {
      throw Exception("Cannot send touch release event");
   }
}

} // namespace Qat