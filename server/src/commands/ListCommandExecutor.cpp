// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/ListCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>
#include <qat-server/qt-tools/ObjectLocator.h>
#include <qat-server/qt-tools/QVariantToJson.h>

#include <QGuiApplication>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaMethod>
#include <QObject>
#include <QVariant>

#include <sstream>
#include <string>
#include <vector>

namespace Qat
{
using namespace Constants;

ListCommandExecutor::ListCommandExecutor(const nlohmann::json& request)
    : BaseCommandExecutor(request)
{
   for (const auto& field : {OBJECT_ATTRIBUTE})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json ListCommandExecutor::Run() const
{
   nlohmann::json result;
   const auto attribute = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   if (attribute == PROPERTIES)
   {
      auto* object = FindObject();
      if (!object)
      {
         throw Exception("Cannot list properties of NULL object: "
                         "FindObject() returned a NULL pointer");
      }

      result[PROPERTIES] = ListProperties(object);
      return result;
   }
   if (attribute == METHODS)
   {
      auto* object = FindObject();
      if (!object)
      {
         throw Exception("Cannot list methods of NULL object: "
                         "FindObject() returned a NULL pointer");
      }

      result[METHODS] = ListMethods(object);
      return result;
   }
   if (attribute == OBJECT_DEFINITION)
   {
      if (!mRequest.contains(OBJECT_DEFINITION))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field " +
            OBJECT_DEFINITION);
      }
      const auto& objectDefinition = mRequest.at(OBJECT_DEFINITION);
      const auto objects = FindObjects(qApp, objectDefinition, true);
      nlohmann::json childrenArray;
      for (auto* const object : objects)
      {
         nlohmann::json objectJson;
         objectJson[CACHE_UID] = RegisterObject(object);
         childrenArray.push_back(objectJson);
      }
      result[OBJECT_DEFINITION] = childrenArray;
      return result;
   }
   if (attribute == TOP_WINDOWS)
   {
      const auto windows = WidgetWrapper::GetTopWindows();
      nlohmann::json childrenArray;
      for (auto* const object : windows)
      {
         nlohmann::json objectJson;
         objectJson[CACHE_UID] = RegisterObject(object);
         childrenArray.push_back(objectJson);
      }
      result[OBJECT_DEFINITION] = childrenArray;
      return result;
   }
   if (attribute == VERSION_INFO)
   {
      QT_VERSION_MAJOR;
      std::stringstream versionStream;
      versionStream << QT_VERSION_MAJOR << "." << QT_VERSION_MINOR << "."
                    << QT_VERSION_PATCH;

      result[CURRENT_QT_VERSION] = versionStream.str();
      return result;
   }
   throw Exception(
      "Invalid list request: "
      "Attribute not supported: " +
      attribute);
}

nlohmann::json ListCommandExecutor::ListProperties(QObject* object)
{
   nlohmann::json propertyList;
   const auto* metaObject = object->metaObject();
   const auto numProperties = metaObject->propertyCount();
   for (auto i = 0; i < numProperties; ++i)
   {
      const std::string propertyName = metaObject->property(i).name();
      const auto value = object->property(propertyName.c_str());
      if (!value.isValid())
      {
         continue;
      }
      if (value.canConvert<QObject*>())
      {
         auto* childObject = value.value<QObject*>();

         nlohmann::json objectJson;
         objectJson[CACHE_UID] = RegisterObject(childObject);

         propertyList[propertyName] = "<object>:" + objectJson.dump();
      }
      else
      {
         propertyList[propertyName] = ToJson(value);
      }
   }

   // Add custom Type property
   propertyList[OBJECT_TYPE] = ObjectLocator::GetObjectType(object);

   // Add custom Id property
   const auto widget = WidgetWrapper::Cast(object);
   if (widget)
   {
      propertyList[OBJECT_ID] = widget->GetId();
   }

   return propertyList;
}

nlohmann::json ListCommandExecutor::ListMethods(QObject* object)
{
   nlohmann::json methodList;
   std::map<std::string, nlohmann::json> methodMap;
   const auto nbMethods = object->metaObject()->methodCount();
   for (int i = 0; i < nbMethods; ++i)
   {
      const auto method = object->metaObject()->method(i);
      const auto signature = method.methodSignature().toStdString();
      const auto methodName = method.name().toStdString();
      if (methodName.starts_with("_"))
      {
         continue; // ignore internal Qt methods
      }
      const auto paramTypes = method.parameterTypes();
      const auto paramNames = method.parameterNames();
      const auto* const returnType = method.typeName();
      const auto type = method.methodType();
      std::string methodType;
      switch (type)
      {
         case QMetaMethod::Signal:
            methodType = "signal";
            break;

         case QMetaMethod::Slot:
            methodType = "slot";
            break;

         default:
            break;
      }
      // Build signature with both arg types and names
      std::stringstream description;
      description << methodName << "(";
      for (auto p = 0; p < paramTypes.size(); ++p)
      {
         if (p > 0)
         {
            description << ", ";
         }
         description << paramTypes[p].toStdString();
         if (p < paramNames.size())
         {
            const auto& paramName = paramNames[p];
            if (!paramName.isEmpty())
            {
               description << " " << paramName.toStdString();
            }
         }
      }
      description << ")";

      nlohmann::json methodDefinition;
      methodDefinition["type"] = methodType;
      methodDefinition["name"] = description.str();
      methodDefinition["returnType"] = returnType;
      // Sort by signature i.e name + args to support overloads
      methodMap[signature] = methodDefinition;
   }
   for (const auto& [_, method] : methodMap)
   {
      methodList.push_back(method);
   }
   return methodList;
}

} // namespace Qat