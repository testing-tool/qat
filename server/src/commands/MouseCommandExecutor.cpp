// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/MouseCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/commands/CommandHelper.h>
#include <qat-server/commands/Devices.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>
#include <qat-server/qt-tools/ObjectLocator.h>
#include <qat-server/qt-tools/WidgetLocator.h>

#include <QCursor>
#include <QEvent>
#include <QGuiApplication>
#include <QJsonArray>
#include <QMouseEvent>
#include <QObject>
#include <QObjectData>
#include <QRegularExpression>
#include <QWheelEvent>
#include <QWindow>

#include <string>
#include <tuple>

namespace
{

/// Send the given mouse event to the given receiver.
/// For Qt widgets, the event will be sent to the parent window using
/// Qt event system and QGuiApplication::sendEvent() function.
/// For native widgets, the event will be sent directly to the underlying
/// QObject by calling its event() function.
/// \param widget The receiver widget
/// \param event A mouse event
/// \return True if the event was handled, False otherwise
bool SendMouseEvent(Qat::IWidget& widget, QEvent* event)
{
   if (widget.GetWindow())
   {
      return qApp->sendEvent(widget.GetWindow(), event);
   }
   if (widget.GetQtObject())
   {
      return widget.GetQtObject()->event(event);
   }
   return false;
}

} // anonymous namespace

namespace Qat
{

using namespace Constants;

MouseCommandExecutor::MouseCommandExecutor(const nlohmann::json& request)
    : BaseCommandExecutor(request)
{
   for (const auto& field : {OBJECT_DEFINITION, OBJECT_ATTRIBUTE, ARGUMENTS})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json MouseCommandExecutor::Run() const
{
   nlohmann::json result;
   result["status"] = true;
   auto* object = FindObject();
   const auto action = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto args = mRequest.at(ARGUMENTS);

   auto widget = WidgetWrapper::Cast(object);
   if (!widget)
   {
      throw Exception("Cannot execute mouse operation: "
                      "Object is not a supported Widget");
   }

   const auto parameters = BuildParameters(args, *widget, object, action);

   if (parameters.button == Qt::MouseButton::NoButton && action != Mouse::MOVE)
   {
      throw Qat::Exception("Cannot run mouse command: "
                           "Button cannot be None for this event");
   }

   // Generate mouse events
   // Note: the order is important to make click and drag events work
   bool actionFound = false;
   bool eventAccepted = false;
   if (
      action == Mouse::PRESS || action == Mouse::CLICK || action == Mouse::DRAG ||
      action == Mouse::DBL_CLICK)
   {
      eventAccepted = SendPressEvent(parameters, *widget) || eventAccepted;
      actionFound = true;
   }
   if (action == Mouse::DBL_CLICK)
   {
      eventAccepted = SendDoubleClickEvent(parameters, *widget) || eventAccepted;
      actionFound = true;
   }

   if (action == Mouse::MOVE)
   {
      eventAccepted = SendMoveEvent(parameters, *widget) || eventAccepted;
      actionFound = true;
   }
   else if (action == Mouse::DRAG)
   {
      SendDragEvent(parameters, *widget);
      actionFound = true;
   }
   else if (action == Mouse::SCROLL)
   {
      // Scene3D does not always accept wheel events
      eventAccepted = SendScrollEvent(parameters, *widget, object) || eventAccepted;
      actionFound = true;
   }

   // Most events need a Release event at the end
   if (
      action == Mouse::RELEASE || action == Mouse::CLICK || action == Mouse::DRAG ||
      action == Mouse::DBL_CLICK)
   {
      eventAccepted = SendReleaseEvent(parameters, *widget, action) || eventAccepted;
      actionFound = true;
   }

   if (!actionFound)
   {
      throw Exception(
         "Cannot send unknown event '" + action +
         "': "
         "Event type is not supported");
   }
   if (!eventAccepted)
   {
      result["warning"] = "No widget accepted this event";
   }
   return result;
}

MouseCommandExecutor::MouseParameters MouseCommandExecutor::BuildParameters(
   const nlohmann::json& args,
   IWidget& widget,
   QObject* object,
   const std::string& action)
{
   MouseParameters parameters{
      .button = CommandHelper::GetButton(args),
      .modifiers = CommandHelper::GetModifier(args)};

   // Optional position arguments (X, Y)
   if (args.contains(Args::X) && args.contains(Args::Y))
   {
      const auto x = args.at(Args::X).get<int>();
      const auto y = args.at(Args::Y).get<int>();
      parameters.localCoordinates = QPoint(x, y);
      if (action != Mouse::MOVE && !widget.Contains(parameters.localCoordinates))
      {
         throw Exception("Cannot execute mouse operation: "
                         "Given coordinates are outside widget's boundaries");
      }
      parameters.globalCoordinates = widget.MapToGlobal(parameters.localCoordinates)
                                        .toPoint();
   }
   else
   {
      std::tie(
         parameters.localCoordinates,
         parameters.globalCoordinates) = WidgetLocator::GetWidgetCenter(object);
   }
   parameters.localCoordinates = widget.MapToScene(parameters.localCoordinates).toPoint();

   // Optional move arguments (DX, DY)
   parameters.moveCoordinates = QPoint{0, 0};
   if (args.contains(Args::DX) && args.contains(Args::DY))
   {
      const auto x = args.at(Args::DX).get<int>();
      const auto y = args.at(Args::DY).get<int>();
      parameters.moveCoordinates = QPoint(x, y);
   }

   return parameters;
}

bool MouseCommandExecutor::SendPressEvent(
   const MouseParameters& parameters, IWidget& widget)
{
   QMouseEvent pressEvent(
      QEvent::Type::MouseButtonPress,
      parameters.localCoordinates,
      parameters.localCoordinates,
      parameters.globalCoordinates,
      parameters.button,
      Qt::MouseButtons().setFlag(parameters.button),
      parameters.modifiers,
      Devices::GetMouseDevice());

   if (!SendMouseEvent(widget, &pressEvent))
   {
      throw Exception("Cannot send press event");
   }

   return pressEvent.isAccepted();
}

bool MouseCommandExecutor::SendDoubleClickEvent(
   const MouseParameters& parameters, IWidget& widget)
{
   QMouseEvent doubleClickEvent(
      QEvent::Type::MouseButtonDblClick,
      parameters.localCoordinates,
      parameters.localCoordinates,
      parameters.globalCoordinates,
      parameters.button,
      Qt::MouseButtons().setFlag(parameters.button),
      parameters.modifiers,
      Devices::GetMouseDevice());

   if (!SendMouseEvent(widget, &doubleClickEvent))
   {
      throw Exception("Cannot send double-click event");
   }

   return doubleClickEvent.isAccepted();
}

bool MouseCommandExecutor::SendMoveEvent(
   const MouseParameters& parameters, IWidget& widget)
{
   QMouseEvent moveEvent(
      QEvent::Type::MouseMove,
      parameters.localCoordinates,
      parameters.localCoordinates,
      parameters.globalCoordinates,
      Qt::NoButton,
      Qt::MouseButtons().setFlag(parameters.button),
      parameters.modifiers,
      Devices::GetMouseDevice());

   if (!SendMouseEvent(widget, &moveEvent))
   {
      // Make sure the mouse is not stuck in a pressed state
      QMouseEvent releaseEvent(
         QEvent::Type::MouseButtonRelease,
         parameters.localCoordinates,
         parameters.localCoordinates,
         parameters.globalCoordinates,
         parameters.button,
         Qt::NoButton,
         parameters.modifiers,
         Devices::GetMouseDevice());
      SendMouseEvent(widget, &releaseEvent);

      throw Exception("Cannot send move event");
   }
   return moveEvent.isAccepted();
}

bool MouseCommandExecutor::SendDragEvent(
   const MouseParameters& parameters, IWidget& widget)
{
   auto length = static_cast<int>(std::sqrt(
      std::pow(parameters.moveCoordinates.x(), 2) +
      std::pow(parameters.moveCoordinates.y(), 2)));

   // Avoid generating too many events
   const int maxDragLength{20};
   length = std::min(length, maxDragLength);
   QEventLoop::ProcessEventsFlags flags;
   flags.setFlag(QEventLoop::ExcludeUserInputEvents);
   // This freezes the execution when using QWidgets
   //flags.setFlag(QEventLoop::WaitForMoreEvents);
   const int processEventsDelay{50};
   for (int i = 0; i <= length; ++i)
   {
      /// \todo Find another way to run async events during mouse drag since this causes issues
      /// when running tests in parallel.
      qApp->processEvents(flags, processEventsDelay);
      const auto stepCoordinates = parameters.moveCoordinates * i / length;

      QMouseEvent moveEvent(
         QEvent::Type::MouseMove,
         parameters.localCoordinates + stepCoordinates,
         parameters.localCoordinates + stepCoordinates,
         parameters.globalCoordinates + stepCoordinates,
         Qt::NoButton,
         Qt::MouseButtons().setFlag(parameters.button),
         parameters.modifiers,
         Devices::GetMouseDevice());

      if (!SendMouseEvent(widget, &moveEvent))
      {
         // Make sure the mouse is not stuck in a pressed state
         QMouseEvent releaseEvent(
            QEvent::Type::MouseButtonRelease,
            parameters.localCoordinates,
            parameters.localCoordinates,
            parameters.globalCoordinates,
            parameters.button,
            Qt::NoButton,
            parameters.modifiers,
            Devices::GetMouseDevice());
         SendMouseEvent(widget, &releaseEvent);

         throw Exception("Cannot send move event");
      }
   }

   return true;
}

bool MouseCommandExecutor::SendScrollEvent(
   const MouseParameters& parameters, IWidget& widget, QObject* object)
{
   QWheelEvent wheelEvent(
      parameters.localCoordinates,
      parameters.globalCoordinates,
      {0, 0}, // pixelDelta
      parameters.moveCoordinates,
      Qt::MouseButtons(),
      parameters.modifiers,
      Qt::ScrollPhase::NoScrollPhase,
      false,
      Qt::MouseEventSynthesizedByApplication
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
      ,
      Devices::GetMouseDevice()
#endif
   );

   if (!SendMouseEvent(widget, &wheelEvent))
   {
      throw Exception("Cannot send wheel event");
   }

   // Scene3D does not always accept wheel events
   const bool eventAccepted = wheelEvent.isAccepted() ||
                              object->inherits("Qt3DRender::Scene3DItem");

   QMouseEvent releaseEvent(
      QEvent::Type::MouseButtonRelease,
      parameters.localCoordinates,
      parameters.localCoordinates,
      parameters.globalCoordinates,
      parameters.button,
      Qt::MouseButtons(),
      parameters.modifiers,
      Devices::GetMouseDevice());

   if (!SendMouseEvent(widget, &releaseEvent))
   {
      throw Exception("Cannot send release event");
   }

   return eventAccepted;
}

bool MouseCommandExecutor::SendReleaseEvent(
   const MouseParameters& parameters, IWidget& widget, const std::string& action)
{
   QMouseEvent releaseEvent(
      QEvent::Type::MouseButtonRelease,
      parameters.localCoordinates + parameters.moveCoordinates,
      parameters.localCoordinates + parameters.moveCoordinates,
      parameters.globalCoordinates + parameters.moveCoordinates,
      parameters.button,
      Qt::MouseButtons(),
      parameters.modifiers,
      Devices::GetMouseDevice());

   if (!SendMouseEvent(widget, &releaseEvent))
   {
      throw Exception("Cannot send release event");
   }
   if (action == Mouse::RELEASE || action == Mouse::CLICK)
   {
      return releaseEvent.isAccepted();
   }
   return false;
}

} // namespace Qat