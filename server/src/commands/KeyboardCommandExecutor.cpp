// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/KeyboardCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/commands/Devices.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>

#include <QEvent>
#include <QGuiApplication>
#include <QJsonArray>
#include <QKeyEvent>
#include <QKeySequence>
#include <QObject>
#include <QObjectData>
#include <QWindow>

#include <iostream>
#include <string>
#include <tuple>

namespace
{

/// \brief Small class implementing RAII for keyboard focus
class FocusHolder final
{
public:
   /// Constructor. Takes the focus.
   /// \param[in] widget Pointer to the widget requiring the focus
   explicit FocusHolder(Qat::IWidget* widget)
       : mWidget(widget)
   {
      if (!mWidget)
      {
         return;
      }
      mWidget->ForceActiveFocus(Qt::ActiveWindowFocusReason);
      qApp->processEvents();
   }

   /// Destructor. Releases the focus.
   ~FocusHolder()
   {
      if (mWidget)
      {
         mWidget->ReleaseActiveFocus();
      }
   }

private:
   /// Widget requesting the focus
   Qat::IWidget* mWidget;
};

/// Split the given Key combination into a key and a list modifiers
/// \tparam T Qt::Key on Qt5, QKeyCombination on Qt6
/// \param combination A Key combination
/// \return A pair of key and modifiers
template<typename T>
std::pair<Qt::Key, Qt::KeyboardModifiers> SplitKeyCombination(const T& combination)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   const auto modifiers = static_cast<Qt::KeyboardModifiers>(
      combination & Qt::KeyboardModifierMask);
   const auto key = Qt::Key(combination & ~Qt::KeyboardModifierMask);
   return std::make_pair(key, modifiers);
#else
   return std::make_pair(combination.key(), combination.keyboardModifiers());
#endif
}

/// Simple struct to support both text and special keys
struct KeyEventParams
{
   /// The special key or 0 to enter text
   int key{0};
   /// The text or 0 for special keys
   char text{0};
};

/// Extract special keys from the given text and convert them to a list of KeyEventParams
/// \param[in] text The original text
/// \return A list of KeyEventParams allowing to build QKeyEvents
std::vector<KeyEventParams> ExtractSpecialKeys(const std::string& text)
{
   std::vector<KeyEventParams> result;
   auto start = text.find('<');
   auto end = text.find('>');
   // No special key found: return the entire text
   if (start == std::string::npos || end == std::string::npos || start > end)
   {
      for (const auto c : text)
      {
         result.push_back({0, c});
      }
      return result;
   }
   // Keep text before special key
   if (start > 0)
   {
      const auto before = text.substr(0, start);
      for (const auto c : before)
      {
         result.push_back({0, c});
      }
   }
   // Extract the special key
   KeyEventParams params;
   const auto specialKey = text.substr(start + 1, end - start - 1);
   if (specialKey == "Escape")
   {
      params.key = Qt::Key::Key_Escape;
   }
   else if (specialKey == "Return" || specialKey == "Enter")
   {
      params.key = Qt::Key::Key_Return;
   }
   else if (specialKey == "Backspace")
   {
      params.key = Qt::Key::Key_Backspace;
   }
   else if (specialKey == "Delete")
   {
      params.key = Qt::Key::Key_Delete;
   }
   else if (specialKey == "Tab")
   {
      params.key = Qt::Key::Key_Tab;
   }
   else if (specialKey == "Control")
   {
      params.key = Qt::Key::Key_Control;
   }
   else if (specialKey == "Shift")
   {
      params.key = Qt::Key::Key_Shift;
   }
   else if (specialKey == "Alt")
   {
      params.key = Qt::Key::Key_Alt;
   }
   else
   {
      std::cerr << "Special key not supported: <" << specialKey << ">" << std::endl;
      result.push_back({0, '<'});
      for (const auto c : specialKey)
      {
         result.push_back({0, c});
      }
      params.text = '>';
   }

   result.push_back(params);
   const auto remainingText = text.substr(end + 1);
   if (!remainingText.empty())
   {
      const auto remainingKeys = ExtractSpecialKeys(remainingText);
      result.insert(result.end(), remainingKeys.cbegin(), remainingKeys.cend());
   }
   return result;
}

/// Send the given keyboard event to the given receiver.
/// For Qt widgets, the event will be sent to the parent window using
/// Qt event system and QGuiApplication::sendEvent() function.
/// For native widgets, the event will be sent directly to the underlying
/// QObject by calling its event() function.
/// \param widget The receiver widget
/// \param event A keyboard event
/// \return True if the event was handled, False otherwise
bool SendKeyboardEvent(Qat::IWidget& widget, QEvent* event)
{
   if (widget.GetWindow())
   {
      return qApp->sendEvent(widget.GetWindow(), event);
   }
   if (widget.GetQtObject())
   {
      return widget.GetQtObject()->event(event);
   }
   return false;
}
} // anonymous namespace

namespace Qat
{

using namespace Constants;

KeyboardCommandExecutor::KeyboardCommandExecutor(const nlohmann::json& request)
    : BaseCommandExecutor(request)
{
   for (const auto& field : {OBJECT_DEFINITION, ARGUMENTS})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json KeyboardCommandExecutor::Run() const
{
   nlohmann::json result;
   result["status"] = true;
   auto* object = FindObject();
   const auto action = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();

   const auto widget = WidgetWrapper::Cast(object);

   if (!widget)
   {
      throw Exception("Cannot send shortcut event: "
                      "Associated widget was not found");
   }

   if (action == KeyBoard::SHORTCUT)
   {
      TriggerShortcut(object, *widget);
   }
   else
   {
      const auto hasWarning = GenerateKeyEvents(*widget, action);
      if (hasWarning)
      {
         result["warning"] = "No widget accepted this event";
      }
   }

   return result;
}

void KeyboardCommandExecutor::TriggerShortcut(QObject* object, IWidget& widget) const
{
   const auto args = mRequest.at(ARGUMENTS).get<std::string>();
   const auto keySequence = QKeySequence::fromString(QString::fromStdString(args));

   // Custom shortcuts are not handled without parent window being active and parent
   // object having keyboard focus. Since Qat uses simulated events without managing
   // focus, activate shortcuts explicitly when possible.
   nlohmann::json shortcutDefinition;
   shortcutDefinition[OBJECT_TYPE] = "Shortcut";
   shortcutDefinition[CONTAINER] = mRequest.at(OBJECT_DEFINITION);
   const auto shortcutObjects = FindObjects(object, shortcutDefinition);
   for (auto* const shortcutObject : shortcutObjects)
   {
      if (!shortcutObject->property("enabled").toBool())
      {
         continue;
      }
      const auto key = shortcutObject->property("sequence").toString().toStdString();
      if (key == args)
      {
         QMetaObject::invokeMethod(shortcutObject, "activated");
         return;
      }
      const auto multiSeq = shortcutObject->property("sequences").toJsonArray();
      const auto it = std::find_if(
         multiSeq.cbegin(),
         multiSeq.cend(),
         [&args](const auto& element)
         {
            return args == element.toString().toStdString();
         });

      if (it != multiSeq.cend())
      {
         QMetaObject::invokeMethod(shortcutObject, "activated");
         return;
      }
   }
   FocusHolder holdFocus(&widget);
   for (auto i = 0; i < keySequence.count(); ++i)
   {
      const auto keyCombination = SplitKeyCombination(keySequence[i]);

      // Special case: Alt+F4
      if (
         widget.GetWindow() && keyCombination.first == Qt::Key_F4 &&
         keyCombination.second == Qt::KeyboardModifier::AltModifier)
      {
         if (!widget.GetWindow()->close())
         {
            throw Exception("Cannot send Alt+F4 shortcut event: "
                            "Window could not be closed");
         }
         continue;
      }

      QKeyEvent keyPressEvent(
         QEvent::Type::KeyPress,
         keyCombination.first,
         keyCombination.second,
         0,
         0,
         0,
         "",
         false,
         1
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
         ,
         Devices::GetKeyboardDevice()
#endif
      );

      if (!SendKeyboardEvent(widget, &keyPressEvent) || !keyPressEvent.isAccepted())
      {
         throw Exception("Cannot send shortcut event: "
                         "No widget accepted this event");
      }

      QKeyEvent keyReleaseEvent(
         QEvent::Type::KeyRelease,
         keyCombination.first,
         keyCombination.second,
         0,
         0,
         0,
         "",
         false,
         1
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
         ,
         Devices::GetKeyboardDevice()
#endif
      );
      SendKeyboardEvent(widget, &keyReleaseEvent);
   }
}

bool KeyboardCommandExecutor::GenerateKeyEvents(
   IWidget& widget, const std::string& action) const
{
   const auto args = mRequest.at(ARGUMENTS).get<std::string>();
   const auto keys = ExtractSpecialKeys(args);
   FocusHolder holdFocus(&widget);
   bool notAcceptedWarning = false;
   for (const auto& keyParam : keys)
   {
      bool isAccepted = false;
      if (action == KeyBoard::PRESS || action == KeyBoard::TYPE)
      {
         QKeyEvent pressEvent(
            QEvent::Type::KeyPress,
            keyParam.key,
            Qt::KeyboardModifiers(),
            0,
            0,
            0,
            QString(keyParam.text),
            false,
            1
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
            ,
            Devices::GetKeyboardDevice()
#endif
         );
         if (!SendKeyboardEvent(widget, &pressEvent))
         {
            throw Exception("Failed to send keyboard event");
         }
         isAccepted = isAccepted || pressEvent.isAccepted();
         // Some special keys are not accepted by default
         isAccepted = isAccepted || keyParam.key != 0;
      }
      if (action == KeyBoard::RELEASE || action == KeyBoard::TYPE)
      {
         QKeyEvent releaseEvent(
            QEvent::Type::KeyRelease,
            keyParam.key,
            Qt::KeyboardModifiers(),
            0,
            0,
            0,
            QString(keyParam.text),
            false,
            1
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
            ,
            Devices::GetKeyboardDevice()
#endif
         );
         if (!SendKeyboardEvent(widget, &releaseEvent) && action == KeyBoard::RELEASE)
         {
            throw Exception("Failed to send keyboard event");
         }
         isAccepted = isAccepted || releaseEvent.isAccepted();
         // Some special keys are not accepted by default
         isAccepted = isAccepted || keyParam.key != 0;
      }
      if (!isAccepted)
      {
         notAcceptedWarning = true;
      }
   }

   return notAcceptedWarning;
}

} // namespace Qat