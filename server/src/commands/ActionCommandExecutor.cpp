// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/ActionCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/events/ExternalEventsFilter.h>
#include <qat-server/events/NativeEventsFilter.h>
#include <qat-server/events/ObjectPickerFilter.h>
#include <qat-server/plugins/IObjectPicker.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>
#include <qat-server/wrappers/ImageWrapper.h>

#include <QGuiApplication>
#include <QMouseEvent>
#include <QObject>
#include <QPointer>
#include <QWindow>
// Qt private headers are required to force application deactivation
#include <QtGui/qpa/qwindowsysteminterface.h>
#include <QtGui/qpa/qwindowsysteminterface_p.h>

#include <filesystem>
#include <iostream>
#include <string>

namespace
{

/// Pointer to the shared external event filter
QPointer<Qat::ExternalEventsFilter> externalEventFilter{nullptr};

/// Take a screenshot of each main window and save them to the given path.
/// If multiple windows exist, an index will be added to the file name,
/// e.g. screenshot1.png, screenshot2.png, ...
/// \param path Path to the screenshot file.
/// \return True upon success, False in case of error.
bool TakeScreenshot(std::filesystem::path path)
{
   using namespace Qat;
   std::filesystem::create_directories(path.parent_path());
   const auto mainWindows = WidgetWrapper::GetTopWindows();
   bool success = true;
   const auto originalFileName = path.stem();
   int currentIndex = 1;
   for (std::size_t i = 0; i < mainWindows.size(); ++i)
   {
      auto* window = mainWindows[i];
      if (!window)
      {
         continue;
      }

      auto image = WidgetWrapper::GrabImage(window);
      if (!image || image->size().isEmpty())
      {
         continue;
      }
      const auto ext = path.extension();
      if (mainWindows.size() > 1)
      {
         path.replace_filename(originalFileName.string() + std::to_string(currentIndex));
      }
      ++currentIndex;
      path.replace_extension(ext);
      std::cout << "Generating window screenshot to: " << path << std::endl;
      std::filesystem::create_directories(path.parent_path());
      success = success && image->save(QString::fromStdString(path.string()));
   }
   return success;
}

/// Activate or deactivate picker for all visible windows.
/// \param activate True to active the picker, False to deactivate it.
void ActivatePicker(bool activate)
{
   using namespace Qat;
   using namespace Qat::Constants;

   bool pickerFound = false;

   /// Handle CTRL key, allowing to bypass picking
   static auto* pickerFilter = new ObjectPickerFilter(qApp);
   if (activate)
   {
      qApp->installEventFilter(pickerFilter);
   }
   else
   {
      qApp->removeEventFilter(pickerFilter);
   }

   // There is one picker per main window
   for (auto* mainWindow : WidgetWrapper::GetTopWindows())
   {
      IObjectPicker* picker = nullptr;
      QObject* qobject = nullptr;

      // Find existing picker
      qobject = mainWindow->findChild<QObject*>(
         QString::fromStdString(PICKER_NAME), Qt::FindDirectChildrenOnly);

      // qobject_cast does not work with interfaces
      picker = dynamic_cast<IObjectPicker*>(qobject);

      // Create picker if necessary
      if (!picker)
      {
         picker = WidgetWrapper::CreatePicker(mainWindow);
         if (!picker)
         {
            std::cerr << "Could not create picker for window "
                      << mainWindow->objectName().toStdString() << std::endl;
            continue;
         }
         picker->setObjectName(QString::fromStdString(PICKER_NAME));
      }
      pickerFound = true;

      // Reset picker state
      try
      {
         picker->Reset();
      }
      catch (...)
      {
         std::cerr << "Could not reset ObjectPicker (exception)" << std::endl;
      }

      if (activate)
      {
         picker->SetActivated(true);
         if (mainWindow->property("visible").toBool())
         {
            mainWindow->installEventFilter(picker);
         }
         std::cout << "ObjectPicker enabled" << std::endl;
      }
      else
      {
         picker->SetActivated(false);
         mainWindow->removeEventFilter(picker);
         std::cout << "ObjectPicker disabled" << std::endl;
      }
   }

   if (!pickerFound)
   {
      throw Exception(
         "Cannot execute command " + Action::PICKER + ": Object picker was not found");
   }
}

/// Lock or unlock the application's GUI.
/// \param lock True to lock the GUI, False to unlock it.
void LockGui(bool lock)
{
   using namespace Qat;
   if (lock)
   {
      if (!externalEventFilter)
      {
         externalEventFilter = new ExternalEventsFilter(qApp);
      }
      std::cout << "Locking application" << std::endl;
      qApp->installEventFilter(externalEventFilter);
      BaseCommandExecutor::GetNativeEventFilter().Activate(true);
      // Force the application to become inactive to avoid receiving this event later
      // since it affects some other events such as mouse moves or touch drags
      QWindowSystemInterface::handleApplicationStateChanged<
         QWindowSystemInterface::SynchronousDelivery>(Qt::ApplicationInactive, true);
   }
   else
   {
      std::cout << "Unlocking application" << std::endl;
      qApp->removeEventFilter(externalEventFilter);
      BaseCommandExecutor::GetNativeEventFilter().Activate(false);
   }

#if defined(__linux__)
   // Use Window flags to configure Qt's behaviour
   // Note: this only works on linux and is replaced with nativeEventFilter on Windows
   const auto mainWindows = qApp->topLevelWindows();
   for (auto* window : mainWindows)
   {
      window->setFlag(Qt::WindowDoesNotAcceptFocus, lock);
   }
#endif
}

} // anonymous namespace

namespace Qat
{

using namespace Constants;

ActionCommandExecutor::ActionCommandExecutor(const nlohmann::json& request)
    : BaseCommandExecutor(request)
{
   for (const auto& field : {OBJECT_ATTRIBUTE, ARGUMENTS})
   {
      if (!request.contains(field))
      {
         throw Exception(
            "Invalid command: "
            "Missing required field: " +
            field);
      }
   }
}

nlohmann::json ActionCommandExecutor::Run() const
{

   const auto actionName = mRequest.at(OBJECT_ATTRIBUTE).get<std::string>();
   const auto arg = mRequest.at(ARGUMENTS).get<std::string>();
   nlohmann::json result;
   if (actionName == Action::SCREENSHOT)
   {
      std::filesystem::path path(arg);
      if (!path.has_filename())
      {
         path = path / "qat-screenshot.png";
      }

      result["found"] = TakeScreenshot(path);
      return result;
   }
   if (actionName == Action::GRAB)
   {
      auto* object = FindObject();
      const auto widget = WidgetWrapper::Cast(object);
      result["found"] = widget != nullptr;
      if (widget)
      {
         QPointer wrapper = new ImageWrapper();
         result[CACHE_UID] = RegisterObject(wrapper);

         auto callback = [wrapper](const QImage& image)
         {
            if (wrapper)
            {
               wrapper->SetImage(image);
            }
         };
         widget->GrabImage(callback);
      }
      return result;
   }
   if (actionName == Action::PICKER)
   {
      nlohmann::json pickerDefinition;
      pickerDefinition[OBJECT_NAME] = PICKER_NAME;
      if (arg != "enable" && arg != "disable")
      {
         std::string errorMsg{"Cannot execute command "};
         errorMsg.append(actionName);
         errorMsg.append(": Argument ");
         errorMsg.append(arg);
         errorMsg.append(" is not supported");
         throw Exception(errorMsg);
      }

      ActivatePicker(arg == "enable");

      result["found"] = true;
      return result;
   }
   if (actionName == Action::LOCK_UI)
   {
      if (arg != "enable" && arg != "disable")
      {
         std::string errorMsg{"Cannot execute command "};
         errorMsg.append(actionName);
         errorMsg.append(": Argument ");
         errorMsg.append(arg);
         errorMsg.append(" is not supported");
         throw Exception(errorMsg);
      }

      LockGui(arg == "enable");

      result["found"] = true;
      return result;
   }
   throw Exception("Command " + actionName + " not supported");
}

} // namespace Qat