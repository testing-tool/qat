// (c) Copyright 2023, Qat’s Authors

#include <qat-server/commands/BaseCommandExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/events/NativeEventsFilter.h>
#include <qat-server/plugins/INativeInterface.h>
#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/WidgetWrapper.h>
#include <qat-server/qt-tools/ObjectLocator.h>
#include <qat-server/wrappers/ImageWrapper.h>
#include <qat-server/wrappers/MenuWrapper.h>
#include <qat-server/wrappers/ModelIndexWrapper.h>

#include <QGuiApplication>
#include <QObject>
#include <QStyleHints>
#include <QWindow>

#include <iostream>
#include <sstream>
#include <string>

namespace
{
/// Return whether the given definition corresponds to an internal object or not
/// \param[in] definition The object definition
/// \return True if the given definition corresponds to an internal object, False otherwise
bool IsInternalObject(const nlohmann::json& definition)
{
   using namespace Qat::Constants;

   if (definition.contains(OBJECT_TYPE))
   {
      const auto type = definition.at(OBJECT_TYPE).get<std::string>();
      return type == INTERNAL_TYPE;
   }
   return false;
}

/// Return the internal object corresponding to the given definition.
/// \param[in] definition The object definition
/// \return The existing internal object or nullptr if not found
QObject* GetInternalObject(const nlohmann::json& definition)
{
   using namespace Qat::Constants;

   if (definition.contains(OBJECT_ID))
   {
      const auto id = definition.at(OBJECT_ID).get<std::string>();
      if (id == GLOBAL_APP_ID)
      {
         return qGuiApp;
      }
      if (id == NATIVE_IFACE_ID)
      {
         auto* result = Qat::WidgetWrapper::GetNativeInterface();
         if (result)
         {
            result->SetEventFilter(&Qat::BaseCommandExecutor::GetNativeEventFilter());
         }
         return result;
      }
      if (id == STYLE_HINTS_ID)
      {
         return qGuiApp->styleHints();
      }
   }
   return nullptr;
}

/// Return the cached object corresponding to the given definition.
/// \param[in] definition The object definition
/// \return The cached object or nullptr if not found
QObject* GetCachedObject(const nlohmann::json& definition)
{
   using namespace Qat;
   using namespace Qat::Constants;
   const auto uid = definition.at(CACHE_UID).get<BaseCommandExecutor::CacheUidType>();
   auto* object = BaseCommandExecutor::GetRegisteredObject(uid);
   if (object)
   {
      return object;
   }
   if (definition.contains("_temp_image_file"))
   {
      const auto imagePath = definition.at("_temp_image_file").get<std::string>();
      auto* wrapper = new ImageWrapper(imagePath);
      BaseCommandExecutor::RegisterObject(wrapper);
      return wrapper;
   }
   return nullptr;
}

bool FindContainerWindows(
   const nlohmann::json& definition,
   std::vector<QObject*>& containers,
   std::set<QObject*>& windowMatches)
{
   using namespace Qat;
   using namespace Qat::Constants;
   // If no container is given, use all main windows of the application
   const auto mainWindows = WidgetWrapper::GetTopWindows();
   // Verify the windows themselves
   for (auto* window : mainWindows)
   {
      if (ObjectLocator::ObjectMatches(window, definition, nullptr))
      {
         windowMatches.insert(window);
      }
      containers.push_back(window);
   }
   return !windowMatches.empty();
}

/// Return whether the given definition corresponds to a QModelIndex or not
/// \param[in] parent The parent object containing the model index
/// \param[in] definition The object definition
/// \return True if the given definition corresponds to a QModelIndex, False otherwise
bool IsModelIndex(QObject* parent, const nlohmann::json& definition)
{
   return parent && (definition.contains("row") || definition.contains("text")) &&
          (qobject_cast<Qat::ModelIndexWrapper*>(parent) ||
           parent->inherits("QAbstractItemView"));
}

/// Return the model index wrappers matching the given definition
/// \param[in] parent The parent object containing the model index
/// \param[in] definition The object definition
/// \return The model index wrappers (may be empty if not found)
std::vector<std::unique_ptr<Qat::ModelIndexWrapper>>
GetModelIndexes(QObject* parent, const nlohmann::json& definition)
{
   std::vector<std::unique_ptr<Qat::ModelIndexWrapper>> result;
   QAbstractItemModel* model = nullptr;
   QItemSelectionModel* selectionModel = nullptr;
   QModelIndex parentIndex;
   const auto* parentIndexWrapper = qobject_cast<Qat::ModelIndexWrapper*>(parent);
   if (parentIndexWrapper)
   {
      parentIndex = parentIndexWrapper->GetIndex();
      model = parentIndexWrapper->GetModel();
      selectionModel = parentIndexWrapper->GetSelectionModel();
      parent = parentIndexWrapper->GetParentWidget();
   }
   else
   {
      const auto parentWidget = Qat::WidgetWrapper::Cast(parent);
      model = parentWidget->GetModel();
      selectionModel = parentWidget->GetSelectionModel();
   }

   if (!model)
   {
      throw Qat::Exception("No object found that matches this definition: "
                           "Unable to find item model");
   }

   if (definition.contains("row"))
   {
      const auto row = definition.at("row").get<int>();
      auto col = 0;
      if (definition.contains("column"))
      {
         col = definition.at("column").get<int>();
      }
      const auto index = model->index(row, col, parentIndex);
      if (!index.isValid())
      {
         throw Qat::Exception("No object found that matches this definition: "
                              "Index is invalid");
      }
      auto wrapper = std::make_unique<Qat::ModelIndexWrapper>(
         model, selectionModel, index, parent);
      result.emplace_back(std::move(wrapper));
   }
   else if (definition.contains("text"))
   {
      const auto text = definition["text"].get<std::string>();
      QModelIndexList allMatches;
      for (auto col = 0; col < model->columnCount(); ++col)
      {
         const auto startIndex = model->index(0, col, parentIndex);
         const auto matches = model->match(
            startIndex,
            Qt::DisplayRole,
            text.c_str(),
            -1,
            Qt::MatchRecursive | Qt::MatchWildcard);
         allMatches.append(matches);
      }
      for (const auto& index : allMatches)
      {
         auto wrapper = std::make_unique<Qat::ModelIndexWrapper>(
            model, selectionModel, index, parent);
         result.emplace_back(std::move(wrapper));
      }
   }
   return result;
}

std::unique_ptr<Qat::MenuWrapper>
GetMenuItem(QObject* parent, const nlohmann::json& definition)
{
   std::string text;
   if (definition.contains("text"))
   {
      text = definition.at("text").get_to(text);
   }
   if (definition.contains("objectName"))
   {
      text = definition.at("objectName").get_to(text);
   }
   if (text.empty())
   {
      return nullptr;
   }

   return std::make_unique<Qat::MenuWrapper>(parent, text);
}

/// Return whether the given definition corresponds to an item of a QMenu or not
/// \param[in] parent The parent object containing the item
/// \param[in] definition The object definition
/// \return True if the given definition corresponds to an item of a QMenu, False otherwise
bool IsMenuItem(QObject* parent, const nlohmann::json& definition)
{
   if (!parent)
   {
      return false;
   }
   if (!parent->inherits("QMenu") && !parent->inherits("QMenuBar"))
   {
      return false;
   }
   // Do not use wrapper if a type is required
   if (definition.contains(Qat::Constants::OBJECT_TYPE))
   {
      const auto type = definition.at(Qat::Constants::OBJECT_TYPE).get<std::string>();
      // Ignore Menu*Item types for compatibility with QML/QtQuick
      if (type != "MenuItem" && type != "MenuBarItem")
      {
         return false;
      }
   }

   auto wrapper = GetMenuItem(parent, definition);
   if (!wrapper)
   {
      return false;
   }
   return Qat::WidgetWrapper::Cast(wrapper.get()).get();
}
} // namespace

namespace Qat
{

using namespace Constants;

std::map<BaseCommandExecutor::CacheUidType, QObject*> BaseCommandExecutor::mObjectCache;
std::map<QObject*, int> BaseCommandExecutor::mObjectCacheInstances;
std::mutex BaseCommandExecutor::mObjectCacheMutex;

Qat::NativeEventsFilter& BaseCommandExecutor::GetNativeEventFilter()
{
   // Make sure to uninstall the filter before deleting it
   auto deleter = [](NativeEventsFilter* f)
   {
      if (!f)
      {
         return;
      }
      qApp->removeNativeEventFilter(f);
      delete f;
   };
   using DeleterType = std::function<void(NativeEventsFilter*)>;
   static std::unique_ptr<NativeEventsFilter, DeleterType> filter(nullptr, deleter);
   if (!filter)
   {
      // Create and install the filter
      filter = std::unique_ptr<NativeEventsFilter, DeleterType>(
         new NativeEventsFilter(), deleter);
      qApp->installNativeEventFilter(filter.get());
   }
   return *filter;
}

BaseCommandExecutor::BaseCommandExecutor(const nlohmann::json& request)
    : mRequest(request)
{
}

QObject* BaseCommandExecutor::FindObject() const
{
   if (!mRequest.contains(OBJECT_DEFINITION))
   {
      throw Exception(
         "Invalid command: "
         "Missing required field " +
         OBJECT_DEFINITION);
   }
   const auto& objectDefinition = mRequest.at(OBJECT_DEFINITION);
   if (objectDefinition.is_null())
   {
      return nullptr;
   }
   return FindObject(qApp, objectDefinition);
}


std::set<QObject*> BaseCommandExecutor::FindObjects(
   QObject* parent, const nlohmann::json& definition, bool allMatches) const
{
   std::vector<QObject*> containers;
   std::set<QObject*> matches;

   if (IsInternalObject(definition))
   {
      auto* object = GetInternalObject(definition);
      if (!object)
      {
         throw Exception("Could not find internal object " + definition.dump());
      }
      matches.insert(object);
      return matches;
   }

   // Cached object
   if (definition.contains(CACHE_UID))
   {
      auto* object = GetCachedObject(definition);
      if (!object)
      {
         throw Exception(
            "Unable to find object: "
            "May have expired in cache, please provide a full definition again");
      }
      return {object};
   }

   // Search container first (if given)
   if (definition.contains(CONTAINER))
   {
      parent = FindObject(parent, definition.at(CONTAINER));
      if (!parent)
      {
         throw Exception(
            "No object found that matches this definition: "
            "Unable to find container: " +
            definition.at(CONTAINER).dump());
      }
      containers.push_back(parent);
   }
   else if (FindContainerWindows(definition, containers, matches))
   {
      return matches;
   }

   // Model-View framework items
   if (IsModelIndex(parent, definition))
   {
      auto wrappers = GetModelIndexes(parent, definition);
      for (auto& wrapper : wrappers)
      {
         matches.insert(wrapper.get());
         mTemporaryObjects.emplace_back(std::move(wrapper));
      }
      return matches;
   }

   // Menu elements
   if (IsMenuItem(parent, definition))
   {
      auto wrapper = GetMenuItem(parent, definition);
      if (wrapper)
      {
         matches.insert(wrapper.get());
         mTemporaryObjects.emplace_back(std::move(wrapper));
      }
      return matches;
   }

   // Restrict search to direct children of the given parent
   QObject* parentObject = nullptr;
   if (definition.contains(OBJECT_PARENT))
   {
      parentObject = FindObject(parent, definition.at(OBJECT_PARENT));
      if (!parentObject)
      {
         throw Exception(
            "No object found that matches this definition: "
            "Unable to find parent: " +
            definition.at(OBJECT_PARENT).dump());
      }
   }

   // Look for object in all containers
   for (const auto* container : containers)
   {
      const auto objects = ObjectLocator::FindObjects(
         container, definition, parentObject, allMatches);
      matches.insert(objects.cbegin(), objects.cend());
      if (!allMatches && matches.size() > 1)
      {
         break;
      }
   }

   return matches;
}

QObject*
BaseCommandExecutor::FindObject(QObject* parent, const nlohmann::json& definition) const
{
   const auto objects = FindObjects(parent, definition, false);
   if (objects.empty())
   {
      throw Exception(
         "Unable to find object: " + definition.dump() +
         "\nNo object found that matches this definition");
   }
   if (objects.size() > 1)
   {
      throw Exception(
         "Unable to find object: " + definition.dump() +
         "\nMultiple objects found that match this definition");
   }

   return *objects.begin();
}

BaseCommandExecutor::CacheUidType BaseCommandExecutor::RegisterObject(QObject* object)
{
   auto uid = GetObjectCacheUid(object);
   std::lock_guard lock(mObjectCacheMutex);
   if (!mObjectCacheInstances.contains(object))
   {
      mObjectCacheInstances[object] = 0;
   }
   mObjectCache[uid] = object;
   if (object)
   {
      QObject::connect(
         object,
         &QObject::destroyed,
         [uid, object]()
         {
            std::lock_guard lock(mObjectCacheMutex);
            if (mObjectCacheInstances.contains(object))
            {
               mObjectCacheInstances[object]++;
            }
            BaseCommandExecutor::mObjectCache.erase(uid);
         });
   }
   return uid;
}

QObject* BaseCommandExecutor::GetRegisteredObject(const CacheUidType& uid)
{
   std::lock_guard lock(mObjectCacheMutex);
   if (mObjectCache.contains(uid))
   {
      return mObjectCache.at(uid);
   }

   return nullptr;
}

BaseCommandExecutor::CacheUidType BaseCommandExecutor::GetObjectCacheUid(QObject* object)
{
   int instanceNumber = 0;
   {
      std::lock_guard lock(mObjectCacheMutex);
      if (mObjectCacheInstances.contains(object))
      {
         instanceNumber = mObjectCacheInstances[object];
      }
   }
   const auto address = reinterpret_cast<std::uintptr_t>(object);
   std::stringstream uidBuilder;
   uidBuilder << address;
   uidBuilder << instanceNumber;
   return uidBuilder.str();
}

} // namespace Qat