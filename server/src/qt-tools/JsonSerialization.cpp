// (c) Copyright 2024, Qat’s Authors

#include <qat-server/qt-tools/JsonSerialization.h>

#include <qat-server/qt-tools/EnumConversion.h>

#include <QAbstractItemModel>
#include <QBrush>
#include <QByteArray>
#include <QColor>
#include <QFont>
#include <QLine>
#include <QModelIndex>
#include <QPoint>
#include <QQuaternion>
#include <QRect>
#include <QSize>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <nlohmann/json.hpp>

#include <iostream>

void from_json(const nlohmann::json& j, QBrush& brush)
{
   Qt::BrushStyle style = Qt::SolidPattern;
   QColor color;
   if (j.contains("style"))
   {
      const auto brushStyle = static_cast<Qat::BrushStyle>(j["style"].get<int>());
      style = Qat::ToQt(brushStyle);
   }
   if (j.contains("color"))
   {
      j["color"].get_to(color);
   }
   if (j.contains("gradient"))
   {
      const auto gradientType = j["gradient"].get<int>();
      if (static_cast<QGradient::Type>(gradientType) != QGradient::Type::NoGradient)
      {
         std::cout << "Warning: gradient will be ignored when using this QBrush"
                   << std::endl;
      }
   }
   brush = QBrush(color, style);
}

void to_json(nlohmann::json& j, const QBrush& brush)
{
   j["style"] = static_cast<std::underlying_type_t<Qat::BrushStyle>>(
      Qat::FromQt(brush.style()));
   j["color"] = brush.color();
   j["gradient"] = brush.gradient() ? brush.gradient()->type()
                                    : QGradient::Type::NoGradient;
}

void from_json(const nlohmann::json& j, QByteArray& byteArray)
{
   if (j.contains("bytes"))
   {
      for (const auto& value : j["bytes"])
      {
         byteArray.append(static_cast<char>(value.get<int>()));
      }
   }
}

void to_json(nlohmann::json& j, const QByteArray& byteArray)
{
   std::vector<char> array(byteArray.cbegin(), byteArray.cend());
   j["bytes"] = array;
}

void from_json(const nlohmann::json& j, QColor& color)
{
   if (j.contains("name"))
   {
      const auto colorName{j["name"].get<std::string>()};
      color = QColor(QString::fromStdString(colorName));
      return; // 'name' overrides other values
   }
   if (j.contains("red") && j.contains("green") && j.contains("blue"))
   {
      int alpha = std::numeric_limits<uint8_t>::max();
      if (j.contains("alpha"))
      {
         alpha = j["alpha"].get<int>();
      }
      color = QColor(
         j["red"].get<int>(), j["green"].get<int>(), j["blue"].get<int>(), alpha);
   }
}

void to_json(nlohmann::json& j, const QColor& color)
{
   j["name"] = color.name(QColor::HexArgb).toStdString();
   j["red"] = color.red();
   j["green"] = color.green();
   j["blue"] = color.blue();
   j["alpha"] = color.alpha();
}

void from_json(const nlohmann::json& j, QFont& font)
{
   if (j.contains("family"))
   {
      const auto familyName{j["family"].get<std::string>()};
      font.setFamily(QString::fromStdString(familyName));
   }
   if (j.contains("bold"))
   {
      font.setBold(j["bold"].get<bool>());
   }
   if (j.contains("italic"))
   {
      font.setItalic(j["italic"].get<bool>());
   }
   if (j.contains("strikeOut"))
   {
      font.setStrikeOut(j["strikeOut"].get<bool>());
   }
   if (j.contains("underline"))
   {
      font.setUnderline(j["underline"].get<bool>());
   }
   if (j.contains("fixedPitch"))
   {
      font.setFixedPitch(j["fixedPitch"].get<bool>());
   }
   if (j.contains("pixelSize"))
   {
      font.setPixelSize(j["pixelSize"].get<int>());
   }
   if (j.contains("pointSize"))
   {
      font.setPointSize(j["pointSize"].get<int>());
   }
   if (j.contains("weight"))
   {
      const auto weight = static_cast<Qat::FontWeight>(j["weight"].get<int>());
      font.setWeight(Qat::ToQt(weight));
   }
}

void to_json(nlohmann::json& j, const QFont& font)
{
   j["bold"] = font.bold();
   j["family"] = font.family().toStdString();
   j["fixedPitch"] = font.fixedPitch();
   j["italic"] = font.italic();
   j["pixelSize"] = font.pixelSize();
   j["pointSize"] = font.pointSize();
   j["strikeOut"] = font.strikeOut();
   j["underline"] = font.underline();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   const auto weight = static_cast<QFont::Weight>(font.weight());
#else
   const auto weight = font.weight();
#endif
   j["weight"] = static_cast<std::underlying_type_t<Qat::FontWeight>>(
      Qat::FromQt(weight));
}

void from_json(const nlohmann::json& j, QLine& line)
{
   QPoint point1;
   QPoint point2;
   if (j.contains("p1"))
   {
      point1 = j["p1"].get_to(point1);
   }
   if (j.contains("p2"))
   {
      point2 = j["p2"].get_to(point2);
   }
   line = QLine(point1, point2);
}

void to_json(nlohmann::json& j, const QLine& line)
{
   j["p1"] = line.p1();
   j["p2"] = line.p2();
   j["center"] = line.center();
}

void from_json(const nlohmann::json& j, QLineF& line)
{
   QPointF point1;
   QPointF point2;
   if (j.contains("p1"))
   {
      point1 = j["p1"].get_to(point1);
   }
   if (j.contains("p2"))
   {
      point2 = j["p2"].get_to(point2);
   }
   line = QLineF(point1, point2);
}

void to_json(nlohmann::json& j, const QLineF& line)
{
   j["p1"] = line.p1();
   j["p2"] = line.p2();
   j["center"] = line.center();
}

void from_json(const nlohmann::json& j, QModelIndex& index)
{
   if (j.contains("row") && j.contains("column") && j.contains("model"))
   {
      const auto row = j["row"].get<int>();
      const auto column = j["column"].get<int>();
      QModelIndex parentIndex;
      if (j.contains("parentIndex"))
      {
         j["parentIndex"].get_to(parentIndex);
      }
      const auto pointer = std::stoull(j["model"].get<std::string>());
      auto* model = reinterpret_cast<QAbstractItemModel*>(pointer);
      if (model)
      {
         index = model->index(row, column, parentIndex);
      }
   }
}

void to_json(nlohmann::json& j, const QModelIndex& index)
{
   j["row"] = index.row();
   j["column"] = index.column();
   j["model"] = std::to_string(reinterpret_cast<std::uintptr_t>(index.model()));
   const auto parentIndex = index.parent();
   if (parentIndex.isValid())
   {
      j["parentIndex"] = parentIndex;
   }
}

void from_json(const nlohmann::json& j, QPoint& point)
{
   if (j.contains("x"))
   {
      point.setX(j["x"].get<int>());
   }
   if (j.contains("y"))
   {
      point.setY(j["y"].get<int>());
   }
}

void to_json(nlohmann::json& j, const QPoint& point)
{
   j["x"] = point.x();
   j["y"] = point.y();
}

void from_json(const nlohmann::json& j, QPointF& point)
{
   if (j.contains("x"))
   {
      point.setX(j["x"].get<double>());
   }
   if (j.contains("y"))
   {
      point.setY(j["y"].get<double>());
   }
}

void to_json(nlohmann::json& j, const QPointF& point)
{
   j["x"] = point.x();
   j["y"] = point.y();
}

void from_json(const nlohmann::json& j, QQuaternion& quaternion)
{
   if (j.contains("x"))
   {
      quaternion.setX(static_cast<float>(j["x"].get<double>()));
   }
   if (j.contains("y"))
   {
      quaternion.setY(static_cast<float>(j["y"].get<double>()));
   }
   if (j.contains("z"))
   {
      quaternion.setZ(static_cast<float>(j["z"].get<double>()));
   }
   if (j.contains("scalar"))
   {
      quaternion.setScalar(static_cast<float>(j["scalar"].get<double>()));
   }
}

void to_json(nlohmann::json& j, const QQuaternion& quaternion)
{
   j["x"] = quaternion.x();
   j["y"] = quaternion.y();
   j["z"] = quaternion.z();
   j["scalar"] = quaternion.scalar();
}

void from_json(const nlohmann::json& j, QRect& rect)
{
   const auto topLeft = j.get<QPoint>();
   rect.setTopLeft(topLeft);
   const auto size = j.get<QSize>();
   rect.setSize(size);
}

void to_json(nlohmann::json& j, const QRect& rect)
{
   j["x"] = rect.x();
   j["y"] = rect.y();
   j["width"] = rect.width();
   j["height"] = rect.height();
}

void from_json(const nlohmann::json& j, QRectF& rect)
{
   const auto topLeft = j.get<QPointF>();
   rect.setTopLeft(topLeft);
   const auto size = j.get<QSizeF>();
   rect.setSize(size);
}

void to_json(nlohmann::json& j, const QRectF& rect)
{
   j["x"] = rect.x();
   j["y"] = rect.y();
   j["width"] = rect.width();
   j["height"] = rect.height();
}

void from_json(const nlohmann::json& j, QSize& size)
{
   if (j.contains("width"))
   {
      size.setWidth(j["width"].get<int>());
   }
   if (j.contains("height"))
   {
      size.setHeight(j["height"].get<int>());
   }
}

void to_json(nlohmann::json& j, const QSize& size)
{
   j["width"] = size.width();
   j["height"] = size.height();
}

void from_json(const nlohmann::json& j, QSizeF& size)
{
   if (j.contains("width"))
   {
      size.setWidth(j["width"].get<double>());
   }
   if (j.contains("height"))
   {
      size.setHeight(j["height"].get<double>());
   }
}

void to_json(nlohmann::json& j, const QSizeF& size)
{
   j["width"] = size.width();
   j["height"] = size.height();
}

void from_json(const nlohmann::json& j, QVector2D& vector)
{
   if (j.contains("x"))
   {
      vector.setX(static_cast<float>(j["x"].get<double>()));
   }
   if (j.contains("y"))
   {
      vector.setY(static_cast<float>(j["y"].get<double>()));
   }
}

void to_json(nlohmann::json& j, const QVector2D& vector)
{
   j["x"] = vector.x();
   j["y"] = vector.y();
}

void from_json(const nlohmann::json& j, QVector3D& vector)
{
   if (j.contains("x"))
   {
      vector.setX(static_cast<float>(j["x"].get<double>()));
   }
   if (j.contains("y"))
   {
      vector.setY(static_cast<float>(j["y"].get<double>()));
   }
   if (j.contains("z"))
   {
      vector.setZ(static_cast<float>(j["z"].get<double>()));
   }
}

void to_json(nlohmann::json& j, const QVector3D& vector)
{
   j["x"] = vector.x();
   j["y"] = vector.y();
   j["z"] = vector.z();
}

void from_json(const nlohmann::json& j, QVector4D& vector)
{
   if (j.contains("x"))
   {
      vector.setX(static_cast<float>(j["x"].get<double>()));
   }
   if (j.contains("y"))
   {
      vector.setY(static_cast<float>(j["y"].get<double>()));
   }
   if (j.contains("z"))
   {
      vector.setZ(static_cast<float>(j["z"].get<double>()));
   }
   if (j.contains("w"))
   {
      vector.setW(static_cast<float>(j["w"].get<double>()));
   }
}

void to_json(nlohmann::json& j, const QVector4D& vector)
{
   j["x"] = vector.x();
   j["y"] = vector.y();
   j["z"] = vector.z();
   j["w"] = vector.w();
}