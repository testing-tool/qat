// (c) Copyright 2023, Qat’s Authors

#include <qat-server/qt-tools/QVariantToJson.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/commands/FindCommandExecutor.h>
#include <qat-server/qt-tools/EnumConversion.h>
#include <qat-server/qt-tools/JsonSerialization.h>

#include <QAbstractItemModel>
#include <QBrush>
#include <QColor>
#include <QFont>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QLine>
#include <QModelIndex>
#include <QObject>
#include <QQuaternion>
#include <QRect>
#include <QVariant>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <iostream>
#include <limits>

namespace
{
/// Return the type name of the given type ID
/// \param[in] type A type ID
/// \return the corresponding type name
const auto* GetTypeName(int type)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return QMetaType::typeName(type);
#else
   return QMetaType(type).name();
#endif
}

/// Return the type ID of the given type name
/// \param[in] name A type name
/// \return the corresponding type ID
int GetTypeId(const QByteArray& name)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return QMetaType::type(name);
#else
   return QMetaType::fromName(name).id();
#endif
}
} // namespace

namespace Qat
{

int GetVariantType(const QVariant& variant)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   return variant.type();
#else
   return variant.metaType().id();
#endif
}

nlohmann::json ToJson(const QVariant& variant)
{
   nlohmann::json result;
   const auto variantType = GetVariantType(variant);

   switch (variantType)
   {
      case QMetaType::Type::QColor:
      {
         result = variant.value<QColor>();
         break;
      }
      case QMetaType::Type::QBrush:
      {
         result = variant.value<QBrush>();
         break;
      }
      case QMetaType::Type::QFont:
      {
         result = variant.value<QFont>();
         break;
      }
      case QMetaType::Type::QPoint:
      {
         result = variant.value<QPoint>();
         break;
      }
      case QMetaType::Type::QPointF:
      {
         result = variant.value<QPointF>();
         break;
      }
      case QMetaType::Type::QLine:
      {
         result = variant.value<QLine>();
         break;
      }
      case QMetaType::Type::QLineF:
      {
         result = variant.value<QLineF>();
         break;
      }
      case QMetaType::Type::QSize:
      {
         result = variant.value<QSize>();
         break;
      }
      case QMetaType::Type::QSizeF:
      {
         result = variant.value<QSizeF>();
         break;
      }
      case QMetaType::Type::QRect:
      {
         result = variant.value<QRect>();
         break;
      }
      case QMetaType::Type::QRectF:
      {
         result = variant.value<QRectF>();
         break;
      }
      case QMetaType::Type::QQuaternion:
      {
         result = variant.value<QQuaternion>();
         break;
      }
      case QMetaType::Type::QVector2D:
      {
         result = variant.value<QVector2D>();
         break;
      }
      case QMetaType::Type::QVector3D:
      {
         result = variant.value<QVector3D>();
         break;
      }
      case QMetaType::Type::QVector4D:
      {
         result = variant.value<QVector4D>();
         break;
      }
      case QMetaType::Type::QByteArray:
      {
         result = variant.value<QByteArray>();
         break;
      }
      case QMetaType::Type::QModelIndex:
      {
         result = variant.value<QModelIndex>();
         break;
      }
      case QMetaType::Type::QVariant:
      {
         // Return without adding custom type fields
         return ToJson(variant.value<QVariant>());
      }
      case QMetaType::Type::QVariantList:
      {
         result = nlohmann::json();
         const auto list = variant.value<QVariantList>();
         for (const auto& element : list)
         {
            result.push_back(ToJson(element));
         }
         // Return without adding custom type fields
         return result;
      }
      default:
      {
         const auto jsonValue = QJsonValue::fromVariant(variant);
         QJsonObject jsonObject;
         jsonObject.insert("temp", jsonValue);
         QJsonDocument doc(jsonObject);
         const auto serialized = doc.toJson().toStdString();
         const auto value = nlohmann::json::parse(serialized);
         return value["temp"];
      }
   }

   result["QVariantType"] = variantType;
   const auto* typeName = GetTypeName(variantType);
   if (typeName)
   {
      result["QVariantTypeName"] = typeName;
   }

   return result;
}


QVariant FromJson(const QJsonValue& json)
{
   if (json.isObject())
   {
      try
      {
         QJsonObject jsonObject;
         jsonObject.insert(Constants::OBJECT_DEFINITION.c_str(), json);
         QJsonDocument doc(jsonObject);
         const auto definition = nlohmann::json::parse(doc.toJson().toStdString());
         FindCommandExecutor executor(definition);
         auto* obj = executor.FindObject();
         return QVariant::fromValue(obj);
      }
      catch (const Exception&) // NOLINT(bugprone-empty-catch)
      {
         // May be a custom object
      }
   }
   const auto jsonObject = json.toObject();
   int variantType = QMetaType::Type::UnknownType;
   if (jsonObject.contains("QVariantType"))
   {
      variantType = jsonObject.value("QVariantType").toInt();
   }
   else if (jsonObject.contains("QVariantTypeName"))
   {
      variantType = GetTypeId(jsonObject.value("QVariantTypeName").toString().toUtf8());
   }
   else
   {
      return json.toVariant();
   }

   // Convert json content from Qt type to nlohmann type.
   QJsonDocument doc(jsonObject);
   const auto jsonContent = nlohmann::json::parse(doc.toJson().toStdString());

   switch (variantType)
   {
      case QMetaType::Type::QColor:
      {
         return jsonContent.get<QColor>();
      }
      case QMetaType::Type::QBrush:
      {
         return jsonContent.get<QBrush>();
      }
      case QMetaType::Type::QFont:
      {
         return jsonContent.get<QFont>();
      }
      case QMetaType::Type::QPoint:
      {
         return jsonContent.get<QPoint>();
      }
      case QMetaType::Type::QPointF:
      {
         return jsonContent.get<QPointF>();
      }
      case QMetaType::QLine:
      {
         return jsonContent.get<QLine>();
      }
      case QMetaType::QLineF:
      {
         return jsonContent.get<QLineF>();
      }
      case QMetaType::Type::QSize:
      {
         return jsonContent.get<QSize>();
      }
      case QMetaType::Type::QSizeF:
      {
         return jsonContent.get<QSizeF>();
      }
      case QMetaType::Type::QRect:
      {
         return jsonContent.get<QRect>();
      }
      case QMetaType::Type::QRectF:
      {
         return jsonContent.get<QRectF>();
      }
      case QMetaType::Type::QQuaternion:
      {
         return jsonContent.get<QQuaternion>();
      }
      case QMetaType::Type::QVector2D:
      {
         return jsonContent.get<QVector2D>();
      }
      case QMetaType::Type::QVector3D:
      {
         return jsonContent.get<QVector3D>();
      }
      case QMetaType::Type::QVector4D:
      {
         return jsonContent.get<QVector4D>();
      }
      case QMetaType::Type::QByteArray:
      {
         return jsonContent.get<QByteArray>();
      }
      case QMetaType::Type::QModelIndex:
      {
         const auto index = jsonContent.get<QModelIndex>();
         if (index.isValid())
         {
            return index;
         }
         break;
      }
      default:
         return {};
   }

   return {};
}

} // namespace Qat