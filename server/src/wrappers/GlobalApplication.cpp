// (c) Copyright 2024, Qat’s Authors

#include <qat-server/wrappers/GlobalApplication.h>

#include <qat-server/Constants.h>

#include <QGuiApplication>
#include <QPainter>
#include <QPixmap>
#include <QRegion>
#include <QScreen>

#include <iostream>

namespace Qat
{

QObject* GlobalApplication::GetQtObject() const
{
   return qGuiApp;
}

std::string GlobalApplication::GetId() const
{
   return Constants::GLOBAL_APP_ID;
}

QObject* GlobalApplication::GetParent() const
{
   return nullptr;
}

std::vector<QObject*> GlobalApplication::GetChildWidgets() const
{
   return {};
}

QAbstractItemModel* GlobalApplication::GetModel() const
{
   return nullptr;
}

QItemSelectionModel* GlobalApplication::GetSelectionModel() const
{
   return nullptr;
}

QWindow* GlobalApplication::GetWindow() const
{
   return nullptr;
}

QPointF GlobalApplication::MapToGlobal(const QPointF& point) const
{
   return point;
}

QPointF GlobalApplication::MapFromGlobal(const QPointF& point) const
{
   return point;
}

QPointF GlobalApplication::MapToScene(const QPointF& point) const
{
   return point;
}

QPointF GlobalApplication::MapToWidget(const IWidget* widget, const QPointF& point) const
{
   if (!widget)
   {
      std::cerr << "Cannot map coordinates: widget is null" << std::endl;
      return point;
   }
   return widget->MapFromGlobal(point.toPoint());
}

bool GlobalApplication::Contains(const QPointF& point) const
{
   return GetBounds().contains(point.toPoint());
}

QSizeF GlobalApplication::GetSize() const
{
   return GetBounds().size();
}

qreal GlobalApplication::GetWidth() const
{
   return GetSize().width();
}

qreal GlobalApplication::GetHeight() const
{
   return GetSize().height();
}

QRect GlobalApplication::GetBounds() const
{
   QRegion virtualGeometry;
   for (auto* const screen : QGuiApplication::screens())
   {
      virtualGeometry += screen->geometry();
   }
   return virtualGeometry.boundingRect();
}

float GlobalApplication::GetPixelRatio() const
{
   return 1.0F;
}

qreal GlobalApplication::GetZ() const
{
   return 0;
}

bool GlobalApplication::IsVisible() const
{
   return true;
}

void GlobalApplication::ForceActiveFocus([[maybe_unused]] Qt::FocusReason reason) const {}

void GlobalApplication::ReleaseActiveFocus() const {}

void GlobalApplication::GrabImage(std::function<void(const QImage&)> callback) const
{
   int totalWidth = 0;
   int totalHeight = 0;
   for (auto* const screen : QGuiApplication::screens())
   {
      totalWidth = std::max(totalWidth, screen->size().width());
      totalHeight += screen->size().height();
   }

   QPixmap image(totalWidth, totalHeight);
   QPainter painter(&image);
   int currentYPosition = 0;
   for (auto* const screen : QGuiApplication::screens())
   {
      painter.drawPixmap(QPoint(0, currentYPosition), screen->grabWindow(0));
      currentYPosition += screen->size().height();
   }

   callback(image.toImage());
}
} // namespace Qat