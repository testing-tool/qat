// (c) Copyright 2023, Qat’s Authors

#include <qat-server/wrappers/ImageWrapper.h>

#include <qat-server/Exception.h>

#include <QImage>

#include <filesystem>
#include <string>
#include <thread>

std::deque<Qat::ImageWrapper*> Qat::ImageWrapper::mCache;
std::mutex Qat::ImageWrapper::mMutex;

namespace
{
/// Hold the maximum number of wrappers to be stored
constexpr int CACHE_SIZE = 10;
/// Image quality when calling save()
constexpr int IMAGE_QUALITY = 100;
} // namespace

namespace Qat
{

ImageWrapper::ImageWrapper()
{
   std::lock_guard lock(mMutex);
   mCache.push_back(this);
   if (mCache.size() > CACHE_SIZE)
   {
      mCache.front()->deleteLater();
      mCache.pop_front();
   }
}

ImageWrapper::ImageWrapper(const QImage& image)
    : mImage(image)
{
   std::lock_guard lock(mMutex);
   mCache.push_back(this);
   if (mCache.size() > CACHE_SIZE)
   {
      mCache.front()->deleteLater();
      mCache.pop_front();
   }
}


ImageWrapper::ImageWrapper(const std::string& path)
{
   std::lock_guard lock(mMutex);
   mHasImage = mImage.load(QString::fromStdString(path));

   mCache.push_back(this);
   if (mCache.size() > CACHE_SIZE)
   {
      mCache.front()->deleteLater();
      mCache.pop_front();
   }
}

void ImageWrapper::ClearCache()
{
   std::lock_guard lock(mMutex);
   if (!mCache.empty())
   {
      mCache.front()->deleteLater();
      mCache.pop_front();
   }
}

void ImageWrapper::SetImage(const QImage& image)
{
   mImage = image.copy();
   mHasImage = true;
}

int ImageWrapper::GetWidth() const
{
   if (!mHasImage)
   {
      return -1;
   }
   return mImage.width();
}

int ImageWrapper::GetHeight() const
{
   if (!mHasImage)
   {
      return -1;
   }
   return mImage.height();
}

unsigned int ImageWrapper::getPixel(int x, int y) const
{
   if (!mHasImage)
   {
      return {};
   }
   return mImage.pixel(x, y);
}

QColor ImageWrapper::getPixelRGBA(int x, int y) const
{
   if (!mHasImage)
   {
      return {};
   }
   const auto pixel = getPixel(x, y);
   return QColor(qRed(pixel), qGreen(pixel), qBlue(pixel), qAlpha(pixel));
}

void ImageWrapper::save(const QString& fileName)
{
   if (!mHasImage)
   {
      throw Exception("Cannot save image: "
                      "Current image is not available");
   }

   std::filesystem::path destinationFile = fileName.toStdString();
   std::filesystem::create_directories(destinationFile.parent_path());
   if (!std::filesystem::exists(destinationFile.parent_path()))
   {
      throw Exception("Cannot save image: "
                      "Destination folder does not exist");
   }

   if (!mImage.save(fileName, nullptr, IMAGE_QUALITY))
   {
      throw Exception("Cannot save image: "
                      "Internal error: QImage::save() returned false");
   }
   // Make sure the QImage data is synchronized with the file.
   // This allows the equals() function to work properly when comparing in-memory images
   // to reloaded ones.
   constexpr auto loopPeriod = std::chrono::duration<int, std::milli>(100);
   while (!std::filesystem::exists(fileName.toStdString()))
   {
      std::this_thread::sleep_for(loopPeriod);
   }
   if (!mImage.load(fileName))
   {
      throw Exception("Cannot reload image: "
                      "Internal error: QImage::load() returned false");
   }
}

bool ImageWrapper::equals(const ImageWrapper* other) const
{
   if (!other)
   {
      return false;
   }
   return mImage == other->mImage;
}

} // namespace Qat