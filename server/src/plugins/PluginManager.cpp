// (c) Copyright 2023, Qat’s Authors

#include <qat-server/library-tools/LibPath.h>
#include <qat-server/library-tools/Platform.h>
#include <qat-server/plugins/Plugin.h>
#include <qat-server/plugins/PluginManager.h>

#include <qat-server/Exception.h>

#include <QtGlobal>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#if defined(FOR_WINDOWS)
   #include <windows.h>
#elif defined(FOR_LINUX)
   #include <dlfcn.h>
   #include <link.h>
#elif defined(FOR_MACOS)
   #include <dlfcn.h>
#endif

namespace Qat
{

const PluginManager& PluginManager::GetInstance()
{
   static auto globalManager = PluginManager("plugins");
   return globalManager;
}

PluginManager::PluginManager(const std::filesystem::path& root)
{
   const auto currentPath = GetLibraryPath();
   mPluginRoot = currentPath.parent_path() / root;

   // Get current Qt version
   const std::string qtVersion = QT_VERSION_STR;

   // Get plugin library suffix based on Qt version
   std::stringstream versionStream(qtVersion);
   std::string token;
   std::vector<std::string> elements;
   while (std::getline(versionStream, token, '.'))
   {
      elements.push_back(token);
   }
   if (elements.size() < 2)
   {
      std::cerr << "Could not get Qt version elements" << std::endl;
      return;
   }
   const auto librarySuffix = elements[0] + "." + elements[1] + LIB_EXTENSION;
   LoadPlugins(librarySuffix);
}

const std::map<std::string, std::unique_ptr<Plugin>>& PluginManager::GetPlugins() const
{
   return mPlugins;
}

void PluginManager::LoadPlugins(const std::string& librarySuffix)
{
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   for (const auto& file : std::filesystem::directory_iterator(mPluginRoot))
   {
      const auto& filePath = file.path();
      if (filePath.filename().string().ends_with(librarySuffix))
      {
         auto* const handle = dlopen(filePath.string().c_str(), RTLD_LAZY);
         if (!handle)
         {
            std::cerr << "Failed to load plugin: " << filePath.string() << std::endl;
            std::cerr << dlerror() << std::endl;
            continue;
         }
         std::cout << "Successfully loaded plugin: " << filePath.string() << std::endl;

         // Strip "lib" prefix
         const auto pluginName = filePath.stem().string().substr(3);
         mPlugins[pluginName] = std::make_unique<Qat::Plugin>(handle);
      }
   }
#elif defined(FOR_WINDOWS)
   for (const auto& file : std::filesystem::directory_iterator(mPluginRoot))
   {
      const auto& filePath = file.path();
      if (filePath.filename().string().ends_with(librarySuffix))
      {
         const auto handle = LoadLibraryA(filePath.string().c_str());
         if (!handle)
         {
            std::cerr << "Failed to load plugin: " << filePath.string() << std::endl;
            continue;
         }
         else
         {
            std::cout << "Successfully loaded plugin: " << filePath.string() << std::endl;
         }

         const auto pluginName = filePath.stem().string();
         mPlugins[pluginName] = std::make_unique<Qat::Plugin>(handle);
      }
   }
#endif
}
} // namespace Qat