// (c) Copyright 2023, Qat’s Authors

#include <qat-server/plugins/Plugin.h>

#include <qat-server/Exception.h>
#include <qat-server/library-tools/Platform.h>
#include <qat-server/plugins/IWidget.h>

#include <iostream>
#include <string>
#include <vector>

#if defined(FOR_WINDOWS)
   #include <windows.h>
#elif defined(FOR_LINUX)
   #include <dlfcn.h>
   #include <link.h>
#elif defined(FOR_MACOS)
   #define FOR_MACOS
   #include <dlfcn.h>
#endif

namespace Qat
{
Plugin::Plugin(LibHandle handle)
    : mHandle(handle)
{
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   *(void**) (&mCastFunction) = dlsym(mHandle, "CastObject");
   *(void**) (&mGetTopWindowsFunction) = dlsym(mHandle, "GetTopWindows");
   *(void**) (&mGrabFunction) = dlsym(mHandle, "GrabImage");
   *(void**) (&mCreatePickerFunction) = dlsym(mHandle, "CreatePicker");
   *(void**) (&mGetNativeInterfaceFunction) = dlsym(mHandle, "GetNativeInterface");
#elif defined(FOR_WINDOWS)
   mCastFunction = (CastFunctionType) GetProcAddress(mHandle, "CastObject");
   mGetTopWindowsFunction = (GetTopWindowsFunctionType) GetProcAddress(
      mHandle, "GetTopWindows");
   mGrabFunction = (GrabFunctionType) GetProcAddress(mHandle, "GrabImage");
   mCreatePickerFunction = (CreatePickerFunctionType) GetProcAddress(
      mHandle, "CreatePicker");
   mGetNativeInterfaceFunction = (GetNativeInterfaceFunctionType) GetProcAddress(
      mHandle, "GetNativeInterface");
#endif
   if (!mCastFunction)
   {
      std::cerr << "Could not find Cast function" << std::endl;
   }
   if (!mGetTopWindowsFunction)
   {
      std::cerr << "Could not find GetTopWindows function" << std::endl;
   }
   if (!mGrabFunction)
   {
      std::cerr << "Could not find GrabImage function" << std::endl;
   }
   if (!mCreatePickerFunction)
   {
      std::cerr << "Could not find CreatePicker function" << std::endl;
   }
   // mGetNativeInterfaceFunction is optional
}

std::unique_ptr<IWidget> Plugin::CastObject(const QObject* qobject) const
{
   if (!mCastFunction)
   {
      return nullptr;
   }
   std::unique_ptr<IWidget> result;
   result.reset(mCastFunction(qobject));
   return result;
}

bool Plugin::GetTopWindows(QObject** windowList, unsigned int* count) const
{
   if (!mGetTopWindowsFunction)
   {
      return false;
   }
   return mGetTopWindowsFunction(windowList, count);
}

std::unique_ptr<QImage> Plugin::GrabImage(QObject* window) const
{
   if (!mGrabFunction)
   {
      return nullptr;
   }

   std::unique_ptr<QImage> result;
   result.reset(mGrabFunction(window));
   return result;
}

IObjectPicker* Plugin::CreatePicker(QObject* window) const
{
   if (!mCreatePickerFunction)
   {
      return nullptr;
   }
   return mCreatePickerFunction(window);
}

INativeInterface* Plugin::GetNativeInterface() const
{
   if (!mGetNativeInterfaceFunction)
   {
      return nullptr;
   }
   return mGetNativeInterfaceFunction();
}

} // namespace Qat