// (c) Copyright 2023, Qat’s Authors

#include <qat-server/plugins/WidgetWrapper.h>

#include <qat-server/plugins/IWidget.h>
#include <qat-server/plugins/Plugin.h>
#include <qat-server/plugins/PluginManager.h>
#include <qat-server/wrappers/GlobalApplication.h>

#include <QGuiApplication>
#include <QObject>
#include <QVariant>
#include <QWindow>

#include <algorithm>
#include <memory>
#include <set>
#include <vector>

namespace Qat
{

std::unique_ptr<IWidget> WidgetWrapper::Cast(const QObject* qobject)
{
   if (qobject == qGuiApp)
   {
      return std::make_unique<GlobalApplication>();
   }
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto result = it.second->CastObject(qobject);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}

std::vector<QObject*> WidgetWrapper::GetTopWindows()
{
   std::set<QObject*> windows;
   std::vector<QObject*> result;
   const auto& pluginMgr = PluginManager::GetInstance();
   // All plugins can provide top windows
   for (const auto& it : pluginMgr.GetPlugins())
   {
      unsigned int count = 0;
      std::vector<QObject*> windowList;
      if (!it.second->GetTopWindows(nullptr, &count) || count == 0)
      {
         continue;
      }
      windowList.reserve(count);
      windowList.resize(count);
      if (!it.second->GetTopWindows(windowList.data(), &count))
      {
         continue;
      }

      // Ignore null items that may happen due to the delay between the 2 calls to GetTopWindows()
      // (a window may have been closed between calls).
      const auto last = std::remove_if(
         windowList.begin(),
         windowList.end(),
         [](const auto& window)
         {
            return !window;
         });
      windows.insert(windowList.begin(), last);
   }
   // Split windows into Qt vs native
   std::map<QString, std::pair<QObject*, bool>> qtWindows;
   std::map<QString, std::pair<QObject*, bool>> nativeWindows;
   for (const auto& object : windows)
   {
      const auto topWidget = WidgetWrapper::Cast(object);
      if (!topWidget)
      {
         continue;
      }
      const auto hasChild = !object->children().empty();
      const auto* const w = topWidget->GetWindow();
      if (w)
      {
         const auto handleStr =
            QString("0x") +
            QString("%1").arg(w->winId(), QT_POINTER_SIZE, 16, QChar('0')).toUpper();
         qtWindows[handleStr] = std::make_pair(object, hasChild);
      }
      else
      {
         const auto handle = object->property("handle");
         if (handle.isValid())
         {
            nativeWindows[handle.toString()] = std::make_pair(object, hasChild);
         }
         else
         {
            // Qt window without actual QWindow: accept it
            result.push_back(object);
         }
      }
   }

   // Filter native windows having a Qt counterpart
   for (const auto& [handle, value] : nativeWindows)
   {
      const auto it = qtWindows.find(handle);
      // Either there is no Qt window or both Qt and native windows have children
      if ((it == qtWindows.cend()) || (value.second && it->second.second))
      {
         result.push_back(value.first);
      }
      // Qt window has children: keep it and ignore native one
      else if (it->second.second)
      {
         continue;
      }
      // Qt window has no child: keep native one only
      else
      {
         result.push_back(value.first);
         qtWindows.erase(it);
      }
   }
   for (const auto& [handle, value] : qtWindows)
   {
      result.push_back(value.first);
   }

   return result;
}

std::unique_ptr<QImage> WidgetWrapper::GrabImage(QObject* window)
{
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto result = it.second->GrabImage(window);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}


IObjectPicker* WidgetWrapper::CreatePicker(QObject* window)
{
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto* result = it.second->CreatePicker(window);
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}

INativeInterface* WidgetWrapper::GetNativeInterface()
{
   const auto& pluginMgr = PluginManager::GetInstance();
   for (const auto& it : pluginMgr.GetPlugins())
   {
      auto* result = it.second->GetNativeInterface();
      if (result)
      {
         return result;
      }
   }
   return nullptr;
}

} // namespace Qat