// (c) Copyright 2025, Qat’s Authors

#include <qat-server/TcpServerLib.h>

#include <qat-server/Exception.h>
#include <qat-server/library-tools/LibPath.h>

#include <filesystem>
#include <iostream>
#include <string>

#if defined(FOR_WINDOWS)
   #include <windows.h>
#elif defined(FOR_LINUX)
   #include <dlfcn.h>
   #include <link.h>
#elif defined(FOR_MACOS)
   #include <dlfcn.h>
#endif

namespace
{
#if defined(FOR_WINDOWS)
const std::string TCP_SERVER_LIB_NAME{"tcp_server.dll"};
#elif defined(FOR_LINUX)
const std::string TCP_SERVER_LIB_NAME{"libtcp_server.so"};
#elif defined(FOR_MACOS)
const std::string TCP_SERVER_LIB_NAME{"libtcp_server.dylib"};
#endif

Qat::TcpServerLib::LibHandle LoadTcpLibrary()
{
   const auto tcpLibPath = Qat::GetLibraryPath().parent_path() / "tcp_server" /
                           TCP_SERVER_LIB_NAME;
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   auto* const handle = dlopen(tcpLibPath.string().c_str(), RTLD_LAZY);
   if (!handle)
   {
      std::cerr << "Failed to load TCP server library: " << tcpLibPath.string()
                << std::endl;
      std::cerr << dlerror() << std::endl;
      throw Qat::Exception("Failed to load TCP server library");
   }
   std::cout << "Successfully loaded TCP server library: " << tcpLibPath.string()
             << std::endl;
   return handle;

#elif defined(FOR_WINDOWS)

   const auto handle = LoadLibraryA(tcpLibPath.string().c_str());
   if (!handle)
   {
      std::cerr << "Failed to load TCP server library: " << tcpLibPath.string()
                << std::endl;
      throw Qat::Exception("Failed to load TCP server library");
   }
   std::cout << "Successfully loaded TCP server library: " << tcpLibPath.string()
             << std::endl;
   return handle;

#endif
}
} // namespace

namespace Qat
{

TcpServerLib::TcpServerLib()
    : mHandle{LoadTcpLibrary()}
{
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   *(void**) (&mStartServer) = dlsym(mHandle, "start_server");
   *(void**) (&mStopServer) = dlsym(mHandle, "stop_server");
   *(void**) (&mSendResponse) = dlsym(mHandle, "send_response");
   *(void**) (&mInitComm) = dlsym(mHandle, "init_comm");
   *(void**) (&mSendMessage) = dlsym(mHandle, "send_message");
   *(void**) (&mCloseComm) = dlsym(mHandle, "close_comm");
#elif defined(FOR_WINDOWS)
   mStartServer = (StartServerFunctionType) GetProcAddress(mHandle, "start_server");
   mStopServer = (StopServerFunctionType) GetProcAddress(mHandle, "stop_server");
   mSendResponse = (SendResponseFunctionType) GetProcAddress(mHandle, "send_response");
   mInitComm = (InitCommFunctionType) GetProcAddress(mHandle, "init_comm");
   mSendMessage = (SendMessageFunctionType) GetProcAddress(mHandle, "send_message");
   mCloseComm = (CloseCommFunctionType) GetProcAddress(mHandle, "close_comm");
#endif
   if (
      !mStartServer || !mStopServer || !mSendResponse || !mInitComm || !mSendMessage ||
      !mCloseComm)
   {
      throw Exception("Could not load TCP server library (missing function)");
   }
}

uint16_t TcpServerLib::StartServer(RequestCallback requestCb) const
{
   return mStartServer(requestCb);
}

bool TcpServerLib::StopServer() const
{
   return mStopServer();
}

bool TcpServerLib::SendResponse(ClientIdType clientId, const char* response) const
{
   return mSendResponse(clientId, response);
}

bool TcpServerLib::InitComm(ClientIdType clientId, const char* url) const
{
   return mInitComm(clientId, url);
}

bool TcpServerLib::SendMessage(ClientIdType clientId, const char* message) const
{
   return mSendMessage(clientId, message);
}

bool TcpServerLib::CloseComm(ClientIdType clientId) const
{
   return mCloseComm(clientId);
}

} // namespace Qat