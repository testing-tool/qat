// (c) Copyright 2024, Qat’s Authors

#include <qat-server/events/NativeEventsFilter.h>

#include <QByteArray>
#include <QObject>
#include <QTimer>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define IS_WINDOWS
#endif

#if defined(IS_WINDOWS)
   #include <Windows.h>
#endif
#include <iostream>

namespace
{

#if defined(IS_WINDOWS)
/// Detect input messages from the platform
/// \note This function is a modified version of the one found in
/// Qt's source code: qtbase/src/plugins/platforms/windows/qwindowscontext.cpp
static inline bool isInputMessage(UINT m)
{
   switch (m)
   {
         //   case WM_IME_STARTCOMPOSITION:
         //   case WM_IME_ENDCOMPOSITION:
         //   case WM_IME_COMPOSITION:
         //case WM_NCACTIVATE: // Required to receive Key events
      case WM_ACTIVATE:
      case WM_SETFOCUS:
      case WM_KILLFOCUS:
      case WM_ACTIVATEAPP:
      case WM_INPUT:
      case WM_TOUCH:
      case WM_GESTURE:
      case WM_GESTURENOTIFY:
      case WM_MOUSEHOVER:
      case WM_MOUSELEAVE:
      case WM_NCMOUSEHOVER:
      case WM_NCMOUSELEAVE:
         //   case WM_SIZING:
         //   case WM_MOVING:
         //   case WM_SYSCOMMAND:
         //   case WM_COMMAND:
         //   case WM_DWMNCRENDERINGCHANGED:
         //   case WM_PAINT:
         return true;
      default:
         break;
   }
   return (m >= WM_MOUSEFIRST && m <= WM_MOUSELAST) ||
          (m >= WM_NCMOUSEMOVE && m <= WM_NCXBUTTONDBLCLK) ||
          (m >= WM_KEYFIRST && m <= WM_KEYLAST) ||
          (m >= WM_NCPOINTERUPDATE && m <= WM_POINTERROUTEDRELEASED);
}
#endif

} // anonymous namespace

namespace Qat
{

NativeEventsFilter::NativeEventsFilter()
{
   mKeyEventsTimer = std::make_unique<QTimer>();
   mKeyEventsTimer->setSingleShot(true);
   QObject::connect(
      mKeyEventsTimer.get(),
      &QTimer::timeout,
      [this]()
      {
         std::unique_lock lock(mLock);
         if (mExpectedKeyEvents > 0)
         {
            std::cout << "Warning: application missed some native keyboard events"
                      << std::endl;
         }
         mExpectedKeyEvents = 0;
      });
}

void NativeEventsFilter::Activate(bool activate)
{
   mIsActive = activate;
}

void NativeEventsFilter::ExpectKeyEvents(int nbEvents, std::chrono::milliseconds timeout)
{
   std::unique_lock lock(mLock);
   mExpectedKeyEvents += nbEvents;
   if (mExpectedKeyEvents < 0)
   {
      mExpectedKeyEvents = 0;
   }
   if (mKeyEventsTimer)
   {
      mKeyEventsTimer->start(timeout);
   }
}

bool NativeEventsFilter::nativeEventFilter(
   [[maybe_unused]] const QByteArray& eventType,
   [[maybe_unused]] void* message,
   [[maybe_unused]] ResultType* result)
{
#if defined(IS_WINDOWS)
   if (message && eventType == "windows_generic_MSG")
   {
      MSG* msg = static_cast<MSG*>(message);
      auto m = msg->message;
      if (m >= WM_KEYFIRST && m <= WM_KEYLAST)
      {
         std::unique_lock lock(mLock);
         if (mExpectedKeyEvents > 0)
         {
            mExpectedKeyEvents--;
            return false;
         }
      }
      return mIsActive && isInputMessage(msg->message);
   }
#endif

   return false;
}

} // namespace Qat