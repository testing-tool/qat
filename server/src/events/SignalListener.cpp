// (c) Copyright 2023, Qat’s Authors

#include <qat-server/events/SignalListener.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/RequestHandler.h>
#include <qat-server/commands/BaseCommandExecutor.h>
#include <qat-server/qt-tools/QVariantToJson.h>

#include <nlohmann/json.hpp>

#include <QCoreApplication>
#include <QEvent>
#include <QMetaObject>
#include <QMetaType>
#include <QScreen>
#include <QThread>
#include <QVariant>

#include <QtCore/private/qobject_p.h> // For QMetaCallEvent class

#include <iostream>
#include <sstream>

// Some types are not registered in Qt5, preventing them from being used in QVariant
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
Q_DECLARE_METATYPE(QScreen*)
namespace
{
int unused_id1 = qRegisterMetaType<QVector<int>>();
int unused_id2 = qRegisterMetaType<QScreen*>();
} // namespace
#endif

namespace Qat
{

SignalListener::SignalListener(
   RequestHandler* requestHandler, TcpServerLib::ClientIdType clientId, int numArgs)
    : QObject(requestHandler)
    , mRequestHandler(requestHandler)
    , mClientId(clientId)
    , mNumArgs{numArgs}
{
   mId = std::to_string(reinterpret_cast<std::uintptr_t>(this));
}

SignalListener* SignalListener::Create(
   RequestHandler* requestHandler, TcpServerLib::ClientIdType clientId, int numArgs)
{
   if (numArgs == 0)
   {
      std::cerr << "Useless threaded signal listener created: signals without arg can be "
                   "connected in main thread"
                << std::endl;
   }
   auto* listener = new SignalListener(requestHandler, clientId, numArgs);
   listener->setParent(nullptr);
   auto& thread = SignalListener::GetThread();
   listener->moveToThread(&thread);
   return listener;
}

void SignalListener::AttachTo(QObject* object, std::string property)
{
   mObject = object;
   mPropertyName = std::move(property);
}

QThread& SignalListener::GetThread()
{
   static QThread thread;
   if (!thread.isRunning())
   {
      thread.start();
      QObject::connect(qApp, &QCoreApplication::aboutToQuit, &thread, &QThread::quit);
   }
   return thread;
}

bool SignalListener::event(QEvent* ev)
{
   if (ev && ev->type() == QEvent::MetaCall)
   {
      auto* metaCallEvent = static_cast<QMetaCallEvent*>(ev);
      auto* args = metaCallEvent->args();
      auto* argTypes = metaCallEvent->types();
      QVariantList argList;
      for (auto i = 0; i < mNumArgs; ++i)
      {
         // +1 because first element is the return type
         auto type = argTypes[i + 1];
         QVariant arg(type, args[i + 1]);

// Handle special cases such as QList<>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
         if (type >= QMetaType::Type::User)
#else
         if (type.id() >= QMetaType::Type::User)
#endif
         {
            if (arg.canConvert<QVariantList>())
            {
               QVariant converted = arg.value<QVariantList>();
               argList << converted;
               continue;
            }
         }
         argList << arg;
      }
      Notify(argList);
      return true;
   }
   return QObject::event(ev);
}

void SignalListener::Notify() const
{
   nlohmann::json message;
   nlohmann::json arguments;

   if (mObject)
   {
      const auto value = mObject->property(mPropertyName.c_str());
      if (value.canConvert<QObject*>())
      {
         nlohmann::json definition;
         auto* childObject = value.value<QObject*>();
         if (!childObject)
         {
            definition["object"] = nullptr;
         }
         nlohmann::json objectJson;
         objectJson[Constants::CACHE_UID] = BaseCommandExecutor::RegisterObject(
            childObject);
         definition["object"] = objectJson;
         arguments.push_back(definition);
      }
      else
      {
         nlohmann::json jsonValue;
         jsonValue["value"] = ToJson(value);
         arguments.push_back(jsonValue);
      }
      message[Constants::ARGUMENTS] = arguments;
   }

   message[Constants::OBJECT_ID] = mId;
   mRequestHandler->SendMessage(mClientId, message.dump());
}

void SignalListener::Notify(const QVariantList& args) const
{
   nlohmann::json message;
   nlohmann::json arguments;

   for (const auto& arg : args)
   {
      if (arg.canConvert<QObject*>())
      {
         nlohmann::json definition;
         auto* childObject = arg.value<QObject*>();
         if (!childObject)
         {
            definition["object"] = nullptr;
         }
         nlohmann::json objectJson;
         objectJson[Constants::CACHE_UID] = BaseCommandExecutor::RegisterObject(
            childObject);
         definition["object"] = objectJson;
         arguments.push_back(definition);
      }
      else
      {
         nlohmann::json jsonValue;
         jsonValue["value"] = ToJson(arg);
         arguments.push_back(jsonValue);
      }
   }
   message[Constants::ARGUMENTS] = arguments;
   message[Constants::OBJECT_ID] = mId;
   QString qMessage = QString::fromStdString(message.dump());
   QMetaObject::invokeMethod(
      mRequestHandler,
      "SendMessage",
      Qt::QueuedConnection,
      Q_ARG(quint16, mClientId),
      Q_ARG(QString, qMessage));
}

std::string SignalListener::GetId() const noexcept
{
   return mId;
}

} // namespace Qat