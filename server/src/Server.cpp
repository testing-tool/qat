// (c) Copyright 2023, Qat’s Authors

#include <qat-server/Server.h>

#include <qat-server/RequestHandler.h>
#include <qat-server/TcpServerLib.h>
#include <qat-server/events/DoubleTapEventsFilter.h>
#include <qat-server/wrappers/ImageWrapper.h>

#include <QCoreApplication>
#include <QMetaObject>
#include <QTimer>

#include <iostream>

namespace
{
Qat::DoubleTapEventsFilter* doubleTapFilter{nullptr};

Qat::RequestHandler* requestHandler{nullptr};

extern "C" void
HandleRequest(Qat::TcpServerLib::ClientIdType clientId, const char* request)
{
   if (!requestHandler)
   {
      std::cerr << "Cannot handle TCP request: handler is null" << std::endl;
      return;
   }

   const auto requestStr = QString::fromUtf8(request);
   QString result;

   const auto rc = QMetaObject::invokeMethod(
      requestHandler,
      "Execute",
      Qt::QueuedConnection,
      Q_ARG(QString, requestStr),
      Q_ARG(quint16, clientId));
   if (!rc)
   {
      std::cerr << "Cannot handle TCP request: handler could not be called" << std::endl;
      return;
   }
}
} // namespace

namespace Qat
{

void Server::Create(std::function<void(const Server*)> onCreated)
{
   auto* server = new Server();

   QObject::connect(
      server,
      &Server::IsRunning,
      [server, callback = std::move(onCreated)]()
      {
         callback(server);
      });
   QTimer::singleShot(0, server, SLOT(Start()));
}

Server::Server()
{
   // QTimer must run in a Qt thread
   moveToThread(qApp->thread());
}

Server::~Server()
{
   Stop();
}

void Server::Start()
{
   setParent(qApp);
   mServer = std::make_shared<TcpServerLib>();
   requestHandler = new RequestHandler(this, mServer);
   mPort = mServer->StartServer(HandleRequest);
   doubleTapFilter = new DoubleTapEventsFilter(qApp);
   qApp->installEventFilter(doubleTapFilter);

   emit IsRunning();
}

void Server::Stop()
{
   std::cout << "Closing server..." << std::endl;
   ImageWrapper::ClearCache();
   if (doubleTapFilter)
   {
      qApp->removeEventFilter(doubleTapFilter);
      doubleTapFilter->deleteLater();
   }
   mServer->StopServer();
}

int Server::GetPort() const noexcept
{
   return mPort;
}

} // namespace Qat