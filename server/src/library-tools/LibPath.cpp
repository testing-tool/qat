// (c) Copyright 2025, Qat’s Authors
#include <qat-server/Exception.h>
#include <qat-server/library-tools/LibPath.h>

#if defined(FOR_WINDOWS)
   #include <windows.h>
#elif defined(FOR_LINUX)
   #include <dlfcn.h>
   #include <link.h>
#elif defined(FOR_MACOS)
   #include <dlfcn.h>
#endif

/// This function is defined only to provide a symbolic name in the current library.
/// Its address is used with dladdr1 to retrieve the absolute path of this library.
void ResolvePath() {}

namespace Qat
{
/// Get the path to the currently loaded library
/// \return the path to the currently loaded library
std::filesystem::path GetLibraryPath()
{
#ifdef FOR_LINUX
   Dl_info dlinfo;
   struct link_map* result = nullptr;
   if (!dladdr1((void*) &ResolvePath, &dlinfo, (void**) &result, RTLD_DI_LINKMAP))
   {
      throw Exception("dladdr1 failed: Unable to load plugins");
   }
   if (!result)
   {
      throw Exception("dladdr1 returned NULL pointer: Unable to load plugins");
   }

   return result->l_name;
#elif defined(FOR_WINDOWS)
   constexpr int MAX_PATH_LEN{4096};
   char libPathData[MAX_PATH_LEN];
   const auto libHandle = GetModuleHandleA("injector.dll");
   const auto rc = GetModuleFileNameA(libHandle, libPathData, MAX_PATH_LEN);
   if (!rc)
   {
      std::stringstream message;
      message << "Could not retrieve library path (error #" << GetLastError() << ")";
      throw Exception(message.str());
   }
   return libPathData;
#elif defined(FOR_MACOS)
   Dl_info dlinfo;
   if (!dladdr((void*) &ResolvePath, &dlinfo))
   {
      throw Exception("dladdr1 failed: Unable to load plugins");
   }
   return dlinfo.dli_fname;
#endif
}

} // namespace Qat