// (c) Copyright 2023, Qat’s Authors

#include <qat-server/RequestExecutor.h>

#include <qat-server/Constants.h>
#include <qat-server/Exception.h>
#include <qat-server/RequestHandler.h>
#include <qat-server/commands/ActionCommandExecutor.h>
#include <qat-server/commands/CallCommandExecutor.h>
#include <qat-server/commands/CommCommandExecutor.h>
#include <qat-server/commands/FindCommandExecutor.h>
#include <qat-server/commands/GestureCommandExecutor.h>
#include <qat-server/commands/GetCommandExecutor.h>
#include <qat-server/commands/KeyboardCommandExecutor.h>
#include <qat-server/commands/ListCommandExecutor.h>
#include <qat-server/commands/MouseCommandExecutor.h>
#include <qat-server/commands/SetCommandExecutor.h>
#include <qat-server/commands/TouchCommandExecutor.h>

#include <memory>
#include <string>

namespace Qat
{

using namespace Constants;

RequestExecutor::RequestExecutor(
   const nlohmann::json& request,
   RequestHandler* requestHandler,
   TcpServerLib::ClientIdType clientId)
    : mRequest(request)
    , mRequestHandler(requestHandler)
    , mClientId(clientId)
{
   if (!request.contains(COMMAND_TYPE))
   {
      throw Exception(
         "Invalid command: "
         "Missing required field: " +
         COMMAND_TYPE);
   }
}

nlohmann::json RequestExecutor::Run() const
{
   const auto command = mRequest.at(COMMAND_TYPE).get<std::string>();
   std::unique_ptr<ICommandExecutor> commandExecutor;

   if (command == Command::FIND)
   {
      commandExecutor = std::make_unique<FindCommandExecutor>(mRequest);
   }
   else if (command == Command::LIST)
   {
      commandExecutor = std::make_unique<ListCommandExecutor>(mRequest);
   }
   else if (command == Command::GET)
   {
      commandExecutor = std::make_unique<GetCommandExecutor>(mRequest);
   }
   else if (command == Command::SET)
   {
      commandExecutor = std::make_unique<SetCommandExecutor>(mRequest);
   }
   else if (command == Command::CALL)
   {
      commandExecutor = std::make_unique<CallCommandExecutor>(mRequest);
   }
   else if (command == Command::MOUSE)
   {
      commandExecutor = std::make_unique<MouseCommandExecutor>(mRequest);
   }
   else if (command == Command::KEYBOARD)
   {
      commandExecutor = std::make_unique<KeyboardCommandExecutor>(mRequest);
   }
   else if (command == Command::ACTION)
   {
      commandExecutor = std::make_unique<ActionCommandExecutor>(mRequest);
   }
   else if (command == Command::COMMUNICATION)
   {
      commandExecutor = std::make_unique<CommCommandExecutor>(
         mRequest, mRequestHandler, mClientId);
   }
   else if (command == Command::GESTURE)
   {
      commandExecutor = std::make_unique<GestureCommandExecutor>(mRequest);
   }
   else if (command == Command::TOUCH)
   {
      commandExecutor = std::make_unique<TouchCommandExecutor>(mRequest);
   }
   else
   {
      throw Exception(
         "Invalid command: "
         "Unsupported command type: " +
         command);
   }
   return commandExecutor->Run();
}

} // namespace Qat