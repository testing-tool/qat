// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/commands/BaseCommandExecutor.h>

#include <nlohmann/json.hpp>

class QObject;

namespace Qat
{

class IWidget;

/// \brief Class responsible to execute keyboard commands.
/// \note Custom keyboard shortcuts are partially implemented
class KeyboardCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit KeyboardCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~KeyboardCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Trigger the shortcut of this request on the given widget.
   /// \param object Target Qt object.
   /// \param widget Widget wrapper for the given object.
   void TriggerShortcut(QObject* object, IWidget& widget) const;

   /// Generate and send keyboard events to the given object.
   /// \param widget Widget wrapper for the given object.
   /// \param action Type of keyboard event (press, release).
   /// \return True is some events were not accepted (warning), False otherwise.
   bool GenerateKeyEvents(IWidget& widget, const std::string& action) const;
};

} // namespace Qat