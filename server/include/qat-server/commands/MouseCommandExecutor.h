// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/commands/BaseCommandExecutor.h>

#include <nlohmann/json.hpp>

#include <QMouseEvent>
#include <QPoint>

namespace Qat
{

class IWidget;

/// \brief Class responsible to execute mouse action commands.
/// It handles various mouse events :
///   - left/right/middle click
///   - double click
///   - mouse button press/release
///   - scrolling
///   - drag/move
class MouseCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit MouseCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~MouseCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Structure holding mouse event parameters
   struct MouseParameters
   {
      Qt::MouseButton button;
      Qt::KeyboardModifiers modifiers;
      QPoint localCoordinates;
      QPoint globalCoordinates;
      QPoint moveCoordinates;
   };

   /// Build event parameters from given arguments.
   /// \param args JSON arguments for the current command.
   /// \param widget Target widget.
   /// \param object Original object from command.
   /// \param action Current mouse action (click, move, release, etc)
   /// \return Event parameters.
   static MouseParameters BuildParameters(
      const nlohmann::json& args,
      IWidget& widget,
      QObject* object,
      const std::string& action);

   /// Send a mouse press event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \return True if the event was accepted, False otherwise.
   static bool SendPressEvent(const MouseParameters& parameters, IWidget& widget);

   /// Send a mouse double-click event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \return True if the event was accepted, False otherwise.
   static bool SendDoubleClickEvent(const MouseParameters& parameters, IWidget& widget);

   /// Send mouse move events to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \return True if the event was accepted, False otherwise.
   static bool SendMoveEvent(const MouseParameters& parameters, IWidget& widget);

   /// Send mouse drag events to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \return True if the event was accepted, False otherwise.
   static bool SendDragEvent(const MouseParameters& parameters, IWidget& widget);

   /// Send a scroll (wheel) press event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \param object Original object from command.
   /// \return True if the event was accepted, False otherwise.
   static bool
   SendScrollEvent(const MouseParameters& parameters, IWidget& widget, QObject* object);

   /// Send a mouse release event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   /// \return True if the event was accepted, False otherwise.
   static bool SendReleaseEvent(
      const MouseParameters& parameters, IWidget& widget, const std::string& action);
};

} // namespace Qat