// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Interface for the command executor
class ICommandExecutor
{
public:
   /// Default constructor
   ICommandExecutor() = default;

   /// Destructor
   virtual ~ICommandExecutor() = default;

   /// Run the command and return the result
   /// \return The result in JSON format
   virtual nlohmann::json Run() const = 0;
};

} // namespace Qat