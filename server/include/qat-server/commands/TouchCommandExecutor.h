// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/commands/BaseCommandExecutor.h>

#include <nlohmann/json.hpp>

#include <QMouseEvent>
#include <QPoint>

namespace Qat
{

class IWidget;

/// \brief Class responsible to execute touch commands.
/// It handles Touch, Release, Drag, Move actions
class TouchCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit TouchCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~TouchCommandExecutor() override = default;

   /// \brief ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Structure holding touch event parameters
   struct TouchParameters
   {
      Qt::KeyboardModifiers modifiers;
      std::vector<QPoint> localCoordinates;
      std::vector<QPoint> sceneCoordinates;
      std::vector<QPoint> globalCoordinates;
      std::vector<QPoint> moveCoordinates;
   };

   /// Build event parameters from given arguments.
   /// \param args JSON arguments for the current command.
   /// \param widget Target widget.
   /// \return Event parameters.
   static TouchParameters BuildParameters(const nlohmann::json& args, IWidget& widget);

   /// Send a touch press event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   static void SendPressEvent(const TouchParameters& parameters, IWidget& widget);

   /// Send touch move events to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   static void SendMoveEvent(const TouchParameters& parameters, IWidget& widget);

   /// Send touch drag events to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   static void SendDragEvent(const TouchParameters& parameters, IWidget& widget);

   /// Send a touch release event to the given widget.
   /// \param parameters Parameters of the event.
   /// \param widget Target widget.
   static void SendReleaseEvent(const TouchParameters& parameters, IWidget& widget);
};

} // namespace Qat