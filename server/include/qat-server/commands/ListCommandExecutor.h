// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/commands/BaseCommandExecutor.h>
#include <nlohmann/json.hpp>

namespace Qat
{
/// \brief Class responsible to execute listing commands
///  It can list multiple types of elements:
///   - properties
///   - top-level windows
///   - objects matching a definition
///   - the Qt version used
class ListCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit ListCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~ListCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Serialize the properties of the given object.
   /// \param object A Qt object.
   /// \return Serialized property list.
   static nlohmann::json ListProperties(QObject* object);

   /// Serialize the methods of the given object.
   /// \param object A Qt object.
   /// \return Serialized method list.
   static nlohmann::json ListMethods(QObject* object);
};

} // namespace Qat