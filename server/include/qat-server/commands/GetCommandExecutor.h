// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/commands/BaseCommandExecutor.h>

#include <nlohmann/json.hpp>

#include <optional>

class QObject;

namespace Qat
{
/// \brief Class responsible to execute commands getting a Qt object
class GetCommandExecutor : public BaseCommandExecutor
{
public:
   /// Constructor
   /// \param[in] request The request in JSON format
   explicit GetCommandExecutor(const nlohmann::json& request);

   /// Destructor
   ~GetCommandExecutor() override = default;

   /// \copydoc ICommandExecutor::Run
   nlohmann::json Run() const override;

private:
   /// Find and serialize all children of the given object.
   /// \param object The parent Qt object.
   /// \return Serialized list of children.
   static nlohmann::json GetObjectChildren(QObject* object);

   /// Find and serialize the parent of the given object.
   /// \param object A Qt object.
   /// \return Serialized parent object or nullopt if not found.
   static std::optional<nlohmann::json> GetObjectParent(QObject* object);

   /// Find and serialize the ItemViewModel instance associated to the given object.
   /// \param object A Qt object inheriting the QAbstractItemView class.
   /// \param name Type of the model (data model or selection model).
   /// \return Serialized model or nullopt if not found.
   static std::optional<nlohmann::json>
   GetItemViewModel(QObject* object, const std::string& name);

   /// Find and serialize a Qt property or Qt method of the given object.
   /// \param object A Qt object.
   /// \param name Name of a property or method.
   /// \param result Json instance to store the serialized property.
   static void
   GetQtProperty(QObject* object, const std::string& name, nlohmann::json& result);
};

} // namespace Qat