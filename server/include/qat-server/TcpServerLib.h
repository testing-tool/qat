// (c) Copyright 2025, Qat’s Authors
#pragma once

#include <qat-server/library-tools/Platform.h>

#include <cstdint>

#if defined(FOR_WINDOWS)
   #include "windows.h"
#endif

namespace Qat
{

class TcpServerLib final
{
public:
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   using LibHandle = void*;
#elif defined(FOR_WINDOWS)
   using LibHandle = HMODULE;
#endif
   using ClientIdType = uint16_t;

   /// C function called when a request is received
   /// \param[in] clientId Unique identifier of the client sending the request.
   /// \param[in] request Content of the request as a UTF8 string. Should be in JSON format.
   using RequestCallback = void (*)(ClientIdType clientId, const char* request);

   /// Default constructor. Load the exported tcp_server library.
   /// \warn Will throw if the tcp_server library cannot be loaded.
   TcpServerLib();

   /// Destructor
   ~TcpServerLib() = default;

   /// Start a new TCP server.
   /// This function has no effect if a server is already running.
   /// \param[in] request_cb C function called for each received request.
   /// \return The server port upon success, 0 otherwise.
   uint16_t StartServer(RequestCallback requestCb) const;

   /// Stop the current TCP server.
   /// This function has no effect if no server is running.
   /// Return True upon success, False otherwise.
   bool StopServer() const;

   /// Send the given response to a connected client.
   /// \param[in] clientId Unique identifier of the destination client (as received by the callback).
   /// \param[in] response Content of the response as a UTF8 string. Should be in JSON format.
   /// \return True upon success, False otherwise.
   bool SendResponse(ClientIdType clientId, const char* response) const;

   /// Initialize communication from server to client.
   /// \param[in] clientId Unique identifier of the client.
   /// \param[in] url URL of the server on the client's side. E.g. "localhost:1234";
   /// \return True upon success, False otherwise.
   bool InitComm(ClientIdType clientId, const char* url) const;

   /// Send the given message to a connected client.
   /// \note The InitComm() must be called before sending messages.
   /// \param[in] clientId Unique identifier of the client.
   /// \param[in] message Message to be sent, usually in JSON format.
   /// Return True upon success, False otherwise.
   bool SendMessage(ClientIdType clientId, const char* message) const;

   /// Close communication from server to client.
   /// \note This will make subsequent calls to  SendMessage() fail.
   /// \param[in] clientId Unique identifier of the client.
   /// \return True upon success, False otherwise.
   bool CloseComm(ClientIdType clientId) const;

private:
#if defined(FOR_WINDOWS)
   typedef uint16_t(CALLBACK* StartServerFunctionType)(RequestCallback);
   typedef bool(CALLBACK* StopServerFunctionType)();
   typedef bool(CALLBACK* SendResponseFunctionType)(ClientIdType, const char*);
   typedef bool(CALLBACK* InitCommFunctionType)(ClientIdType, const char*);
   typedef bool(CALLBACK* SendMessageFunctionType)(ClientIdType, const char*);
   typedef bool(CALLBACK* CloseCommFunctionType)(ClientIdType);
#endif

   /// Opaque pointer to the tcp_server library
   const LibHandle mHandle;

#if defined(FOR_LINUX) || defined(FOR_MACOS)
   /// Pointer to the start_server function
   uint16_t (*mStartServer)(RequestCallback);

   /// Pointer to the stop_server function
   bool (*mStopServer)();

   /// Pointer to the send_response function
   bool (*mSendResponse)(ClientIdType, const char*);

   /// Pointer to the init_comm function
   bool (*mInitComm)(ClientIdType, const char*);

   /// Pointer to the send_message function
   bool (*mSendMessage)(ClientIdType, const char*);

   /// Pointer to the close_comm function
   bool (*mCloseComm)(ClientIdType);

#elif defined(FOR_WINDOWS)
   /// Pointer to the start_server function
   StartServerFunctionType mStartServer;

   /// Pointer to the stop_server function
   StopServerFunctionType mStopServer;

   /// Pointer to the send_response function
   SendResponseFunctionType mSendResponse;

   /// Pointer to the init_comm function
   InitCommFunctionType mInitComm;

   /// Pointer to the send_message function
   SendMessageFunctionType mSendMessage;

   /// Pointer to the close_comm function
   CloseCommFunctionType mCloseComm;
#endif
};

} // namespace Qat
