// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/TcpServerLib.h>

#include <QObject>
#include <QPointer>

#include <memory>

namespace Qat
{
/// \brief Class responsible for Sending/Reading requests on a TCP socket to a remote host
class RequestHandler : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(RequestHandler)

public:
   /// Constructor
   /// \param[in] parent The Qt parent (for memory management)
   /// \param[in] tcpServer The underlying TCP server
   RequestHandler(QObject* parent, std::shared_ptr<TcpServerLib> tcpServer);

   /// Destructor
   ~RequestHandler() override = default;

   /// Open a socket and connect it to the given host
   /// \param[in] hostName The host name to connect to
   /// \param[in] port The socket port to use
   /// \param[in] clientId ID of the client to connect to
   void ConnectToHost(
      const std::string& hostName, int port, TcpServerLib::ClientIdType clientId) const;

   /// Disconnect the socket from the host.
   /// Intended to be used by the Python API to close the communication during shutdown.
   /// \param[in] clientId ID of the client to disconnect from
   void DisconnectFromHost(TcpServerLib::ClientIdType clientId) const;

   /// Send the given message to the current host
   /// \note Will throw if ConnectToHost() was never called
   /// \param[in] clientId ID of the client to send the message to
   /// \param[in] message The message to send
   void
   SendMessage(TcpServerLib::ClientIdType clientId, const std::string& message) const;

   /// Send the given message to the current host.
   /// This method is intended to be called via the Qt MetaObject system
   /// \param[in] clientId ID of the client to send the message to
   /// \param message The message to send
   Q_INVOKABLE void SendMessage(quint16 clientId, const QString& message) const;

public slots:
   /// Execute the given request and return the response.
   /// \param[in] request a JSON-encoded request.
   void Execute(const QString& request, quint16 clientId) noexcept;

private:
   /// Pointer to the TCP server
   std::shared_ptr<TcpServerLib> mServer{nullptr};
};

} // namespace Qat