// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QAbstractNativeEventFilter>

#include <QTimer>
#include <QtGlobal>

#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>

namespace Qat
{

/// \brief Class responsible to filter out native events.
/// It ignores all external events received from the OS / platform.
/// This allows to "lock" the application GUI and prevents user activity from
/// affecting the tests.
class NativeEventsFilter : public QAbstractNativeEventFilter
{

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
   using ResultType = long;
#else
   using ResultType = qintptr;
#endif

public:
   /// Default constructor
   NativeEventsFilter();

   /// Activate or de-activate this filter.
   /// \param[in] activate True to activate this filter, False to de-activate it.
   void Activate(bool activate);

   /// \copydoc QAbstractNativeEventFilter::nativeEventFilter
   bool nativeEventFilter(
      const QByteArray& eventType, void* message, ResultType* result) override;

   /// Increment expected number of key events.
   /// \param nbEvents Number of key events to expect.
   /// \param timeout Duration to wait for events.
   void ExpectKeyEvents(int nbEvents, std::chrono::milliseconds timeout);

private:
   /// State of this filter
   bool mIsActive{false};

   /// Lock protecting expected KeyEvents variables
   std::mutex mLock;
   /// Number of expected key events
   std::atomic<int> mExpectedKeyEvents;
   /// Timer resetting expected key events after some delay
   std::unique_ptr<QTimer> mKeyEventsTimer{nullptr};
};

} // namespace Qat