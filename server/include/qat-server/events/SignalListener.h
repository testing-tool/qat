// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/TcpServerLib.h>

#include <QObject>
#include <QPointer>
#include <QVariant>

#include <string>

class QThread;

namespace Qat
{
/// Forward declarations
class RequestHandler;

/// \brief Class responsible for propagating signals from Qt/C++ to the Python client
class SignalListener : public QObject
{
   Q_OBJECT

public:
   /// Constructor
   /// \param[in] requestHandler The request handler for the current client.
   /// \param[in] clientId ID of the associated client.
   /// \param[in] numArgs Number of arguments of the associated signal. Unused for properties.
   explicit SignalListener(
      RequestHandler* requestHandler,
      TcpServerLib::ClientIdType clientId,
      int numArgs = 0);

   /// Factory creating listeners in the dedicated thread to handle MetaCall events.
   /// \param[in] requestHandler The request handler for the current client.
   /// \param[in] clientId ID of the associated client.
   /// \param[in] numArgs Number of arguments of the associated signal.
   /// \return The newly created listener
   static SignalListener* Create(
      RequestHandler* requestHandler, TcpServerLib::ClientIdType clientId, int numArgs);

   /// Destructor
   ~SignalListener() override = default;

   /// Attach the connected object and property.
   /// Not used when connecting signals.
   /// \param[in] object Associated object when connecting properties
   /// \param[in] property Connected property
   void AttachTo(QObject* object, std::string property);

   /// Handle MetaCall events to handle argument propagation.
   /// \param ev A Qt event
   /// \return True if event was handled, False otherwise
   bool event(QEvent* ev) override;

   /// Notify observer that the value changed
   Q_INVOKABLE void Notify() const;

   /// Return the unique ID of this listener
   /// \return the unique ID of this listener
   std::string GetId() const noexcept;

private:
   /// Notify observer that the signal has been emitted
   /// \param args Arguments sent by the signal
   void Notify(const QVariantList& args) const;

   /// Return the thread managing the event loop handling MetaCall events.
   /// \note The thread is initialized and started if necessary.
   /// \return The current running thread.
   static QThread& GetThread();

   /// Pointer to the parent request handler for the current client
   QPointer<RequestHandler> mRequestHandler;

   /// ID of the listening client
   TcpServerLib::ClientIdType mClientId;

   /// ID identifying the remote callback
   std::string mId;

   /// Number of arguments of the associated signal
   int mNumArgs{0};

   /// Associated object when connecting properties (null when using signals)
   QPointer<QObject> mObject{nullptr};

   /// Connected property (empty when using signals)
   std::string mPropertyName;
};

} // namespace Qat