// (c) Copyright 2025, Qat’s Authors
#pragma once

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
   #define FOR_WINDOWS
#elif defined(__linux__)
   #define FOR_LINUX
#elif defined(__APPLE__)
   #define FOR_MACOS
#else
   #error "Unsupported platform"
#endif