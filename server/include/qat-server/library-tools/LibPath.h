// (c) Copyright 2025, Qat’s Authors
#pragma once

#include <qat-server/library-tools/Platform.h>

#include <filesystem>
#include <string>

namespace Qat
{
#if defined(FOR_WINDOWS)
const std::string LIB_EXTENSION{".dll"};
#elif defined(FOR_LINUX)
const std::string LIB_EXTENSION{".so"};
#elif defined(FOR_MACOS)
const std::string LIB_EXTENSION{".dylib"};
#endif

/// Get the path to the currently loaded library
/// \return the path to the currently loaded library
std::filesystem::path GetLibraryPath();

} // namespace Qat