// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QPoint>
#include <tuple>

/// Forward declarations
class QObject;

namespace Qat::WidgetLocator
{

/// Return the local and global coordinates of the center of the given widget
/// \param[in] object A widget
/// \return the center of the given widget
std::tuple<QPoint, QPoint> GetWidgetCenter(QObject* object);

/// Find the child widget of the given object at the given coordinates
/// \param[in] parent The parent widget
/// \param[in] coordinates The local coordinates (widget CS)
/// \return The child widget
QObject* FindWidget(QObject* parent, QPoint coordinates);

/// Find the child object at the given coordinates and return this object along
/// with it local and global coordinates.
/// \param[in] parent The parent widget
/// \param[in] coordinates The local coordinates (widget CS)
/// \return A tuple of the child widget, local, global coordinates
std::tuple<QObject*, QPoint, QPoint> FindWidgetAt(QObject* parent, QPoint coordinates);

} // namespace Qat::WidgetLocator