// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <nlohmann/json.hpp>

class QBrush;
class QByteArray;
class QColor;
class QFont;
class QLine;
class QLineF;
class QModelIndex;
class QPoint;
class QPointF;
class QQuaternion;
class QRect;
class QRectF;
class QSize;
class QSizeF;
class QVector2D;
class QVector3D;
class QVector4D;

// Function names are defined by the nlohmann library.
// NOLINTBEGIN(readability-identifier-naming)
void from_json(const nlohmann::json& j, QBrush& brush);
void to_json(nlohmann::json& j, const QBrush& brush);
void from_json(const nlohmann::json& j, QByteArray& byteArray);
void to_json(nlohmann::json& j, const QByteArray& byteArray);
void from_json(const nlohmann::json& j, QColor& color);
void to_json(nlohmann::json& j, const QColor& color);
void from_json(const nlohmann::json& j, QFont& font);
void to_json(nlohmann::json& j, const QFont& font);
void from_json(const nlohmann::json& j, QPoint& point);
void to_json(nlohmann::json& j, const QPoint& point);
void from_json(const nlohmann::json& j, QPointF& point);
void to_json(nlohmann::json& j, const QPointF& point);
void from_json(const nlohmann::json& j, QLine& line);
void to_json(nlohmann::json& j, const QLine& line);
void from_json(const nlohmann::json& j, QLineF& line);
void to_json(nlohmann::json& j, const QLineF& line);
void from_json(const nlohmann::json& j, QModelIndex& index);
void to_json(nlohmann::json& j, const QModelIndex& index);
void from_json(const nlohmann::json& j, QQuaternion& quaternion);
void to_json(nlohmann::json& j, const QQuaternion& quaternion);
void from_json(const nlohmann::json& j, QRect& rect);
void to_json(nlohmann::json& j, const QRect& rect);
void from_json(const nlohmann::json& j, QRectF& rect);
void to_json(nlohmann::json& j, const QRectF& rect);
void from_json(const nlohmann::json& j, QSize& size);
void to_json(nlohmann::json& j, const QSize& size);
void from_json(const nlohmann::json& j, QSizeF& size);
void to_json(nlohmann::json& j, const QSizeF& size);
void from_json(const nlohmann::json& j, QVector2D& vector);
void to_json(nlohmann::json& j, const QVector2D& vector);
void from_json(const nlohmann::json& j, QVector3D& vector);
void to_json(nlohmann::json& j, const QVector3D& vector);
void from_json(const nlohmann::json& j, QVector4D& vector);
void to_json(nlohmann::json& j, const QVector4D& vector);
// NOLINTEND(readability-identifier-naming)