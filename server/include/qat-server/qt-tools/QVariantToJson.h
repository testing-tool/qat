// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <nlohmann/json.hpp>

/// Forward declarations
/// @{
class QJsonValue;
class QVariant;
/// @}

namespace Qat
{

/// Return the type ID of the given QVariant
/// \param[in] variant A QVariant instance
/// \return the corresponding type ID
int GetVariantType(const QVariant& variant);


/// Serialize the given variant to Json. Supports native and Qt types.
/// \param[in] variant A variant
/// \return the serialized variant
nlohmann::json ToJson(const QVariant& variant);


/// Deserialize the given Json object to a variant. Supports native and Qt types.
/// \param[in] json A Json object
/// \return the deserialized variant
QVariant FromJson(const QJsonValue& json);

} // namespace Qat