// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <QObject>

#include <functional>
#include <memory>

namespace Qat
{

/// Forward declarations
class TcpServerLib;

/// \brief Class responsible for handling Qat server
class Server : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY_MOVE(Server)

public:
   /// Default constructor
   Server();

   /// Destructor
   ~Server() override;

   /// Factory to create a Server in Qt thread
   /// \param[in] onCreated callback to be called when Server has been created
   static void Create(std::function<void(const Server*)> onCreated);

   /// Return the port used by the server
   /// \return the port used by the server
   int GetPort() const noexcept;

signals:
   /// Emitted when the server is running
   void IsRunning();

private slots:

   /// Start the server
   void Start();

private:
   /// Stop the server
   void Stop();

   /// Pointer to the TCP server
   std::shared_ptr<TcpServerLib> mServer{nullptr};

   /// Port used by this server
   int mPort{-1};
};

} // namespace Qat