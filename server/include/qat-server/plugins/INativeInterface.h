// (c) Copyright 2024, Qat’s Authors
#pragma once

#include <QObject>

// Forward declarations
class QEvent;

namespace Qat
{
class NativeEventsFilter;

/// \brief Plugin interface to implement native event handlers.
class INativeInterface : public QObject
{
   Q_OBJECT

public:
   /// Default constructor
   INativeInterface()
       : QObject(nullptr)
   {
   }

   /// Default destructor
   ~INativeInterface() override = default;

   /// \copydoc QObject::event()
   bool event(QEvent* event) override = 0;

   /// Set event filter for the current platform.
   /// \param filter A native event filter.
   virtual void SetEventFilter(NativeEventsFilter* filter) = 0;
};

} // namespace Qat