// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <qat-server/library-tools/Platform.h>

#include <memory>
#include <vector>

#if defined(FOR_WINDOWS)
   #include "windows.h"
#endif

/// Forward declarations
/// @{
class QImage;
class QObject;
class QWindow;
/// @}

namespace Qat
{
/// Forward declarations
/// @{
class IObjectPicker;
class IWidget;
class INativeInterface;
/// @}

/// \brief Class responsible for creating a Qat plugin
class Plugin final
{
public:
#if defined(FOR_LINUX) || defined(FOR_MACOS)
   using LibHandle = void* const;
#elif defined(FOR_WINDOWS)
   using LibHandle = HMODULE;
   typedef IWidget*(CALLBACK* CastFunctionType)(const QObject*);
   typedef bool(CALLBACK* GetTopWindowsFunctionType)(QObject**, unsigned int*);
   typedef QImage*(CALLBACK* GrabFunctionType)(QObject*);
   typedef IObjectPicker*(CALLBACK* CreatePickerFunctionType)(QObject*);
   typedef INativeInterface*(CALLBACK* GetNativeInterfaceFunctionType)();
#endif

   /// Constructor
   /// \param[in] handle Opaque pointer to the plugin library
   explicit Plugin(LibHandle handle);

   /// Destructor
   ~Plugin() = default;

   /// Create a wrapper for the given QObject
   /// \param[in] qobject A QObject instance
   /// \return The wrapped QObject as a IWidget instance or nullptr if object
   ///         is not supported by the plugin
   std::unique_ptr<IWidget> CastObject(const QObject* qobject) const;

   /// Get all top-level windows/widgets
   /// \note windowList must be allocated (and freed) by the caller.
   /// When calling this function with a count of 0, the function writes
   /// the number of windows to the count argument.
   /// \param[in,out] windowList Pointer to array of windows
   /// \param[in,out] count Size of windowList
   /// \return True upon success, False upon failure
   bool GetTopWindows(QObject** windowList, unsigned int* count) const;

   /// Take a screenshot of the given window
   /// \param window Any valid window
   /// \return The screenshot as a QImage or nullptr if it failed.
   std::unique_ptr<QImage> GrabImage(QObject* window) const;

   /// Create an Object Picker for the given window
   /// \param window Any valid window
   /// \return The object picker or nullptr if it failed.
   IObjectPicker* CreatePicker(QObject* window) const;

   /// Return an object handling native events (only implemented by native plugins).
   /// \return The native interface object or nullptr if the plugin is not native.
   INativeInterface* GetNativeInterface() const;

private:
   /// Opaque pointer to the plugin library
   LibHandle mHandle;

#if defined(FOR_LINUX) || defined(FOR_MACOS)
   /// Pointer to the Cast function
   IWidget* (*mCastFunction)(const QObject*);

   /// Pointer to the GetTopWindows function
   bool (*mGetTopWindowsFunction)(QObject**, unsigned int*);

   /// Pointer to the Grab function
   QImage* (*mGrabFunction)(QObject*);

   /// Pointer to the CreatePicker function
   IObjectPicker* (*mCreatePickerFunction)(QObject*);

   /// Pointer to the GetNativeInterface function
   INativeInterface* (*mGetNativeInterfaceFunction)();

#elif defined(FOR_WINDOWS)
   /// Pointer to the Cast function
   CastFunctionType mCastFunction;

   /// Pointer to the GetTopWindows function
   GetTopWindowsFunctionType mGetTopWindowsFunction;

   /// Pointer to the Grab function
   GrabFunctionType mGrabFunction;

   /// Pointer to the CreatePicker function
   CreatePickerFunctionType mCreatePickerFunction;

   /// Pointer to the GetNativeInterface function
   GetNativeInterfaceFunctionType mGetNativeInterfaceFunction;
#endif
};

} // namespace Qat