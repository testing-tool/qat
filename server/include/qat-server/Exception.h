// (c) Copyright 2023, Qat’s Authors
#pragma once

#include <stdexcept>
#include <string>

namespace Qat
{
/// \brief Class defining exception type used in Qat
class Exception : public std::runtime_error
{
public:
   /// Constructor
   /// \param[in] message The error message
   explicit Exception(const std::string& message);

   /// Default destructor
   ~Exception() noexcept override = default;
};

} // namespace Qat