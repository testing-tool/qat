use std::{
    collections::HashMap,
    ffi::CString,
    io::{BufRead, BufReader, BufWriter, ErrorKind, Read, Write},
    net::TcpStream,
    sync::{Arc, Mutex},
    thread::{self, JoinHandle},
};

use crate::types::RequestCallback;

/// Structure handling communication for a single client
pub struct ClientConnection {
    /// TCP writer to send responses
    response_writer: BufWriter<TcpStream>,
    /// TCP stream for this client
    message_stream: Option<TcpStream>,
    /// Internal thread receiving TCP requests
    _thread: JoinHandle<()>,
}

impl ClientConnection {
    /// Create a new client with the given TCP stream.
    /// The client will automatically remove itself from the given map when the stream is closed.
    pub fn new(
        stream: TcpStream,
        client_map: Arc<Mutex<HashMap<u16, ClientConnection>>>,
        request_cb: RequestCallback,
    ) -> anyhow::Result<Self> {
        let peer_port = stream.peer_addr()?.port();
        stream.set_nonblocking(false)?;
        let response_writer = BufWriter::new(stream.try_clone()?);
        Ok(Self {
            response_writer,
            message_stream: None,
            _thread: thread::Builder::new().spawn(move || {
                if let Err(error) = Self::run(stream, peer_port, client_map, request_cb) {
                    eprintln!("Client thread failed: {error}");
                }
            })?,
        })
    }

    /// Send a response to the client
    pub fn send_response(&mut self, response: &str) -> anyhow::Result<()> {
        let encoded = format!("{}\n{response}", response.len());
        self.response_writer.write_all(encoded.as_bytes())?;
        self.response_writer.flush()?;
        Ok(())
    }

    /// Set the message stream (to the client's server) for this client.
    pub fn set_message_stream(&mut self, stream: Option<TcpStream>) {
        if let Some(stream) = self.message_stream.take() {
            let _ = stream.shutdown(std::net::Shutdown::Both);
        }
        self.message_stream = stream;
    }

    /// Send the given message to this client
    pub fn send_message(&mut self, message: &str) -> anyhow::Result<()> {
        if message.is_empty() {
            return Err(anyhow::Error::msg("Cannot send empty message"));
        }
        let encoded = format!("{}\n{message}", message.len());
        match self.message_stream.as_ref() {
            Some(mut stream) => Ok(stream.write_all(encoded.as_bytes())?),
            None => Err(anyhow::Error::msg(
                "Cannot send message: communication is not initialized",
            )),
        }
    }

    /// Handle incoming requests
    fn run(
        stream: TcpStream,
        peer_port: u16,
        client_map: Arc<Mutex<HashMap<u16, ClientConnection>>>,
        request_cb: RequestCallback,
    ) -> anyhow::Result<()> {
        let mut reader = BufReader::new(stream.try_clone()?);
        loop {
            let mut buffer = String::default();
            match reader.read_line(&mut buffer) {
                Ok(0) => {
                    println!("Client connection has been closed");
                    break;
                }
                Ok(_) => {
                    // Header contains the size of the request
                    match buffer.trim_end().parse::<usize>() {
                        Ok(size) => {
                            Self::handle_request(peer_port, request_cb, &mut reader, size);
                        }
                        Err(_) => {
                            eprintln!("Invalid header received ({buffer}) - ignoring")
                        }
                    }
                }
                Err(error) => {
                    eprintln!("Error receiving TCP data: {error}");
                    let fatal_errors = [
                        ErrorKind::ConnectionAborted,
                        ErrorKind::ConnectionReset,
                        ErrorKind::BrokenPipe,
                        ErrorKind::NotConnected,
                        ErrorKind::PermissionDenied,
                    ];
                    if fatal_errors.contains(&error.kind()) {
                        eprintln!("Aborting connection");
                        break;
                    }
                }
            }
        }
        // Remove client from map and drop it
        match client_map.lock() {
            Ok(mut map) => {
                map.remove(&peer_port);
                println!("Client removed");
            }
            Err(error) => eprintln!("Could not remove client: {error}"),
        }
        Ok(())
    }

    /// Forward request to callback and send its response on the TCP stream
    fn handle_request(
        peer_port: u16,
        request_cb: RequestCallback,
        reader: &mut BufReader<TcpStream>,
        size: usize,
    ) {
        let mut request_data = vec![0u8; size];
        match reader.read_exact(&mut request_data) {
            Ok(_) => match String::from_utf8(request_data) {
                Ok(request) => {
                    match CString::new(request) {
                        Ok(req_str) => {
                            request_cb(peer_port, req_str.as_ptr() as *const libc::c_char);
                        }
                        Err(error) => {
                            eprintln!("Invalid request received: {error}")
                        }
                    };
                }
                Err(error) => {
                    eprintln!("Invalid client request received: {error}")
                }
            },
            Err(error) => {
                eprintln!("Error reading client request: {error}")
            }
        }
    }
}
