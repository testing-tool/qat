// This library should not compile if documentation is missing
#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]

//! This crates implements a simple TCP server
//! intended to be used by a C / C++ wrapper

use std::{
    ffi::CStr,
    sync::{Mutex, OnceLock},
};

use tcp_server::TcpServer;
use types::RequestCallback;

/// Module handling client connections
mod client_connection;
/// TCP server implementation
mod tcp_server;
/// Type definitions
mod types;

/// Return the global instance of the TCP server. Create the server if necessary.
fn get_server() -> &'static Mutex<TcpServer> {
    /// Global instance of the TCP server.
    static SERVER: OnceLock<Mutex<TcpServer>> = OnceLock::new();
    SERVER.get_or_init(|| Mutex::new(TcpServer::new()))
}

/// Start a new TCP server.
/// This function has no effect if a server is already running.
/// Return The server port upon success, 0 otherwise.
#[no_mangle]
pub extern "C" fn start_server(request_cb: RequestCallback) -> u16 {
    match get_server().lock() {
        Ok(mut server) => {
            if server.start(request_cb).is_ok() {
                server.port
            } else {
                0
            }
        }
        Err(error) => {
            eprintln!("Could not start server: {error}");
            0
        }
    }
}

/// Stop the current TCP server.
/// This function has no effect if no server is running.
/// Return True upon success, False otherwise.
#[no_mangle]
pub extern "C" fn stop_server() -> bool {
    match get_server().lock() {
        Ok(mut server) => server.stop().is_ok(),
        Err(error) => {
            eprintln!("Could not stop server: {error}");
            false
        }
    }
}

/// Send the given response to a connected client.
/// Return True upon success, False otherwise.
///
/// # Safety
/// This function uses a C-string as argument and will dereference it.
/// This may crash the library if the string is invalid.
#[no_mangle]
pub unsafe extern "C" fn send_response(client_id: u16, response: *const libc::c_char) -> bool {
    if response.is_null() {
        eprintln!("null pointer received in send_response()");
        return false;
    }
    match get_server().lock() {
        Ok(server) => {
            let message = unsafe { CStr::from_ptr(response) };
            match message.to_str() {
                Ok(message) => server.send_response(client_id, message).is_ok(),
                Err(error) => {
                    eprintln!("Invalid response: {error}");
                    false
                }
            }
        }
        Err(error) => {
            eprintln!("Could not send response: {error}");
            false
        }
    }
}

/// Initialize communication from server to client.
/// Return True upon success, False otherwise.
///
/// # Safety
/// This function uses a C-string as argument and will dereference it.
/// This may crash the library if the string is invalid.
#[no_mangle]
pub unsafe extern "C" fn init_comm(client_id: u16, url: *const libc::c_char) -> bool {
    if url.is_null() {
        eprintln!("null pointer received in init_comm()");
        return false;
    }
    match get_server().lock() {
        Ok(server) => {
            let url = unsafe { CStr::from_ptr(url) };
            match url.to_str() {
                Ok(url) => server.init_comm(client_id, url).is_ok(),
                Err(error) => {
                    eprintln!("init_comm: Invalid URL: {error}");
                    false
                }
            }
        }
        Err(error) => {
            eprintln!("Could not initialize communication: {error}");
            false
        }
    }
}

/// Send the given message to a connected client.
/// Return True upon success, False otherwise.
///
/// # Safety
/// This function uses a C-string as argument and will dereference it.
/// This may crash the library if the string is invalid.
#[no_mangle]
pub unsafe extern "C" fn send_message(client_id: u16, message: *const libc::c_char) -> bool {
    if message.is_null() {
        eprintln!("null pointer received in send_message()");
        return false;
    }
    match get_server().lock() {
        Ok(server) => {
            let message = unsafe { CStr::from_ptr(message) };
            match message.to_str() {
                Ok(message) => server.send_message(client_id, message).is_ok(),
                Err(error) => {
                    eprintln!("Invalid message: {error}");
                    false
                }
            }
        }
        Err(error) => {
            eprintln!("Could not send message: {error}");
            false
        }
    }
}

/// Close communication from server to client.
/// Return True upon success, False otherwise.
#[no_mangle]
pub extern "C" fn close_comm(client_id: u16) -> bool {
    match get_server().lock() {
        Ok(server) => server.close_comm(client_id).is_ok(),
        Err(error) => {
            eprintln!("Could not close communication: {error}");
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        ffi::CString,
        io::{self, BufRead, BufReader, Read, Write},
        net::{TcpListener, TcpStream},
        thread,
        time::Duration,
    };

    use crate::{close_comm, init_comm, send_message, send_response, start_server};

    #[test]
    fn complete_scenario() {
        // Callback function sending the client ID as a response
        extern "C" fn echo_client_id_cb(client_id: u16, _request: *const libc::c_char) {
            let response = format!("{client_id}");
            let response = CString::new(response).expect("Response should be a valid C string");
            unsafe { send_response(client_id, response.as_ptr()) };
        }

        let server_port = start_server(echo_client_id_cb);
        assert_ne!(server_port, 0);

        // Create 2 clients
        let client1 = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client1.is_ok());
        let mut client1 = client1.unwrap();
        assert!(client1.set_nonblocking(false).is_ok());

        let client2 = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client2.is_ok());
        let mut client2 = client2.unwrap();
        assert!(client2.set_nonblocking(false).is_ok());

        // Create one TCP server per client
        let listener1 =
            TcpListener::bind("127.0.0.1:0").expect("Test server should be able to start");
        let addr1 = listener1
            .local_addr()
            .expect("Test server address must be defined");
        let listener2 =
            TcpListener::bind("127.0.0.1:0").expect("Test server should be able to start");
        let addr2 = listener2
            .local_addr()
            .expect("Test server address must be defined");

        // Send one request per client to retrieve their ID
        let data1 = "hello1";
        let request = format!("{}\n{data1}", data1.len());
        assert!(client1.write_all(request.as_bytes()).is_ok());

        let data2 = "hello2";
        let request = format!("{}\n{data2}", data2.len());
        assert!(client2.write_all(request.as_bytes()).is_ok());

        // Wait for responses to initialize reverse communication
        let clients = [(&client1, addr1.port()), (&client2, addr2.port())];
        let mut client_ids = Vec::<u16>::new();
        for (client, port) in clients {
            let mut reader =
                BufReader::new(client.try_clone().expect("Cloning stream should work"));
            let mut header = String::new();
            reader
                .read_line(&mut header)
                .expect("Stream should receive header");
            let length = header
                .trim_end()
                .parse::<usize>()
                .expect("Header should contain valid length");
            let mut buffer = vec![0u8; length];
            assert!(reader.read_exact(&mut buffer).is_ok());
            let response = String::from_utf8(buffer).expect("Response should be valid UTF8");
            let id = response
                .parse::<u16>()
                .expect("Response should contain valid client ID");
            client_ids.push(id);
            let url = format!("127.0.0.1:{port}");
            let url = CString::new(url).expect("URL should be a valid C string");
            assert!(unsafe { init_comm(id, url.as_ptr()) });
        }

        // Verify that each client has been connected
        let mut client_streams = Vec::<TcpStream>::new();
        for listener in &[listener1, listener2] {
            let mut client_stream: Option<TcpStream> = None;
            listener
                .set_nonblocking(true)
                .expect("Test server should be non-blocking");
            for _ in 0..10 {
                match listener.accept() {
                    Ok(stream) => {
                        let stream = stream.0;
                        stream
                            .set_nonblocking(false)
                            .expect("Socket should work in blocking mode");
                        stream
                            .set_read_timeout(Some(Duration::from_secs(1)))
                            .expect("Socket should allow read timeout");
                        client_stream = Some(stream);
                        break;
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                        thread::sleep(Duration::from_millis(100));
                    }
                    Err(error) => panic!("Test server error: {error}"),
                }
            }
            assert!(
                client_stream.is_some(),
                "Test server did not receive client connection"
            );

            // Wait for synchronization message
            let client_stream = client_stream.unwrap();
            let mut reader = BufReader::new(client_stream.try_clone().unwrap());
            let mut buffer = String::new();
            reader
                .read_line(&mut buffer)
                .expect("Synchronization message should be received");
            assert_eq!(buffer.trim_end(), "0");
            client_streams.push(client_stream);
        }
        assert_eq!(client_streams.len(), 2);

        // Send a message to each client
        let mut messages = Vec::<String>::new();
        for client_id in &client_ids {
            let message = format!("Hi {client_id}");
            messages.push(message.clone());
            let message = CString::new(message).expect("Message should be a valid C string");
            assert!(unsafe { send_message(*client_id, message.as_ptr()) });
        }
        assert_eq!(messages.len(), 2);

        // Verify that each client received its message
        let expected = [
            (&client_streams[0], &messages[0]),
            (&client_streams[1], &messages[1]),
        ];
        for (client_stream, message) in expected {
            let mut reader = BufReader::new(client_stream);
            let mut header = String::new();
            reader
                .read_line(&mut header)
                .expect("Stream should receive header");
            let length = header
                .trim_end()
                .parse::<usize>()
                .expect("Header should contain valid size");
            let mut buffer = vec![0u8; length];
            assert!(reader.read_exact(&mut buffer).is_ok());
            let response = String::from_utf8(buffer).expect("Response should be valid UTF8");
            println!("Received message: {response}");
            assert_eq!(response, *message);
        }

        // Verify that messages were not sent to the wrong client
        for client_stream in &mut client_streams {
            assert!(client_stream
                .set_read_timeout(Some(Duration::from_millis(200)))
                .is_ok());
            let mut buf = [0u8; 1];
            assert!(client_stream.read_exact(&mut buf).is_err());
        }

        // Close communication with client1
        assert!(close_comm(client_ids[0]));
        let message = CString::new("disconnected").expect("Message should be a valid C string");
        assert!(unsafe { !send_message(client_ids[0], message.as_ptr()) });
    }
}
