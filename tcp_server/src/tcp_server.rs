use std::{
    collections::HashMap,
    io::Write,
    net::{TcpListener, TcpStream},
    sync::{atomic::AtomicBool, Arc, Mutex},
    thread::{self, JoinHandle},
};

use anyhow::anyhow;

use crate::{client_connection::ClientConnection, types::RequestCallback};

/// Implementation of a simple TCP server handling incoming connections.
pub struct TcpServer {
    /// Port number of the server socket
    pub port: u16,
    /// Main server thread waiting for client connections
    thread: Option<JoinHandle<()>>,
    /// Thread-safe flag stopping the server thread when set to True
    stop_flag: Arc<AtomicBool>,
    /// Thread-safe container of client connections, identified by their remote port number
    clients: Arc<Mutex<HashMap<u16, ClientConnection>>>,
}

impl TcpServer {
    /// Create a new TCP server
    pub fn new() -> Self {
        Self {
            thread: None,
            port: 0,
            stop_flag: Arc::new(AtomicBool::default()),
            clients: Arc::new(Mutex::new(HashMap::<u16, ClientConnection>::new())),
        }
    }

    /// Start the server by initializing a TCP listener in a dedicated thread
    /// Note: has no effect if server is already running.
    pub fn start(&mut self, request_cb: RequestCallback) -> anyhow::Result<()> {
        // Do nothing if server is already running
        if self.thread.is_some() {
            return Ok(());
        }
        let flag = self.stop_flag.clone();
        let listener = TcpListener::bind("0.0.0.0:0")?;
        let addr = listener.local_addr()?;
        self.port = addr.port();
        println!("TCP server running on port {0}", self.port);
        let clients = self.clients.clone();
        self.thread = Some(thread::Builder::new().spawn(move || {
            let _ = Self::run(listener, &flag, clients, request_cb);
        })?);
        Ok(())
    }

    /// Stop the server and wait for its listener thread to finish.
    /// Note: has no effect if server is not running.
    pub fn stop(&mut self) -> anyhow::Result<()> {
        if let Some(thread) = self.thread.take() {
            self.stop_flag
                .store(true, std::sync::atomic::Ordering::Release);
            // Connect to the server to unblock the listener
            TcpStream::connect(format!("localhost:{}", self.port))?;
            let _ = thread.join();
        }
        Ok(())
    }

    /// Send the given response to a connected client.
    pub fn send_response(&self, id: u16, message: &str) -> anyhow::Result<()> {
        let mut client_map = match self.clients.lock() {
            Ok(it) => it,
            Err(error) => return Err(anyhow!("Could not send message: {error}")),
        };
        let client = client_map.get_mut(&id);

        match client {
            Some(client) => {
                client.send_response(message)?;
                Ok(())
            }
            None => Err(anyhow!("Could not send response: client not found")),
        }
    }

    /// Initialize asynchronous communication to the client's TCP server.
    pub fn init_comm(&self, id: u16, url: &str) -> anyhow::Result<()> {
        println!("Connecting to {url}");
        let mut stream = TcpStream::connect(url)?;
        let sync_message = "0\n";
        stream.write_all(sync_message.as_bytes())?;
        stream.flush()?;

        let mut client_map = match self.clients.lock() {
            Ok(it) => it,
            Err(error) => return Err(anyhow!("Could not initialize communication: {error}")),
        };
        let client = client_map.get_mut(&id);

        match client {
            Some(client) => {
                client.set_message_stream(Some(stream));
                Ok(())
            }
            None => Err(anyhow!("Could not init communication: client not found")),
        }
    }

    /// Close asynchronous communication to the client's TCP server.
    pub fn close_comm(&self, id: u16) -> anyhow::Result<()> {
        let mut client_map = match self.clients.lock() {
            Ok(it) => it,
            Err(error) => return Err(anyhow!("Could not close communication: {error}")),
        };
        let client = client_map.get_mut(&id);

        match client {
            Some(client) => {
                client.set_message_stream(None);
                Ok(())
            }
            None => Err(anyhow!("Could not close communication: client not found")),
        }
    }

    /// Send the given message to a connected client.
    pub fn send_message(&self, id: u16, message: &str) -> anyhow::Result<()> {
        let mut client_map = match self.clients.lock() {
            Ok(it) => it,
            Err(error) => return Err(anyhow!("Could not send message: {error}")),
        };
        let client = client_map.get_mut(&id);

        match client {
            Some(client) => {
                client.send_message(message)?;
                Ok(())
            }
            None => Err(anyhow!("Could not send message: client not found")),
        }
    }

    /// Internal function handling new client connections in a loop.
    /// Intended to be called by a dedicated thread.
    fn run(
        listener: TcpListener,
        stop_flag: &AtomicBool,
        clients: Arc<Mutex<HashMap<u16, ClientConnection>>>,
        request_cb: RequestCallback,
    ) -> anyhow::Result<()> {
        for stream in listener.incoming() {
            if stop_flag.load(std::sync::atomic::Ordering::Relaxed) {
                break;
            }
            match stream {
                Ok(stream) => {
                    println!("New client");
                    match clients.lock() {
                        Ok(mut client_map) => match stream.peer_addr() {
                            Ok(addr) => {
                                match ClientConnection::new(stream, clients.clone(), request_cb) {
                                    Ok(client) => {
                                        client_map.insert(addr.port(), client);
                                    }
                                    Err(error) => eprintln!("Unable to create client: {error}"),
                                }
                            }
                            Err(error) => eprintln!("Unable to get client IP: {error}"),
                        },
                        Err(error) => eprintln!("Unable to add client: {error}"),
                    }
                }
                Err(error) => eprintln!("TCP connection failed: {error}"),
            }
        }
        Ok(())
    }

    /// For testing purposes only
    fn _get_nb_clients(&self) -> usize {
        match self.clients.lock() {
            Ok(map) => map.len(),
            Err(_) => {
                eprintln!("Could not access client map");
                0
            }
        }
    }
}

impl Drop for TcpServer {
    fn drop(&mut self) {
        let _ = self.stop();
    }
}

#[cfg(test)]
mod tests {
    use std::{
        ffi::CStr,
        io::{self, BufRead, BufReader, Read, Write},
        sync::OnceLock,
        time::Duration,
    };

    use super::*;

    #[test]
    fn server_lifecycle() {
        extern "C" fn null_response_cb(_client_id: u16, _request: *const libc::c_char) {}
        let mut server = TcpServer::new();

        // Calling stop should have no effect when the server has not been started
        assert!(server.stop().is_ok());

        assert!(server.start(null_response_cb).is_ok());
        assert!(server.stop().is_ok());

        // Restart the server to test the Drop trait implementation (should not deadlock)
        assert!(server.start(null_response_cb).is_ok());

        // Starting the same server multiple times should have no effect
        assert!(server.start(null_response_cb).is_ok());
    }

    #[test]
    fn single_client() {
        fn get_server() -> &'static Mutex<TcpServer> {
            static SERVER: OnceLock<Mutex<TcpServer>> = OnceLock::new();
            SERVER.get_or_init(|| Mutex::new(TcpServer::new()))
        }

        extern "C" fn response_cb(client_id: u16, _request: *const libc::c_char) {
            let server = get_server().lock().unwrap();
            server
                .send_response(client_id, "9")
                .expect("Sending response should work");
        }

        // Start a server
        let server_port;
        {
            let mut server = get_server().lock().unwrap();
            assert!(server.start(response_cb).is_ok());

            // No client should be connected by default
            assert_eq!(server._get_nb_clients(), 0);
            server_port = server.port;
        }

        // Create a client
        let client = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client.is_ok());
        let mut client = client.unwrap();
        assert!(client.set_nonblocking(false).is_ok());
        assert!(client
            .set_read_timeout(Some(Duration::from_secs(1)))
            .is_ok());

        // Send request
        let data = "hello";
        let request = format!("{}\n{data}", data.len());
        assert!(client.write_all(request.as_bytes()).is_ok());

        // Wait for response
        let mut reader = BufReader::new(client.try_clone().expect("Cloning stream should work"));
        let mut header = String::new();
        reader
            .read_line(&mut header)
            .expect("Stream should receive header");
        assert_eq!(header.trim_end(), "1");
        let mut buffer = [0u8; 1];
        assert!(reader.read_exact(&mut buffer).is_ok());
        assert_eq!(buffer[0], "9".as_bytes()[0]);

        {
            let server = get_server().lock().unwrap();
            assert_eq!(server._get_nb_clients(), 1);

            // Disconnect client
            assert!(client.shutdown(std::net::Shutdown::Both).is_ok());

            for _ in 0..50 {
                thread::sleep(Duration::from_millis(10));
                if server._get_nb_clients() == 0 {
                    break;
                }
            }
            assert_eq!(server._get_nb_clients(), 0);
        }
    }

    #[test]
    fn multiple_clients() {
        fn get_server() -> &'static Mutex<TcpServer> {
            static SERVER: OnceLock<Mutex<TcpServer>> = OnceLock::new();
            SERVER.get_or_init(|| Mutex::new(TcpServer::new()))
        }

        extern "C" fn echo_response_cb(client_id: u16, request: *const libc::c_char) {
            let server = get_server().lock().unwrap();
            let response = unsafe { CStr::from_ptr(request) }.to_str().unwrap();
            server
                .send_response(client_id, response)
                .expect("Sending response should work");
        }

        // Start a server
        let server_port;
        {
            let mut server = get_server().lock().unwrap();
            assert!(server.start(echo_response_cb).is_ok());

            // No client should be connected by default
            assert_eq!(server._get_nb_clients(), 0);
            server_port = server.port;
        }

        // Create 2 clients
        let client1 = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client1.is_ok());
        let mut client1 = client1.unwrap();
        assert!(client1.set_nonblocking(false).is_ok());

        let client2 = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client2.is_ok());
        let mut client2 = client2.unwrap();
        assert!(client2.set_nonblocking(false).is_ok());

        // Send one request per client
        let data1 = "hello1";
        let request = format!("{}\n{data1}", data1.len());
        assert!(client1.write_all(request.as_bytes()).is_ok());

        let data2 = "hello2";
        let request = format!("{}\n{data2}", data2.len());
        assert!(client2.write_all(request.as_bytes()).is_ok());

        // Wait for responses
        let expected = [(client1, data1), (client2, data2)];
        for (client, data) in expected {
            let mut reader =
                BufReader::new(client.try_clone().expect("Cloning stream should work"));
            let mut header = String::new();
            reader
                .read_line(&mut header)
                .expect("Stream should receive header");
            assert_eq!(header.trim_end(), data.len().to_string());
            let mut buffer = vec![0u8; data.len()];
            assert!(reader.read_exact(&mut buffer).is_ok());
            let response = String::from_utf8(buffer).expect("Response should be valid UTF8");
            assert_eq!(response, data);
        }
    }

    #[test]
    fn message() {
        fn get_server() -> &'static Mutex<TcpServer> {
            static SERVER: OnceLock<Mutex<TcpServer>> = OnceLock::new();
            SERVER.get_or_init(|| Mutex::new(TcpServer::new()))
        }

        extern "C" fn id_response_cb(id: u16, _request: *const libc::c_char) {
            let server = get_server().lock().unwrap();
            server
                .send_response(id, &format!("{id}"))
                .expect("Server should allow sending responses");
        }

        // Start a server
        // Start a server
        let server_port;
        {
            let mut server = get_server().lock().unwrap();
            assert!(server.start(id_response_cb).is_ok());

            // No client should be connected by default
            assert_eq!(server._get_nb_clients(), 0);
            server_port = server.port;
        }

        // Create a client
        let client = TcpStream::connect(format!("127.0.0.1:{}", server_port));
        assert!(client.is_ok());
        let mut client = client.unwrap();
        assert!(client.set_nonblocking(false).is_ok());
        assert!(client
            .set_read_timeout(Some(Duration::from_secs(1)))
            .is_ok());

        // Send a request to retrieve its ID.
        let data1 = "hello";
        let request = format!("{}\n{data1}", data1.len());
        assert!(client.write_all(request.as_bytes()).is_ok());

        let mut reader = BufReader::new(client.try_clone().expect("Cloning stream should work"));
        let mut header = String::new();
        reader
            .read_line(&mut header)
            .expect("Stream should receive header");
        let length = header
            .trim_end()
            .parse::<usize>()
            .expect("Header should contain valid size");
        let mut buffer = vec![0u8; length];
        assert!(reader.read_exact(&mut buffer).is_ok());
        let response = String::from_utf8(buffer).expect("Response should be valid UTF8");
        let id = response
            .parse::<u16>()
            .expect("Response should contain the client ID");

        // Create a TCP server
        let listener =
            TcpListener::bind("127.0.0.1:0").expect("Test server should be able to start");
        let addr = listener
            .local_addr()
            .expect("Test server address must be defined");

        // Init communication for this client
        let url = format!("127.0.0.1:{}", addr.port());
        {
            let server = get_server().lock().unwrap();
            assert!(server.init_comm(id, &url).is_ok());
        }
        listener
            .set_nonblocking(true)
            .expect("Test server should be non-blocking");

        let mut client_stream: Option<TcpStream> = None;
        for _ in 0..10 {
            match listener.accept() {
                Ok(stream) => {
                    let stream = stream.0;
                    stream
                        .set_nonblocking(false)
                        .expect("Socket should work in blocking mode");
                    stream
                        .set_read_timeout(Some(Duration::from_secs(1)))
                        .expect("Socket should allow read timeout");
                    client_stream = Some(stream);
                    break;
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    thread::sleep(Duration::from_millis(100));
                }
                Err(error) => panic!("Test server error: {error}"),
            }
        }
        assert!(
            client_stream.is_some(),
            "Test server did not receive client connection"
        );

        // Wait for synchronization byte
        let client_stream = client_stream.unwrap();
        let mut reader = BufReader::new(client_stream.try_clone().unwrap());
        let mut buffer = String::new();
        reader
            .read_line(&mut buffer)
            .expect("Synchronization message should be received");
        assert_eq!(buffer.trim_end(), "0");

        // Send an asynchronous message to the client
        let message = "Message!";
        {
            let server = get_server().lock().unwrap();
            assert!(server.send_message(id, message).is_ok());
        }

        // Verify that the message was sent
        let mut reader = BufReader::new(client_stream);
        let mut header = String::new();
        reader
            .read_line(&mut header)
            .expect("Stream should receive header");
        let length = header
            .trim_end()
            .parse::<usize>()
            .expect("Header should contain valid size");
        let mut buffer = vec![0u8; length];
        assert!(reader.read_exact(&mut buffer).is_ok());
        let response = String::from_utf8(buffer).expect("Response should be valid UTF8");
        assert_eq!(response, message);
    }
}
