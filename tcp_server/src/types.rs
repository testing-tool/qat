/// C function called when a request is received
pub type RequestCallback = extern "C" fn(client_id: u16, request: *const libc::c_char);
